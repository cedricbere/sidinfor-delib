from django.urls import path#,include
from deliberation.views import Creation_jury, saisie_Note,Consulter_note_form,choix_saisie_note,\
    choix_consulter_note, consulter_Note, detail_cursus, modifier_Note,\
    consulter_Jury, liste_Jury, consulter_Membre_Jury, del_Jury, get_Jury,\
    modif_Jury, deliberer, imprimer_Releve, supp_releve, Modifier_Note_Etd,\
    liste_Jury_par_ens, imprimer_membre_Jury, imprimer_Note



urlpatterns=[
    path('creation/jury/', Creation_jury, name="creation_jury"),
    path('saisir_note/<idMatiere>/<departement>/<niveau>/', saisie_Note, name="saisie_Note"),
    path('Modifier_note/<idMatiere>/<departement>/<niveau>/', modifier_Note, name="modifier_Note"),
    path('consulter_note/<id_matiere>/<depart>/<niveau>/', consulter_Note, name="consulter_Note"),
    path('consulter_note/formulaire/', Consulter_note_form, name="Consulter_note_form"),
    path('Saisir_note/', choix_saisie_note, name="choix_saisie_note"),
    path('Consulter_note/', choix_consulter_note, name="choix_consulter_note"),
    path('Consulter_jury/<idJury>/', consulter_Jury, name="consulter_Jury"),
     path('Consulter_membre_jury/<idJury>/', consulter_Membre_Jury, name="consulter_Membre_Jury"),
    path('liste/jury/',liste_Jury, name="liste_Jury"),
    path('liste/jury-par-enseignant/',liste_Jury_par_ens, name="liste_Jury_par_ens"),
    path('supprimer/jury/<idJury>/', del_Jury, name="del_Jury"),
    path('recuperer/jury/<idJury>/', get_Jury, name="get_Jury"),
    path('modifier/jury/<idJury>/', modif_Jury, name="modif_Jury"),
    path('Consulter_cursus/<departement>/<niveau>/<annee>/<etudiant>/', detail_cursus, name="detail_cursus"),
    
    path('deliberation/<idJury>/', deliberer, name='deliberer'),
    path('modifier-note-etudiant/<idE>/<idJ>/', Modifier_Note_Etd, name="Modifier_Note_Etd"),
    
    path('impression-releve/',imprimer_Releve, name='imprimer_Releve'),
    path('impression-note/<idM>/',imprimer_Note, name='imprimer_Note'),
    path('impression-membre-de-jury/<idJury>/',imprimer_membre_Jury, name='imprimer_membre_Jury'),
    
    path('suppression-de-releve-de-note/<idRel>', supp_releve, name='supp_releve'),
    
    
    ]