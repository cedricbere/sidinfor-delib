from django.shortcuts import render, redirect
from deliberation.forms import Creer_jury, Membre_jury, Saisie_note, HeaderForm,\
    ImpressionForm
from deliberation.models import Formation_Jury, Jury, Note, Note_Save, Option,\
    Deliberation, Note_UE, Releve, Cursus
from base.models import Etudiant, Matiere, Notifications, UE, Niveau,\
    Departement, AnneeAcad, Enseignant
from base.other import envoyer_Mail_Affectation, envoi_Admin_Aff_Jury,\
    envoyer_Mail_Inscrit
from django.views.generic.dates import timezone_today
from base.views import form_user
from gestion_enseignement.models import Enseign_An_Matiere
from django.conf import settings
from django.contrib.auth.models import Group
#from excel_response import ExcelResponse
from openpyxl import Workbook
from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from wkhtmltopdf.views import PDFTemplateResponse

# Create your views here.

def Creation_jury(request):
    """
    """
    message=""
    creation_jury=Creer_jury()
    formation_membre=Membre_jury()
    logged_user=form_user(request)
    if logged_user:
        if request.POST:
            creation_jury=Creer_jury(request.POST)
            formation_membre=Membre_jury(request.POST)
            if creation_jury.is_valid() and formation_membre.is_valid():
                
                nbre=creation_jury.cleaned_data['nbre_membre']
                jury=Jury(intitule=creation_jury.cleaned_data['intitule'], niveau=creation_jury.cleaned_data['niveau'], departement=creation_jury.cleaned_data['departement'], nbre_membre=nbre)
                jury.save()
                
                formation_membre.jury=jury
                for i in range(1,nbre+1):
                    
                    ens=formation_membre.cleaned_data['ens'+str(i)]
                    fonct=formation_membre.cleaned_data['fonction'+str(i)]
                    
                    m= Formation_Jury(enseignant=ens,jury=formation_membre.jury, fonction=fonct)
                    new=Notifications(enseignant=ens, information="Vous avez été ajouté aux membres d'un jury avec la fonction de : "+fonct,url="http://localhost:8000/enseignement/affiche_jury/<jury.id>/", date=timezone_today())
                   
                    m.save()
                    new.save()
                    
                    user =ens.compte
                    if fonct=='Membre':
                        membre_jury=Group.objects.get(name='membre_jury')
                        user.groups.add(membre_jury)
                    
                    elif fonct=='Secrétaire':
                        secretaire_jury=Group.objects.get(name='secretaire_jury')
                        user.groups.add(secretaire_jury)
                        
                    elif fonct=='Président':
                        president_jury=Group.objects.get(name='president_jury')
                        user.groups.add(president_jury)
                        
                    ens.save()
                    
                    mail_envoye=envoyer_Mail_Affectation(type_mail='Jury', destinataire=formation_membre.cleaned_data['ens'+str(i)].compte.email)
                    
                detail='La création du jury de délibération '+creation_jury.cleaned_data['intitule']+' du niveau '+str(creation_jury.cleaned_data['niveau'])+' departement '+str(creation_jury.cleaned_data['departement'])+' à été effectuée avec succès '
                envoi_Admin_Aff_Jury(type_mail = 'Jury', detail =detail, mail_envoye = mail_envoye)
                    
                message="Jury crée avec succès :) "
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        creation_jury=Creer_jury()
        formation_membre=Membre_jury()
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif, 'message':message,'creation_jury':creation_jury, 'formation_membre':formation_membre}
        
        return render(request, 'creationJury.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def choix_saisie_note(request):
    """
    """
    
    logged_user=form_user(request)
    if logged_user:
        note_ens_saisie={}
        note_jury_saisie={}
        liste_matiere_all =Matiere.objects.filter(is_actif=True)
        liste_matiere_enseigne=Enseign_An_Matiere.objects.filter(enseignant=logged_user,matiere__is_actif=True, is_actif=True)
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user, jury__is_actif=True)
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_note_save=Note_Save.objects.filter(is_actif=True)
        
        
        for sve in liste_note_save:
            for item in liste_matiere_enseigne:
                if item.matiere == sve.matiere :
                    note_ens_saisie[item.matiere.intitule]=item.matiere
                    
            for elt in liste_matiere_all:
                if elt == sve.matiere:
                    note_jury_saisie[elt.intitule]=elt
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'user':logged_user, 'liste_matiere_enseigne':liste_matiere_enseigne, 'liste_jury':liste_jury,'nbre_notif':nbre_notif, 'liste_notif':liste_notif, 'liste_matiere_all':liste_matiere_all, 'liste_note_save':liste_note_save, 'note_ens_saisie':note_ens_saisie, 'note_jury_saisie':note_jury_saisie}
        
        return render(request, 'choix_saisie_note.html', context)
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def choix_consulter_note(request):
    """
    """
    
    logged_user=form_user(request)
    if logged_user:
        note_ens_saisie={}
        note_jury_saisie={}
        liste_matiere_all =Matiere.objects.filter(is_actif=True)
        liste_matiere_enseigne=Enseign_An_Matiere.objects.filter(enseignant=logged_user,matiere__is_actif=True, is_actif=True)
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user, jury__is_actif=True)
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_note_save=Note_Save.objects.filter(is_actif=True)
        
        for sve in liste_note_save:
            for item in liste_matiere_enseigne:
                if item.matiere == sve.matiere :
                    note_ens_saisie[item.matiere.intitule]=item.matiere
                    
            for elt in liste_matiere_all:
                if elt == sve.matiere:
                    note_jury_saisie[elt.intitule]=elt
        
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user, 'liste_matiere_enseigne':liste_matiere_enseigne, 'liste_jury':liste_jury,'nbre_notif':nbre_notif, 'liste_notif':liste_notif, 'liste_matiere_all':liste_matiere_all, 'liste_note_save':liste_note_save, 'note_ens_saisie':note_ens_saisie, 'note_jury_saisie':note_jury_saisie}
        
        return render(request, 'choix_consulter_note.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def saisie_Note(request,idMatiere, departement, niveau):
    """
    """
    liste_note=False
    an=''
    logged_user=form_user(request)
    if logged_user:
        if request.POST:
            liste_etudiant=Etudiant.objects.filter(departement__intitule=request.POST['departement'],niveau__abrege=request.POST['niveau'], is_actif=True, is_ancien=False).order_by('nom', 'prenom')
            idM=request.POST['idMat']
            mat=Matiere.objects.get(id=idM)
            print(mat.coef)
            coef= mat.coef
            for item in liste_etudiant:
                etd=Etudiant.objects.get(id=item.id)
                an=etd.anneeAca
                if len(Note.objects.filter(student=etd, course=mat, anneeAca=etd.anneeAca))==0:
                    
                    val_note=request.POST['note_'+str(item.id)]
                    if val_note!='':
                        
                        note=Note(note=val_note, pondere=float(val_note) * float (coef), student=etd,course=mat,anneeAca=etd.anneeAca, is_actif=True )
                        note.save()
                    else :
                        note=Note(note=0, pondere=0, student=etd,course=mat,anneeAca=etd.anneeAca , is_actif=True)
                        note.save()
            
            # signalement pour montrer que cette note à été renseignée
            
            etudiant=Etudiant.objects.filter(departement__intitule=departement,niveau__abrege=niveau, is_actif=True, is_ancien=False)[0]
            
            nsave=Note_Save(matiere=mat, anneeaca=etudiant.anneeAca, is_actif=True)
            nsave.save()        
                
            #liste_note=Note.objects.filter(course__id=idMatiere, annee_aca=etd.anneeAca)
            #context={'liste_etudiant':liste_etudiant, 'matiere':mat, 'niveau':request.POST['niveau'], 'departement':request.POST['departement'],'liste_note':liste_note}
            
            # enregistrer une notification pour le secrétaire de jury 
            secretaire_jury=Formation_Jury.objects.get(jury__niveau__abrege=niveau, jury__departement__intitule=departement, fonction='Secrétaire')
            
            
            ens_secretaire=secretaire_jury.enseignant
            
            #new=Notifications(enseignant=ens_secretaire, information="Les notes en "+mat.intitule+" ont été renseigné",url="http://localhost:8000/Delibération/consulter_note/<id>/", date=timezone_today())
            #new.save()
           
            
            # je recupère le nombre de matière du niveau dont les nombres sont entrées pour voir si elles sont toutes rentré
            
            note_save= len(Note_Save.objects.filter(matiere__uniteEnseign__niveau__abrege=niveau, matiere__uniteEnseign__departement__intitule=departement, anneeaca=an, is_actif=True))
            
            # compter le nombre de matiere au total
            
            note_total= len(Matiere.objects.filter(uniteEnseign__niveau__abrege=niveau, uniteEnseign__departement__intitule=departement, is_actif=True))
            
            if (note_save == note_total):
                new=Notifications(enseignant=ens_secretaire, information="Toutes les notes en "+departement+" niveau "+niveau+" sont entrées !" ,url="http://localhost:8000/Delibération/consulter_note/<id>/", date=timezone_today())
                new.save()
                
                # envoie de mail 
                
                mail_envoye = envoyer_Mail_Inscrit(type_mail = 'Secrétariat', regime = 'Enseignant', destinataire=ens_secretaire.compte.email)
                
                print(mail_envoye)
                
            message=" Les notes ont été enrégistrées avec succès !"
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'message':message, 'matiere':mat, 'niveau':request.POST['niveau'], 'departement':request.POST['departement']}
            
            return render(request, 'note_saisie_succes.html', context)
        
        #liste_note=Note.objects.filter()
        
        matiere=Matiere.objects.get(id=idMatiere)
        liste_etudiant=Etudiant.objects.filter(departement__intitule=departement,niveau__abrege=niveau, is_actif=True, is_ancien=False).order_by('nom', 'prenom')
        
        # recupération d'un étudiant pour avoir l'année académique
        etudiant=Etudiant.objects.filter(departement__intitule=departement,niveau__abrege=niveau, is_actif=True, is_ancien=False)[0]
        
        # recupération des notes pour les étudiants qui ont déja une note dans la matiere
        
        liste_note=Note.objects.filter(course=matiere, anneeAca=etudiant.anneeAca)
        #liste_note=Note.objects.filter(course__id=idMatiere, annee_aca=anc)
        
        # déclaration du dictionnaire
        dict_note={}
        
        for etd in liste_etudiant:
            for note in liste_note:
                if note.student==etd:
                    dict_note[etd]=note.note
                    
        #print(dict_note)
        
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'liste_etudiant':liste_etudiant, 'matiere':matiere, 'niveau':niveau, 'departement':departement,'dict_note':dict_note}
        
        return render(request, 'saisie_note.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def modifier_Note(request,idMatiere, departement, niveau):
    """
    """
    liste_note=False
    logged_user=form_user(request)
    if logged_user:
        if request.POST:
            liste_etudiant=Etudiant.objects.filter(departement__intitule=departement,niveau__abrege=niveau, is_actif=True, is_ancien=False).order_by('nom', 'prenom')
            mat=Matiere.objects.get(id=idMatiere)
            coef=mat.coef
            dict_note={}
            etudiant=Etudiant.objects.filter(departement__intitule=departement,niveau__abrege=niveau, is_actif=True, is_ancien=False)[0]
        
            liste_note=Note.objects.filter(course=mat, anneeAca=etudiant.anneeAca, course__is_actif=True, is_actif=True)
            
            # Je commence par enregistrer tous les etudiants qui avaient des notes dans le table
            for etd in liste_etudiant:
                for note in liste_note:
                    if note.student==etd:
                        dict_note[etd]=note.note
            
            for item in liste_etudiant:
                
                if item in dict_note:
                    
                    for key, val in dict_note.items() :
                        
                        if item == key :
                            # vérification si l'étudiant ne dispose de note dans la matière depuis les semestres antérieures
                            
                            val_note=request.POST['note_'+str(item.id)]
                            
                            if val_note!='':
                                # SI ce n'est pas une chaine ou si la note n'est pas >20 ou <0
                                if val != val_note:
                                
                                    etd_note=Note.objects.get(student=item, course=mat, anneeAca=item.anneeAca)
                                
                                    etd_note.note=val_note
                                    etd_note.pondere=float(val_note) * float (coef) 
                                    etd_note.save()
                
                else :
                    val_note=request.POST['note_'+str(item.id)]
                    
                    if val_note!='':
                        
                        note=Note(note=val_note,pondere=float(val_note) * float (coef), student=item,course=mat,anneeAca=item.anneeAca )
                        note.save()
                    else : 
                        note=Note(note=0,pondere=0, student=item,course=mat,anneeAca=item.anneeAca )
                        note.save()      
                    
            
            #liste_note=Note.objects.filter(course__id=idMatiere, annee_aca=etd.anneeAca)
            #context={'liste_etudiant':liste_etudiant, 'matiere':mat, 'niveau':request.POST['niveau'], 'departement':request.POST['departement'],'liste_note':liste_note}
            
            # enregistrer une notification pour le secrétaire de jury 
            secretaire_jury=Formation_Jury.objects.get(jury__niveau__abrege=niveau, jury__departement__intitule=departement, fonction='Secrétaire')
            
            
            ens_secretaire=secretaire_jury.enseignant
            
            new=Notifications(enseignant=ens_secretaire, information="Les notes en "+mat.intitule+" ont été modifiée",url="http://localhost:8000/Delibération/consulter_note/<id>/", date=timezone_today())
            new.save()
           
                
            message=" Les notes ont été enrégistrées avec succès !"
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'message':message, 'matiere':mat, 'niveau':request.POST['niveau'], 'departement':request.POST['departement']}
            
            return render(request, 'note_saisie_succes.html', context)
        
        #liste_note=Note.objects.filter()
        
        matiere=Matiere.objects.get(id=idMatiere)
        liste_etudiant=Etudiant.objects.filter(departement__intitule=departement,niveau__abrege=niveau,is_actif=True, is_ancien=False).order_by('nom', 'prenom')
        
        # recupération d'un étudiant pour avoir l'année académique
        etudiant=Etudiant.objects.filter(departement__intitule=departement,niveau__abrege=niveau, is_actif=True, is_ancien=False)[0]
        
        # recupération des notes pour les étudiants qui ont déja une note dans la matiere
        
        # declaration de dictionnaire pour contenir les notes des étudiants
        dict_note={}
        
        liste_note=Note.objects.filter(course=matiere, anneeAca=etudiant.anneeAca, is_actif=True)
        #liste_note=Note.objects.filter(course__id=idMatiere, annee_aca=anc)
        
        for etd in liste_etudiant:
            for note in liste_note:
                if note.student==etd:
                    dict_note[etd]=note.note
                    
        #print(dict_note)
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'liste_etudiant':liste_etudiant, 'matiere':matiere, 'niveau':niveau, 'departement':departement,'dict_note':dict_note}
        
        return render(request, 'modifier_note.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def consulter_Note(request, id_matiere, depart, niveau):
    """
    
    """
    logged_user=form_user(request)
    if logged_user:
        
        # recupération d'un étudiant pour avoir l'année académique
        etudiant=Etudiant.objects.filter(departement__intitule=depart,niveau__abrege=niveau, is_actif=True)[0]
        
        mat=Matiere.objects.get(id=id_matiere)
        liste_etudiant=Etudiant.objects.filter(departement__intitule=depart,niveau__abrege=niveau, anneeAca=etudiant.anneeAca, is_ancien=False, is_actif=True).order_by('nom', 'prenom')
        
        liste_note=Note.objects.filter(course=mat, anneeAca=etudiant.anneeAca,course__is_actif=True, is_actif=True)
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'liste_etudiant':liste_etudiant, 'matiere':mat, 'niveau':niveau, 'departement':depart,'liste_note':liste_note}
        
        return render(request, 'consulter_note.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def Consulter_note_form(request):
    """
    
    """
    if request.POST:
        return True
    
    form_saisie_note=Saisie_note()
    
    context={'form_saisie_note':form_saisie_note}
    
    return render(request, 'form_saisir_note.html', context)

def detail_cursus(request, departement, niveau, annee, etudiant):
    """
    pour donner les details d'un cursus d'un étudiant
    """
    logged_user=form_user(request)
    if logged_user:
        """
        """
        
        # recupération de l'étudiant en question
        etd=Etudiant.objects.get(id=etudiant)
        
        ## Recuération du Cursus ##"
        
        termine =True
        
        cursus = Cursus.objects.get(anneeAca__an = annee, niveau__abrege=niveau, departement__intitule = departement, etudiant = etd)
        if cursus.statut == "En cours":
            termine =False
        else:
            termine = True
        
        
        liste_matiere=Matiere.objects.filter(uniteEnseign__niveau__abrege=niveau, uniteEnseign__departement__intitule=departement, is_actif=True)
        
        # liste des notes de l'étudiant dans les différentes matières
        
        liste_note=Note.objects.filter(course__uniteEnseign__niveau__abrege=niveau, course__uniteEnseign__departement__intitule=departement, anneeAca__an=annee, student=etd) #, course__is_actif=True
        
        # les informations du tableau de bord
        if logged_user.type_user == 'Enseignant':
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            nbre_notif=len(liste_notif)
            context={'etudiant':etd, 'niveau':niveau, 'departement': departement, 'annee' : annee,'liste_jury':liste_jury,
                 'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'etudiant':etd, 
                 'liste_matiere':liste_matiere, 'liste_note':liste_note, 'termine':termine, 'idC':cursus.id}
        
        
        else:
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            context={'etudiant':etd, 'niveau':niveau, 'departement': departement, 'annee' : annee,
                 'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'etudiant':etd, 
                 'liste_matiere':liste_matiere, 'liste_note':liste_note, 'termine':termine, 'idC':cursus.id}
        
        
        
        return render(request, 'detail_cursus.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def consulter_Jury(request, idJury):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        
        jury=Jury.objects.get(id=idJury)
        liste_membre_jury=Formation_Jury.objects.filter(jury__id=idJury)
        
        # les informations du tableau de bord
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'liste_membre_jury':liste_membre_jury, 'jury':jury}
        return render(request, 'detail_jury.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def liste_Jury(request):
    """
    """ 
    logged_user=form_user(request)
    if logged_user:
        liste_jury_ens=''
        if logged_user.titre == 'Chef de département':
            liste_jury_ens=Jury.objects.filter(departement=logged_user.departement)
            
        elif logged_user.titre == 'Directeur':
            liste_jury_ens=Jury.objects.filter(departement__ufr=logged_user.departement.ufr, is_actif=True)
            
        # les informations du tableau de bord
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'liste_jury_ens':liste_jury_ens}
        return render(request, 'liste_jury.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def liste_Jury_par_ens(request):
    """
    """ 
    logged_user=form_user(request)
    if logged_user:
        
        # les informations du tableau de bord
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user, jury__is_actif=True)
        
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif}
        return render(request, 'liste_jury_par_ens.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
  
def consulter_Membre_Jury(request, idJury):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        
        jury=Jury.objects.get(id=idJury)
        liste_membre_jury=Formation_Jury.objects.filter(jury=jury)
        
        # les informations du tableau de bord
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'liste_membre_jury':liste_membre_jury, 'jury':jury}
        return render(request, 'liste_membre.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
 
def del_Jury(request, idJury):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        jury=Jury.objects.get(id=idJury)
        jury.is_actif=False
        
        jury.save()
        
        return redirect('/deliberation/liste/jury')
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def get_Jury(request, idJury):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        jury=Jury.objects.get(id=idJury)
        jury.is_actif=True
        
        jury.save()
        
        return redirect('/deliberation/liste/jury')
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def modif_Jury(request, idJury):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        jury=Jury.objects.get(id=idJury)
        form=Formation_Jury.objects.filter(jury=jury)
        creation_jury=Creer_jury(data=request.POST or None, instance=jury)
        formation_membre=Membre_jury(data=request.POST or None)
        
        if request.POST :
            if creation_jury.is_valid() and formation_membre.is_valid():
                if creation_jury.has_changed():
                    creation_jury.save()
                
                nbre=creation_jury.nbre_membre
                
                for item in form:
                    item.delete()
                    new=Notifications(enseignant=item.enseignant, information="Vous avez été ajouté aux membres d'un jury avec la fonction de : "+item.fonction,url="http://localhost:8000/deliberation/retirer-de-jury/<creation_jury.id>/", date=timezone_today())
                    new.save()
                    
                
                formation_membre.jury=creation_jury
                for i in range(1,nbre+1):
                    
                    ens=formation_membre.cleaned_data['ens'+str(i)]
                    fonct=formation_membre.cleaned_data['fonction'+str(i)]
                    
                    m= Formation_Jury(enseignant=ens,jury=formation_membre.jury, fonction=fonct)
                    new=Notifications(enseignant=ens, information="Vous avez été ajouté aux membres d'un jury avec la fonction de : "+fonct,url="http://localhost:8000/deliberation/affiche_jury/<creation_jury.id>/", date=timezone_today())
                   
                    m.save()
                    new.save()
                    
                    user =ens.compte
                    if fonct=='Membre':
                        membre_jury=Group.objects.get(name='membre_jury')
                        user.groups.add(membre_jury)
                    
                    elif fonct=='Secrétaire':
                        secretaire_jury=Group.objects.get(name='secretaire_jury')
                        user.groups.add(secretaire_jury)
                        
                    elif fonct=='Président':
                        president_jury=Group.objects.get(name='president_jury')
                        user.groups.add(president_jury)
                        
                    ens.save()
                    
                    mail_envoye=envoyer_Mail_Affectation(type_mail='Jury', destinataire=formation_membre.cleaned_data['ens'+str(i)].compte.email)
                    
                print(mail_envoye)
                detail='La modification du jury de délibération '+creation_jury.cleaned_data['intitule']+' du niveau '+str(creation_jury.cleaned_data['niveau'])+' departement '+str(creation_jury.cleaned_data['departement'])+' à été effectuée avec succès '
                envoi_Admin_Aff_Jury(type_mail = 'Jury', detail =detail, mail_envoye = mail_envoye)
                
            return redirect('/deliberation/liste/jury')
                
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'creation_jury':creation_jury, 'formation_membre':formation_membre }
        
        return render(request, 'modif_jury.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def deliberer(request, idJury):
    """
    """
    logged_user= form_user(request)
    button=False
    liste_note=''
    liste_matiere=list()
    liste_etudiant=''
    liste_ajourne=list()
    liste_admis=list()
    nbre_note_save=0
    nbre_matiere_total=0
    dict_mat_save={}
    if logged_user:
        
        header=HeaderForm(data=request.POST or None)
        
        jury=Jury.objects.get(id=idJury)
        
        niv=jury.niveau
        dep=jury.departement
        Niv = Niveau.objects.get(abrege=niv)
        Depart=Departement.objects.get(intitule=dep)
        print(niv)
        print(dep)
        
        # récupération de la liste des étudiants
        
        liste_etudiant=Etudiant.objects.filter(is_actif=True, niveau=niv, departement=dep, is_ancien=False).order_by('nom', 'prenom')
        
        print(liste_etudiant)
        liste_matiere=Matiere.objects.filter(is_actif=True, uniteEnseign__niveau=niv, uniteEnseign__departement=dep).order_by('intitule')
        liste_ue=UE.objects.filter(is_actif=True, niveau=niv, departement=dep).order_by('intitule')
        
        nbre_matiere_total=len(liste_matiere)
        
        # récupération du nombre de matière dont les notes sont sauvégardé
        
        
        
        if len(liste_etudiant)!=0:
            
            etudiant=Etudiant.objects.filter(is_actif=True, is_ancien=False, niveau=niv, departement=dep)[0]
            
            an_ac=etudiant.anneeAca
            
            #récupération de la liste des matières
            liste_note_save=Note_Save.objects.filter(is_actif=True, anneeaca=an_ac, matiere__uniteEnseign__niveau=niv, matiere__uniteEnseign__departement=dep)
            nbre_note_save=len(liste_note_save)
            
            
            # récupération de la liste des notes
            
            liste_note=Note.objects.filter(anneeAca=an_ac, student__niveau=niv, student__departement=dep, course__is_actif=True,
                                            course__uniteEnseign__niveau=niv, course__uniteEnseign__departement=dep, is_actif=True)
            
            
            i=0
            for item in liste_note:
                if item.course in dict_mat_save:
                    i=i
                else :
                    
                    i=i+1
                    dict_mat_save[item.course]=i
            
            
            if nbre_note_save < nbre_matiere_total :
                button=False
            else :
                button=True
        
        if request.POST:
            
            # recherche du nombre de colonne pour les ue et ec
            n_colonne=0 ### le nombre de colonne pour les feuilles de PV
            n_col=0 ###Le nombre de colonne pour la grande feuille excel
            for ue in liste_ue :
                compteur=0
                for ec in liste_matiere:
                   
                    if ec.uniteEnseign == ue :
                        compteur = compteur +1
                
                if compteur == 1:
                    
                    n_colonne= n_colonne + 2
                    n_col=n_col+4
                elif compteur > 1:
                    n_colonne = n_colonne + compteur + 2
                    n_col=n_col+compteur+4
            
            # ajout de total et de moy + result
            
            n_colonne = n_colonne +3 + 6
            n_col=n_col+3+6
            ### Déclaration de dictionnaire pour les entềtes 
            
            dict_lettre={}
            
            dict_lettre['A']=1
            dict_lettre['B']=2
            dict_lettre['C']=3
            dict_lettre['D']=4
            dict_lettre['E']=5
            dict_lettre['F']=6
            dict_lettre['G']=7
            dict_lettre['H']=8
            dict_lettre['I']=9
            dict_lettre['J']=10
            dict_lettre['K']=11
            dict_lettre['L']=12
            dict_lettre['M']=13
            dict_lettre['N']=14
            dict_lettre['O']=15
            dict_lettre['P']=16
            dict_lettre['Q']=17
            dict_lettre['R']=18
            dict_lettre['S']=19
            dict_lettre['T']=20
            dict_lettre['U']=21
            dict_lettre['V']=22
            dict_lettre['W']=23
            dict_lettre['X']=24
            dict_lettre['Y']=25
            dict_lettre['Z']=26
            dict_lettre['AA']=27
            dict_lettre['AB']=28
            dict_lettre['AC']=29
            dict_lettre['AD']=30
            dict_lettre['AE']=31
            dict_lettre['AF']=32
            dict_lettre['AG']=33
            dict_lettre['AH']=34
            dict_lettre['AI']=35
            dict_lettre['AJ']=36
            dict_lettre['AK']=37
            dict_lettre['AL']=38
            dict_lettre['AM']=39
            dict_lettre['AN']=40
            dict_lettre['AO']=41
            dict_lettre['AP']=42
            dict_lettre['AQ']=43
            dict_lettre['AR']=44
            dict_lettre['AS']=45
            dict_lettre['AT']=46
            dict_lettre['AU']=47
            dict_lettre['AV']=48
            dict_lettre['AW']=49
            dict_lettre['AX']=50
            dict_lettre['AY']=51
            dict_lettre['AZ']=52
            
            if header.is_valid():
                type_delib=header.cleaned_data['type_delib']
                
                print(header.cleaned_data['imp_pv'])
                print(header.cleaned_data['imp_pvd'])
                print(header.cleaned_data['imp_jur'])
                print(header.cleaned_data['imp_adm'])
                # Calcul de la moyenne de chaque etudiant
                for  etd in liste_etudiant :
                    
                    etd_pon=0
                    coef=0
                    for note in liste_note:
                        if etd == note.student:
                            
                            etd_pon+=note.pondere
                            coef+=note.course.coef
                        
                    
                    etd_moy=float(etd_pon)/int(coef)
                
                    if etd_moy < 10:
                        liste_ajourne.append(etd)
                    elif etd_moy >= 10:
                        liste_admis.append(etd)
                        
                """
                fin de la verification si l'etudiant est admis ou pas.
                """
                
                session = header.cleaned_data['session']
                
                liste_membre_jury=Formation_Jury.objects.filter(jury__id=idJury)
                dict_admis={}
                """
                Ecriture du fichier excel : les 4 feuilles excel
                """
                
                # déclaration d'un fichier excel
                
                wb =Workbook()
                uf=Depart.ufr.intitule
                opt=Option.objects.get(intitule = header.cleaned_data['option'])
                abr=Depart.ufr.code
                # mise en forme
                thin = Side(border_style="thin", color="000000")
                #double = Side(border_style="double", color="ff0000")
                
                if header.cleaned_data['imp_pv'] :
                    
                    # déclaration de la prémière feuille
                    sheet=wb.active
                    
                    sheet.title = "PV de délibération"
                    
                    sheet.sheet_properties.tabColor = "AE72BA"
                    
                    """ 
                    Entête du fichier 
                    """
                    
                    sheet['A1'].value = header.cleaned_data['universite']                           
                    sheet['B2'].value = "---------------"
                    sheet['A3'].value = "UNITE DE FORMATION ET DE RECHERCHE EN"  
                    uf=Depart.ufr.intitule
                    sheet['A4'].value =uf.upper()
                    sheet['B5'].value = "---------------"
                    sheet['B6'].value = header.cleaned_data['service']
                    
                
                    sheet.cell(row=3,column=n_colonne-3,value="Année Universitaire : "+str(an_ac))
                    #sheet['U3'].value = "Année Universitaire : "
                    sheet.cell(row=5, column=n_colonne-2, value=str(header.cleaned_data['session'] ))
                    #sheet['V5'].value = header.cleaned_data['session'] 
                    ma_d_col=''       
                    for key, val in dict_lettre.items():
                        if val == n_colonne:
                            ma_d_col=key
                            break
                            
                    sheet.merge_cells('A7:'+ma_d_col+'7')
                    titre=sheet['A7']
                    titre.value = header.cleaned_data['titre']
                    
                    
                    
                    titre.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    titre.fill = PatternFill("solid", fgColor="DDDDDD")
                    titre.fill = GradientFill(stop=("000000", "FFFFFF"))
                    titre.font  = Font(b=True, color="ffffff")
                    titre.alignment = Alignment(horizontal="center", vertical="center")
                    
                    opt=Option.objects.get(intitule = header.cleaned_data['option'])
                    sheet.merge_cells('A8:'+ma_d_col+'8')
                    posi=sheet['A8']
                    posi.value = Niv.intitule+' '+str(dep)+' - Option : '+opt.intitule+' ('+opt.abrege+')'
                    posi.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    posi.fill = PatternFill("solid", fgColor="DDDDDD")
                    posi.fill = GradientFill(stop=("000000", "FFFFFF"))
                    posi.font  = Font(b=True, color="ffffff")
                    posi.alignment = Alignment(horizontal="center", vertical="center")
                    
                    
                    ## Les champs des etudiants
                    
                    sheet['A12'].value = 'N°'
                    sheet['A12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet['A12'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet['A12'].font  = Font(b=True, color="000000")
                    
                    sheet['B12'].value = 'Nom'
                    sheet['B12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet['B12'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet['B12'].font  = Font(b=True, color="000000")
                    
                    sheet['C12'].value = 'Prénom(s)'
                    sheet['C12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet['C12'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet['C12'].font  = Font(b=True, color="000000")
                    
                    sheet['D12'].value = 'Date de Naissance'
                    sheet['D12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet['D12'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet['D12'].font  = Font(b=True, color="000000")
                    
                    sheet['E12'].value = 'Lieu de Naissance'
                    sheet['E12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet['E12'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet['E12'].font  = Font(b=True, color="000000")
                    
                    sheet['F12'].value = 'Matricule'
                    sheet['F12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet['F12'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet['F12'].font  = Font(b=True, color="000000")
                    
                    #### La ligne des crédits 
                    
                    sheet['F11'].value = "Crédits"
                    sheet['F11'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet['F11'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet['F11'].font  = Font(b=True, color="000000")
                    
                    ###### Les champs des ec, ue, note et moy
                    
                    ma_col=''
                    col_sui=''
                    col_fin=''
                    deb = 7
                    for ue in liste_ue :
                        nb=0
                        for ec in liste_matiere:
                            if ec.uniteEnseign == ue :
                                nb=nb+1
                        
                        ######## Les cases en fonctions du nombre de ec 
                        
                        
                        if nb == 1:
                            for key, val in dict_lettre.items():
                                if val == deb:
                                    ma_col=key
                                    break
                                
                            for key, val in dict_lettre.items():
                                if val == deb+1:
                                    col_sui=key
                                    break
                            
                            sheet.merge_cells(ma_col+'9:'+col_sui+'10')
                            d = sheet[ma_col+'9']
                            d.value = ue.intitule
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            d.alignment = Alignment(horizontal="center", vertical="center")
                            d.font  = Font(b=True, color="000000")
                            
                            sheet[ma_col+'11'].value = ue.coef
                            sheet[ma_col+'11'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            sheet[ma_col+'11'].alignment = Alignment(horizontal="center", vertical="center")
                            sheet[ma_col+'11'].font  = Font(b=True, color="000000")
                             
                            sheet[ma_col+'12'].value = "NOTE UE"
                            sheet[ma_col+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            sheet[ma_col+'12'].alignment = Alignment(horizontal="center", vertical="center")
                            sheet[ma_col+'12'].font  = Font(b=True, color="000000")
                            
                            sheet.merge_cells(col_sui+'11:'+col_sui+'12')
                            
                            d=sheet[col_sui+'11']
                            d.value = "POND"
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            d.alignment = Alignment(horizontal="center", vertical="center")
                            d.font  = Font(b=True, color="000000")
                            
                            deb=deb+2
                        
                        #### Cas ou le nombre de ec est equivalent a 2
                        elif nb == 2:
                            for key, val in dict_lettre.items():
                                if val == deb:
                                    ma_col=key
                                    break
                                
                            for key, val in dict_lettre.items():
                                if val == deb+3:
                                    col_fin=key
                                    break
                            sheet.merge_cells(ma_col+'9:'+col_fin+'9')
                            d=sheet[ma_col+'9']
                            d.value = ue.intitule
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            d.alignment = Alignment(horizontal="center", vertical="center")
                            d.font  = Font(b=True, color="000000")
                            
                            cpter = deb
                            for ec in liste_matiere:
                                if ec.uniteEnseign == ue :
                                    for key, val in dict_lettre.items():
                                        if val == cpter:
                                            ma_col=key
                                            break
                                    sheet[ma_col+'10'].value = ec.intitule
                                    sheet[ma_col+'10'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    sheet[ma_col+'10'].alignment = Alignment(horizontal="center", vertical="center")
                                    sheet[ma_col+'10'].font  = Font(b=True, color="000000")
                                    
                                    sheet[ma_col+'11'].value = ec.coef
                                    sheet[ma_col+'11'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    sheet[ma_col+'11'].alignment = Alignment(horizontal="center", vertical="center")
                                    sheet[ma_col+'11'].font  = Font(b=True, color="000000")
                                    
                                    sheet[ma_col+'12'].value = "NOTE"
                                    sheet[ma_col+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    sheet[ma_col+'12'].alignment = Alignment(horizontal="center", vertical="center")
                                    sheet[ma_col+'12'].font  = Font(b=True, color="000000")
                                    
                                    cpter=cpter +1
                                    
                            for key, val in dict_lettre.items():
                                if val == deb+2:
                                    ma_col=key
                                    break
                            
                            sheet.merge_cells(ma_col+'10:'+ma_col+'12')
                            d=sheet[ma_col+'10']
                            d.value = "MOY UE"
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            d.alignment = Alignment(horizontal="center", vertical="center")
                            d.font  = Font(b=True, color="000000")
                            
                            for key, val in dict_lettre.items():
                                if val == deb+3:
                                    ma_col=key
                                    break
                            
                            sheet.merge_cells(ma_col+'10:'+ma_col+'12')
                            d=sheet[ma_col+'10']
                            d.value = "POND"
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            d.alignment = Alignment(horizontal="center", vertical="center")
                            d.font  = Font(b=True, color="000000")
                            
                            deb =deb+4
                            ############ Cas ou le nombre de ec est equivalent a 3
                        elif nb == 3:
                            for key, val in dict_lettre.items():
                                if val == deb:
                                    ma_col=key
                                    break
                                
                            for key, val in dict_lettre.items():
                                if val == deb+4:
                                    col_fin=key
                                    break
                            sheet.merge_cells(ma_col+'9:'+col_fin+'9')
                            d=sheet[ma_col+'9']
                            d.value = ue.intitule
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            d.alignment = Alignment(horizontal="center", vertical="center")
                            d.font  = Font(b=True, color="000000")
                            
                            cpter = deb
                            for ec in liste_matiere:
                                if ec.uniteEnseign == ue :
                                    for key, val in dict_lettre.items():
                                        if val == cpter:
                                            ma_col=key
                                            break
                                    sheet[ma_col+'10'].value = ec.intitule
                                    sheet[ma_col+'10'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    sheet[ma_col+'10'].alignment = Alignment(horizontal="center", vertical="center")
                                    sheet[ma_col+'10'].font  = Font(b=True, color="000000")
                                    
                                    sheet[ma_col+'11'].value = ec.coef
                                    sheet[ma_col+'11'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    sheet[ma_col+'11'].alignment = Alignment(horizontal="center", vertical="center")
                                    sheet[ma_col+'11'].font  = Font(b=True, color="000000")
                                    
                                    sheet[ma_col+'12'].value = "NOTE"
                                    sheet[ma_col+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    sheet[ma_col+'12'].alignment = Alignment(horizontal="center", vertical="center")
                                    sheet[ma_col+'12'].font  = Font(b=True, color="000000")
                                    
                                    cpter=cpter +1
                                    
                            for key, val in dict_lettre.items():
                                if val == deb+3:
                                    ma_col=key
                                    break
                            
                            sheet.merge_cells(ma_col+'10:'+ma_col+'12')
                            d=sheet[ma_col+'10']
                            d.value = "MOY UE"
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            d.alignment = Alignment(horizontal="center", vertical="center")
                            d.font  = Font(b=True, color="000000")
                            
                            for key, val in dict_lettre.items():
                                if val == deb+4:
                                    ma_col=key
                                    break
                            
                            sheet.merge_cells(ma_col+'10:'+ma_col+'12')
                            d=sheet[ma_col+'10']
                            d.value = "POND"
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            d.alignment = Alignment(horizontal="center", vertical="center")
                            d.font  = Font(b=True, color="000000")
                            
                            deb=deb+5
                        
                        
                        ####### Partie des totaux, moy et result
                        
                    for key, val in dict_lettre.items():
                        if val == deb:
                            ma_col=key
                            break
                    
                    sheet.merge_cells(ma_col+'9:'+ma_col+'10')    
                    d=sheet[ma_col+'9']
                    d.value = ""
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    Tcoef=0
                    for ec in liste_matiere:
                        Tcoef =Tcoef + ec.coef
                    
                    sheet[ma_col+'11'].value = Tcoef
                    sheet[ma_col+'11'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet[ma_col+'11'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet[ma_col+'11'].font  = Font(b=True, color="000000")
                    
                    sheet[ma_col+'12'].value = "TOTAL"
                    sheet[ma_col+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet[ma_col+'12'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet[ma_col+'12'].font  = Font(b=True, color="000000")
                    
                    
                    for key, val in dict_lettre.items():
                        if val == deb+1:
                            ma_col=key
                            break
                        
                    for key, val in dict_lettre.items():
                        if val == deb+2:
                            col_sui=key
                            break
                    
                    sheet.merge_cells(ma_col+'9:'+col_sui+'11')
                    d=sheet[ma_col+'9']
                    d.value = ""
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    
                    sheet[ma_col+'12'].value = "Moy."
                    sheet[ma_col+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet[ma_col+'12'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet[ma_col+'12'].font  = Font(b=True, color="000000")
                    
                    sheet[col_sui+'12'].value = "Résult."
                    sheet[col_sui+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    sheet[col_sui+'12'].alignment = Alignment(horizontal="center", vertical="center")
                    sheet[col_sui+'12'].font  = Font(b=True, color="000000")
                    
                    
                    ######### La signature ######
                    
                    nbre_etd=len(liste_etudiant)
                    
                    ctre = int(n_colonne/2)
                    
                    for key, val in dict_lettre.items():
                        if val == ctre:
                            col_sui=key
                            break
                        
                    deca =nbre_etd+2+12
                    sheet[col_sui+''+str(deca)].value = "Ouagadougou, le "+header.cleaned_data['date_delib']
                    sheet[col_sui+''+str(deca)].font  = Font(b=True, color="000000")
                    
                    #### Récupération de la liste des membres du jury #####
                    
                    liste_membre_jury=Formation_Jury.objects.filter(jury__id=idJury)
                    
                    deca = deca+1
                    
                    sheet['B'+str(deca)].value = "Membres du jury"
                    cpter = deca
                    for mbre in liste_membre_jury :
                        if mbre.fonction != "Président" :
                            cpter=cpter+1
                            sheet['B'+str(cpter)].value = mbre.enseignant.nom+' '+mbre.enseignant.prenom
                    
                    
                    
                    for key, val in dict_lettre.items():
                        if val == n_colonne-2:
                            col_sui=key
                            break
                    
                    sheet[col_sui+''+str(deca)].value = "Président du jury"
                    cpter=deca
                    for mbre in liste_membre_jury :
                        if mbre.fonction == "Président" :
                            cpter=cpter+1
                            sheet[col_sui+''+str(cpter)].value = mbre.enseignant.nom+' '+mbre.enseignant.prenom
                    
                    
                    ##### Renseigner chaque étudiant et ses notes, sa moyenne et son rang
                    
                    debut =12
                    i=0
                    
                    dict_admis={} ### Le dictionnaire qui va contenir les admis
                    for etd in liste_etudiant :
                        debut=debut+1
                        i=i+1
                        sheet['A'+str(debut)].value = i
                        sheet['A'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        sheet['A'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        sheet['B'+str(debut)].value = etd.nom
                        sheet['B'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        sheet['B'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        sheet['C'+str(debut)].value = etd.prenom
                        sheet['C'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        sheet['C'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        sheet['D'+str(debut)].value = etd.date_naissance
                        sheet['D'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        sheet['D'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        sheet['E'+str(debut)].value = etd.lieu_naissance
                        sheet['E'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        sheet['E'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        sheet['F'+str(debut)].value = etd.matricule
                        sheet['F'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        sheet['F'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        pos=7 ### La dernière position d'ajout de note
                        ma_col=''
                        ma_col_sui=''
                        total_note=0
                        total_coef=0
                        moy=0
                        for ue in liste_ue:
                            pond_ue=0
                            nbre = 0 ### Connaitre le nombre de ec qui compose l'ue
                            for ec in liste_matiere:
                                if ec.uniteEnseign == ue :
                                    nbre = nbre+1
                            
                            #### afficher les notes des ec, ue en foncion du nombre de ec dans chaque ue
                            if nbre == 1:
                                for key, val in dict_lettre.items():
                                    if val == pos:
                                        ma_col=key
                                        break
                                
                                for key, val in dict_lettre.items():
                                    if val == pos+1:
                                        ma_col_sui=key
                                        break
                                
                                for note in liste_note : ### Effectuer un parcours pour afficher les notes
                                    if note.course.uniteEnseign == ue and note.student == etd :
                                        sheet[ma_col+''+str(debut)].value = note.note
                                        sheet[ma_col+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                        sheet[ma_col+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                        
                                        sheet[ma_col_sui+''+str(debut)].value = note.pondere
                                        sheet[ma_col_sui+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                        sheet[ma_col_sui+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                        
                                        total_note=total_note+note.pondere
                                        
                                        
                                pos=pos+2 ### Reinitialisation de la position
                            elif nbre == 2:
                                cpter=pos
                                
                                for ec in liste_matiere: ### a la recherche des ec appartenant à l'ue
                                    if ec.uniteEnseign == ue:
                                        for note in liste_note:
                                            if note.course == ec and note.student == etd: ### recherhe de la note de l'etudiant dans cette matière
                                                for key, val in dict_lettre.items():
                                                    if val == cpter:
                                                        ma_col=key
                                                        break  
                                                ### J'affiche la note trouvée
                                                sheet[ma_col+''+str(debut)].value = note.note
                                                sheet[ma_col+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                                sheet[ma_col+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                                
                                                cpter = cpter+1 ### Décalage du compteur
                                                pond_ue=pond_ue+note.pondere
                                                
                                                total_note=total_note+note.pondere ### Ajout de la note pondérée au total des notes pondérées
                                
                                ### Enregistrement des notes des ec est terminé
                                
                                moy_ue=float(pond_ue/ue.coef)
                                
                                for key, val in dict_lettre.items(): ### recupération de la position de la moyenne de l'UE
                                    if val == pos+2:
                                        ma_col=key
                                        break
                                    
                                for key, val in dict_lettre.items(): ### récuperation position de pondérée de l'ue
                                    if val == pos+3:
                                        ma_col_sui=key
                                        break 
                                
                                sheet[ma_col+''+str(debut)].value = format(moy_ue, '0.2f')
                                sheet[ma_col+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                sheet[ma_col+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                                    
                                sheet[ma_col_sui+''+str(debut)].value = pond_ue
                                sheet[ma_col_sui+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                sheet[ma_col_sui+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                
                                pos=pos+4
                                
                            elif nbre == 3:
                                cpter=pos
                                
                                for ec in liste_matiere: ### a la recherche des ec appartenant à l'ue
                                    if ec.uniteEnseign == ue:
                                        for note in liste_note:
                                            if note.course == ec and note.student == etd: ### recherhe de la note de l'etudiant dans cette matière
                                                for key, val in dict_lettre.items():
                                                    if val == cpter:
                                                        ma_col=key
                                                        break  
                                                ### J'affiche la note trouvée
                                                sheet[ma_col+''+str(debut)].value = note.note
                                                sheet[ma_col+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                                sheet[ma_col+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                                
                                                cpter = cpter+1 ### Décalage du compteur
                                                pond_ue=pond_ue+note.pondere
                                                
                                                total_note=total_note+note.pondere ### Ajout de la note pondérée au total des notes pondérées
                                
                                ### Enregistrement des notes des ec est terminé
                                
                                moy_ue=float(pond_ue/ue.coef)
                                
                                for key, val in dict_lettre.items(): ### recupération de la position de la moyenne de l'UE
                                    if val == pos+3:
                                        ma_col=key
                                        break
                                    
                                for key, val in dict_lettre.items(): ### récuperation position de pondérée de l'ue
                                    if val == pos+4:
                                        ma_col_sui=key
                                        break 
                                
                                sheet[ma_col+''+str(debut)].value = format(moy_ue, '0.2f')
                                sheet[ma_col+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                sheet[ma_col+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                                    
                                sheet[ma_col_sui+''+str(debut)].value = pond_ue
                                sheet[ma_col_sui+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                sheet[ma_col_sui+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                
                                pos=pos+5
                                  
                                
                            total_coef=total_coef+ue.coef
                        
                        ### Fin de l'affichage des notes 
                        
                        #### Affichage du total des notes pondédérées suivie du calcul de la Moyenne et du rang
                        for key, val in dict_lettre.items(): ### récuperation position de pondérée de l'ue
                            if val == pos:
                                ma_col=key
                                break 
                        sheet[ma_col+''+str(debut)].value = total_note
                        sheet[ma_col+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        sheet[ma_col+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        for key, val in dict_lettre.items(): ### récuperation position de pondérée de l'ue
                            if val == pos+1:
                                ma_col=key
                                break 
                        moy=total_note/total_coef
                        sheet[ma_col+''+str(debut)].value =format(moy, '0.2f')
                        sheet[ma_col+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        sheet[ma_col+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        for key, val in dict_lettre.items(): ### récuperation position de pondérée de l'ue
                            if val == pos+2:
                                ma_col=key
                                break 
                        
                        statut=''
                        if moy >= 10 :
                            statut = 'Admis(e)'
                            dict_admis[etd]=float(format(moy,'0.2f'))
                            
                        elif moy < 10:
                            statut= 'Ajourné(e)'
                            
                        sheet[ma_col+''+str(debut)].value = statut
                        sheet[ma_col+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        sheet[ma_col+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                
                """
                Partie de classement des étudiants
                """
                
                dict_admis_cl=dict_admis.copy()
                list_class=list()
                
                val_init=0
                while dict_admis_cl:
                    for key, val in dict_admis_cl.items():
                    
                        etd_ab=key
                        val_init=val
                        
                        for key1, val1 in dict_admis_cl.items():
                            if val_init < val1:
                                val_init=val1
                                etd_ab = key1
                                
                        list_class.append(etd_ab)
                        break
                    
                    del dict_admis_cl[etd_ab]
                    
                
                ###### Fin du classement des étudiants ######
                    
                """
                La sauvegarde de la délibération effectuée
                
                Il faut cette sauvegarde pour garder une trace de la délibération effectuée
                """
                if type_delib == "Définitive" :
                    
                    n1=len(liste_admis)
                    n2=len(liste_etudiant)-n1
                    
                    deliberation=Deliberation(niveau=Niv, departement=Depart, anneeaca=an_ac,session=session,admis=n1, ajourne=n2 )
                    deliberation.save()
                
                
                """ 
                Fin de la sauvegarde de la trace de la deliberation effectuée
                """
                
                            
                #### La feuille pour la liste des admis(e) #############
                
                if header.cleaned_data['imp_adm']:
                    
                    ws=wb.create_sheet("Liste des admis")
                    ws.sheet_properties.tabColor = "AE72EE"    
                    
                    ## En tête du fichier de la liste des admis
                    
                    ws.merge_cells('B1:E1')
                    d=ws['B1']
                    d.value = header.cleaned_data['universite']
                    d.border = Border(top=thin, left=thin, right=thin)
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")  
                    
                    ws.merge_cells('B2:E2')
                    d=ws['B2']
                    d.value = "---------------"
                    d.border = Border(left=thin, right=thin)
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")  
                    
                    ws.merge_cells('B3:E3')
                    d=ws['B3']
                    d.value = "UNITE DE FORMATION ET DE RECHERCHE EN"  
                    d.border = Border(left=thin, right=thin)
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")  
                    
                    uf=Depart.ufr.intitule
                    abr=Depart.ufr.code
                    ws.merge_cells('B4:E4')
                    d=ws['B4']
                    d.value ='  '+uf.upper()+' (UFR-'+abr+')'
                    d.border = Border(left=thin, right=thin, bottom=thin)
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")  
                    
                    
                    ws.merge_cells('F1:H4')
                    d=ws['F1']
                    d.value = "Année Universitaire : "+str(an_ac)
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")  
                    
                    ### Le grand titre
                    
                    ws.merge_cells('A5:H5')
                    d=ws['A5']
                    d.value = "CONTRÔLE DES CONNAISSANCES ET EXAMENS"
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    ws['B6'].value = session.session
                    ws['B6'].font  = Font(b=True, color="000000")
                    ws['B6'].alignment = Alignment(vertical="center")
                    
                    ws['B7'].value = "Licence : "+str(niv)+' Parcours '+str(dep)
                    ws['B7'].font  = Font(b=True, color="000000")
                    ws['B7'].alignment = Alignment(vertical="center")
                    
                    ws.merge_cells('B8:H8')
                    ws['B8'].value = "Sont définitivement admis par ordre de mérite les candidats dont les noms suivent :"
                    ws['B8'].alignment = Alignment(vertical="center")
                    
                    #### Tableau des noms et nombre d'admis 
                    
                    ws['A9'].value = ''
                    ws['A9'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    ws['A9'].alignment = Alignment(horizontal="center", vertical="center")
                    
                    ws['B9'].value = 'Nom'
                    ws['B9'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    ws['B9'].alignment = Alignment(horizontal="center", vertical="center")
                    
                    ws['C9'].value = 'Prénom(s)'
                    ws['C9'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    ws['C9'].alignment = Alignment(horizontal="center", vertical="center")
                    
                    ws['D9'].value = 'Date de Naissance'
                    ws['D9'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    ws['D9'].alignment = Alignment(horizontal="center", vertical="center")
                    
                    ws['E9'].value = 'Lieu de Naissance'
                    ws['E9'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    ws['E9'].alignment = Alignment(horizontal="center", vertical="center")
                    
                    ws['F9'].value = 'Matricule'
                    ws['F9'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    ws['F9'].alignment = Alignment(horizontal="center", vertical="center")
                    
                    ws['G9'].value = 'Résultat'
                    ws['G9'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    ws['G9'].alignment = Alignment(horizontal="center", vertical="center")
                    
                    ws['H9'].value = 'Côte'
                    ws['H9'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    ws['H9'].alignment = Alignment(horizontal="center", vertical="center")
                    
                    ####### Parcours pour afficher les étudiants admis #########
                    deb=9
                    i=0
                    #for etd in liste_etudiant:
                    
                    for etd in list_class :
                        
                        
                        if etd in dict_admis:
                            deb=deb+1
                            i=i+1 ########" Pour faire le compte des étudiants admis ########
                            
                            ### Les différentes colonnes
                            ws['A'+str(deb)].value = i
                            ws['A'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            ws['A'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                            
                            ws['B'+str(deb)].value = etd.nom
                            ws['B'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            ws['B'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                            
                            ws['C'+str(deb)].value = etd.prenom
                            ws['C'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            ws['C'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                            
                            ws['D'+str(deb)].value = etd.date_naissance
                            ws['D'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            ws['D'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                            
                            ws['E'+str(deb)].value = etd.lieu_naissance
                            ws['E'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            ws['E'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                            
                            ws['F'+str(deb)].value = etd.matricule
                            ws['F'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            ws['F'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                            
                            ws['G'+str(deb)].value = 'Admis(e)'
                            ws['G'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            ws['G'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                            
                            for key, val in dict_admis.items():
                                if key == etd : ### Je cherche à recupérer la moyenne de l'étudiant et faire des contrôles la dessus
                                    if val >= 10 and val < 12:
                            
                                        ws['H'+str(deb)].value = 'D'
                                        ws['H'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                        ws['H'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                                        ws['H'+str(deb)].fill = PatternFill("solid", fgColor="CCCCCC")
                                       
                                    elif val >= 12 and val < 14:
                            
                                        ws['H'+str(deb)].value = 'C'
                                        ws['H'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                        ws['H'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                                        ws['H'+str(deb)].fill = PatternFill("solid", fgColor="CCCCCC")
                                        
                                    elif val >= 14 and val < 16:
                            
                                        ws['H'+str(deb)].value = 'B'
                                        ws['H'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                        ws['H'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                                        ws['H'+str(deb)].fill = PatternFill("solid", fgColor="CCCCCC")
                                        
                                    elif val >= 16 and val <= 20:
                            
                                        ws['H'+str(deb)].value = 'A'
                                        ws['H'+str(deb)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                        ws['H'+str(deb)].alignment = Alignment(horizontal="center", vertical="center")
                                        ws['H'+str(deb)].fill = PatternFill("solid", fgColor="CCCCCC")
                                    
                                    break
                    
                    ## Fin pour l'affichage des étudiants admis
                    
                    deb=deb+1
                    
                    ws['B'+str(deb)].value = "Arrêté la présente liste à "+str(i)+" noms."
                    
                    deb=deb+1
                    
                    ws.merge_cells('A'+str(deb)+':H'+str(deb))
                    d=ws['A'+str(deb)]
                    d.value = "Fait à Ouagadougou, le "+header.cleaned_data['date_delib']
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    
                    ######### Partie Jury ########
                    
                    deb=deb+1
                    
                    ws['B'+str(deb)].value = "Les Membres du Jury"
                    ws['B'+str(deb)].font  = Font(b=True, color="000000") #, underline = True
                    cpter=deb
                    for mbre in liste_membre_jury :
                        if mbre.fonction != "Président" :
                            cpter=cpter+1
                            ws['B'+str(cpter)].value = mbre.enseignant.nom+' '+mbre.enseignant.prenom
                    
                    
                    ws['F'+str(deb)].value = "Le Président du Jury"
                    ws['F'+str(deb)].font  = Font(b=True, color="000000") #underline=True, 
                    cpter=deb
                    for mbre in liste_membre_jury :
                        if mbre.fonction == "Président" :
                            cpter=cpter+1
                            ws['F'+str(cpter)].value = mbre.enseignant.nom+' '+mbre.enseignant.prenom
                    
                    
                    
                #### Fin de la liste des admis ####
                
                ##### Liste des membres du jury : 3 ème feuille #######
                
                if header.cleaned_data['imp_jur']:
                    
                    sh=wb.create_sheet("Liste des membres du jury")
                    sh.sheet_properties.tabColor = "EE72EE"
                    
                    ### En tête du fichier ###
                    
                    sh.merge_cells('A1:F1')
                    d=sh['A1']
                    d.value = header.cleaned_data['universite']
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    sh.merge_cells('G1:J1')
                    d=sh['G1']
                    d.value = "Année universitaire : "+str(an_ac)
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    sh.merge_cells('A2:F2')
                    d=sh['A2']
                    d.value = "---------------"
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    sh.merge_cells('A3:F3')
                    d=sh['A3']
                    d.value = "UNITE DE FORMATION ET DE RECHERCHE EN"
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    sh.merge_cells('A4:F4')
                    d=sh['A4']
                    d.value = uf.upper()
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    sh.merge_cells('A5:F5')
                    d=sh['A5']
                    d.value = "(UFR-"+abr+")"
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    sh.merge_cells('A6:F6')
                    d=sh['A6']
                    d.value = "---------------"
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    sh.merge_cells('A7:F7')
                    d=sh['A7']
                    d.value = "Adresse : "+header.cleaned_data['adresse']
                    d.alignment = Alignment(vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    sh.merge_cells('A8:F8')
                    d=sh['A8']
                    d.value = "Tel : "+header.cleaned_data['tel']
                    d.alignment = Alignment(vertical="center")
                    d.font  = Font(b=True, color="000000")    
                    
                    sh.merge_cells('A9:F9')
                    d=sh['A9']
                    d.value = "Fax : "+header.cleaned_data['fax']
                    d.alignment = Alignment(vertical="center")
                    d.font  = Font(b=True, color="000000")        
                    
                    sh.merge_cells('A10:F10')
                    d=sh['A10']
                    d.value = "Télex : "+header.cleaned_data['telex']
                    d.alignment = Alignment( vertical="center")
                    d.font  = Font(b=True, color="000000")  
                    
                    #### Fin des en tête des membres du jury
                    
                    ### Titre de l'affiche et corps ####
                    
                    sh.merge_cells('A11:J11')
                    d=sh['A11']
                    d.value = "PROCES-VERBAL D'EXAMEN"
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, size=16, color="000000") 
                    
                    sh.merge_cells('A12:J12')
                    d=sh['A12']
                    d.value = "---------------"
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, size=14, color="000000")  
                    
                    sh.merge_cells('A13:E13')
                    d=sh['A13']
                    d.value = "Session : "
                    d.alignment = Alignment(vertical="center")
                    
                    sh.merge_cells('F13:J13')
                    d=sh['F13']
                    d.value = session.session+' '+str(an_ac)
                    d.alignment = Alignment(vertical="center")
                    
                    
                    sh.merge_cells('A14:E14')
                    d=sh['A14']
                    d.value = "Examen de : "
                    d.alignment = Alignment(vertical="center")
                    
                    sh.merge_cells('F14:J14')
                    d=sh['F14']
                    d.value = str(niv)+' - LICENCE '+str(dep)
                    d.alignment = Alignment(vertical="center")
                    
                    ### Composition du jury
                    
                    sh.merge_cells('A15:J15')
                    d=sh['A15']
                    d.value = "Composition du jury : "
                    d.alignment = Alignment(horizontal="center" ,vertical="center")
                    
                    
                    ### Affichage des membres
                    
                    sh.merge_cells('B17:C17')
                    d=sh['B17']
                    d.value = "Président"
                    d.alignment = Alignment(vertical="center")
                    d.font  = Font(b=True, color="000000") # underline=True,
                    
                    deb=17
                    for mbre in liste_membre_jury:
                        if mbre.fonction == "Président":
                            deb=deb+1
                            sh.merge_cells('D17:G17')
                            d=sh['D17']
                            d.value = mbre.enseignant.nom+' '+mbre.enseignant.prenom
                            d.alignment = Alignment(vertical="center")
                            d.font  = Font(b=True, color="000000")
                            
                            sh.merge_cells('H17:J17')
                            d=sh['H17']
                            d.value = mbre.enseignant.grade
                            d.alignment = Alignment(vertical="center")
                            d.font  = Font(b=True, color="000000")
                    
                    
                    
                    sh.merge_cells('B18:C18')
                    d=sh['B18']
                    d.value = "Secrétaire"
                    d.alignment = Alignment(vertical="center")
                    d.font  = Font(b=True, color="000000") ########, underline=True
                    
                    for mbre in liste_membre_jury:
                        if mbre.fonction == "Secrétaire":
                            sh.merge_cells('D18:H18')
                            d=sh['D18']
                            d.value = mbre.enseignant.nom+' '+mbre.enseignant.prenom
                            d.alignment = Alignment(vertical="center")
                            d.font  = Font(b=True, color="000000")
                    
                    
                         
                    
                    sh.merge_cells('B19:C19')
                    d=sh['B19']
                    d.value = "Membres"
                    d.alignment = Alignment(vertical="center")
                    d.font  = Font(b=True, color="000000") ##, underline=True
                    
                    for mbre in liste_membre_jury:
                        
                        if mbre.fonction == "Membre" :
                            deb = deb+1
                            sh.merge_cells('D'+str(deb)+':H'+str(deb))
                            d=sh['D'+str(deb)]
                            d.value = mbre.enseignant.nom+' '+mbre.enseignant.prenom
                            d.alignment = Alignment(vertical="center")
                            d.font  = Font(b=True, color="000000")
                    
                    deb=deb+2
                    
                    sh.merge_cells('A'+str(deb)+':J'+str(deb))
                    d=sh['A'+str(deb)]
                    d.value = "Fait à Ouagadougou, le "+header.cleaned_data['date_delib']
                    d.alignment = Alignment(horizontal="center", vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                total_etd=len(liste_etudiant)
                total_admis=len(liste_admis)
                       
            #### Déclaration de la 4 ème feuille excel ######
                
                if header.cleaned_data['imp_pvd']:
                    
                    wsh=wb.create_sheet("Fichier détaillé de la délibération")
                    wsh.sheet_properties.tabColor = "72EE2E"
                    
                    #### En tête du fichier ####
                    
                    wsh.merge_cells('A1:F1')
                    d=wsh['A1']
                    d.value = header.cleaned_data['universite']
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    wsh.merge_cells('A2:F2')
                    d=wsh['A2']
                    d.value = "UNITE DE FORMATION ET DE RECHERCHE EN "
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    wsh.merge_cells('A3:F3')
                    d=wsh['A3']
                    d.value =uf.upper()+' (UFR/'+abr+')'
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    wsh.merge_cells('A4:F4')
                    d=wsh['A4']
                    d.value = "---------------"
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    wsh.merge_cells('A5:F5')
                    d=wsh['A5']
                    d.value = header.cleaned_data['service']
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    
                    
                    wsh['A7'].value =str(niv)+" Parcours "+str(dep)+"/"+opt.intitule+' ('+opt.abrege+')'
                    wsh['A7'].alignment = Alignment(vertical="center")
                    wsh['A7'].font  = Font(b=True, color="000000")
                    
                    pos=int(n_col/2)
                    ma_col=''
                    for key, val in dict_lettre.items(): ### récuperation position 
                        if val == pos+4:
                            ma_col=key
                            break 
                    
                    wsh[ma_col+'3'].value = "ANNEE UNIVERSITAIRE "+str(an_ac)
                    wsh[ma_col+'3'].alignment = Alignment(vertical="center")
                    wsh[ma_col+'3'].font  = Font(b=True, color="000000")
                    
                    for key, val in dict_lettre.items(): ### récuperation position 
                        if val == pos+1:
                            ma_col=key
                            break 
                    
                    wsh[ma_col+'7'].value = header.cleaned_data['titre']
                    wsh[ma_col+'7'].alignment = Alignment(vertical="center")
                    wsh[ma_col+'7'].font  = Font(b=True, color="000000")
                    
                    
                    pos=n_col-2
                    for key, val in dict_lettre.items(): ### récuperation position 
                        if val == pos+1:
                            ma_col=key
                            break 
                    
                    wsh[ma_col+'7'].value = session.session
                    wsh[ma_col+'7'].alignment = Alignment(vertical="center")
                    wsh[ma_col+'7'].font  = Font(b=True, color="000000")
                    
                    ##### EN tête du tableau ######
                    
                    wsh.merge_cells('A8:A11')
                    d=wsh['A8']
                    d.value= "N°"
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    wsh.merge_cells('B8:B11')
                    d=wsh['B8']
                    d.value= "Nom"
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    wsh.merge_cells('C8:C11')
                    d=wsh['C8']
                    d.value= "Prénom(s)"
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    wsh.merge_cells('D8:D11')
                    d=wsh['D8']
                    d.value= "Date de Naissance"
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    wsh.merge_cells('E8:E11')
                    d=wsh['E8']
                    d.value= "Lieu de Naissance"
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    wsh.merge_cells('F8:F11')
                    d=wsh['F8']
                    d.value= "Matricule"
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    wsh.merge_cells('A12:F12')
                    d=wsh['A12']
                    d.value = ''
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                ##### Partie des ue, ec #####"
                    
                    deb = 7
                    for ue in liste_ue :
                        nb=0
                        for ec in liste_matiere:
                            if ec.uniteEnseign == ue :
                                nb=nb+1
                        
                        ######## Les cases en fonctions du nombre de ec 
                        
                        
                        col_deb=''
                        col_fin=''
                        if nb == 1:
                            for key, val in dict_lettre.items():
                                if val == deb:
                                    col_deb=key
                                    break
                                
                            for key, val in dict_lettre.items():
                                if val == deb+3:
                                    col_fin=key
                                    break
                            
                            wsh.merge_cells(col_deb+'8:'+col_fin+'9')
                            d=wsh[col_deb+'8']
                            d.value = ue.intitule
                            d.alignment = Alignment(horizontal="center" , vertical="center")
                            d.font  = Font(b=True, color="000000")
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                            wsh.merge_cells(col_deb+'10:'+col_fin+'11')
                            d=wsh[col_deb+'10']
                            d.value = "UE"
                            d.alignment = Alignment(horizontal="center" , vertical="center")
                            d.font  = Font(b=True, color="000000")
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                            wsh[col_deb+'12'].value = "NOTE UE"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                            for key, val in dict_lettre.items():
                                if val == deb+1:
                                    col_deb=key
                                    break
                                
                            wsh[col_deb+'12'].value = "VALIDATION"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                            for key, val in dict_lettre.items():
                                if val == deb+2:
                                    col_deb=key
                                    break
                            wsh[col_deb+'12'].value = "CRED VAL"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            wsh[col_fin+'12'].value = "COTE"
                            wsh[col_fin+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_fin+'12'].font  = Font(b=True, color="000000")
                            wsh[col_fin+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                            deb=deb+4
                            #### fin lorsqu 'un seul ec #######
                            
                        elif nb == 2 :
                            for key, val in dict_lettre.items():
                                if val == deb:
                                    col_deb=key
                                    break
                                
                            for key, val in dict_lettre.items():
                                if val == deb+5:
                                    col_fin=key
                                    break
                            
                            wsh.merge_cells(col_deb+'8:'+col_fin+'9')
                            d=wsh[col_deb+'8']
                            d.value = ue.intitule
                            d.alignment = Alignment(horizontal="center" , vertical="center")
                            d.font  = Font(b=True, color="000000")
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            pos=deb-1
                            for ec in liste_matiere:
                                if ec.uniteEnseign == ue :
                                    pos=pos+1
                                    for key, val in dict_lettre.items():
                                        if val == pos:
                                            col_deb=key
                                            break
                                    
                                    wsh.merge_cells(col_deb+'10:'+col_deb+'11')
                                    d=wsh[col_deb+'10']
                                    d.value =ec.intitule
                                    d.alignment = Alignment(horizontal="center" , vertical="center")
                                    d.font  = Font(b=True, color="000000")
                                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                                    wsh[col_deb+'12'].value = "NOTE EC"
                                    wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                                    wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                                    wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            for key, val in dict_lettre.items():
                                if val == deb+2:
                                    col_deb=key
                                    break
                            
                            wsh.merge_cells(col_deb+'10:'+col_fin+'11')
                            d=wsh[col_deb+'10']
                            d.value = "UE"
                            d.alignment = Alignment(horizontal="center" , vertical="center")
                            d.font  = Font(b=True, color="000000")
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            wsh[col_deb+'12'].value = "MOY UE"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            for key, val in dict_lettre.items():
                                if val == deb+3:
                                    col_deb=key
                                    break
                                
                            wsh[col_deb+'12'].value = "VALIDATION"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            for key, val in dict_lettre.items():
                                if val == deb+4:
                                    col_deb=key
                                    break
                            wsh[col_deb+'12'].value = "CRED VAL"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            for key, val in dict_lettre.items():
                                if val == deb+5:
                                    col_deb=key
                                    break
                                
                            wsh[col_deb+'12'].value = "COTE"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            
                            deb=deb+6
                            
                            ### fin si le nombre de ec est égale à 2 #####
                        elif nb == 3 :
                            for key, val in dict_lettre.items():
                                if val == deb:
                                    col_deb=key
                                    break
                                
                            for key, val in dict_lettre.items():
                                if val == deb+6:
                                    col_fin=key
                                    break
                            
                            wsh.merge_cells(col_deb+'8:'+col_fin+'9')
                            d=wsh[col_deb+'8']
                            d.value = ue.intitule
                            d.alignment = Alignment(horizontal="center" , vertical="center")
                            d.font  = Font(b=True, color="000000")
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            pos=deb-1
                            for ec in liste_matiere:
                                if ec.uniteEnseign == ue :
                                    pos=pos+1
                                    for key, val in dict_lettre.items():
                                        if val == pos:
                                            col_deb=key
                                            break
                                    
                                    wsh.merge_cells(col_deb+'10:'+col_deb+'11')
                                    d=wsh[col_deb+'10']
                                    d.value = ec.intitule
                                    d.alignment = Alignment(horizontal="center" , vertical="center")
                                    d.font  = Font(b=True, color="000000")
                                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                                    wsh[col_deb+'12'].value = "NOTE EC"
                                    wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                                    wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                                    wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            for key, val in dict_lettre.items():
                                if val == deb+3:
                                    col_deb=key
                                    break
                            
                            wsh.merge_cells(col_deb+'10:'+col_fin+'11')
                            d=wsh[col_deb+'10']
                            d.value = "UE"
                            d.alignment = Alignment(horizontal="center" , vertical="center")
                            d.font  = Font(b=True, color="000000")
                            d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            wsh[col_deb+'12'].value = "MOY UE"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            for key, val in dict_lettre.items():
                                if val == deb+4:
                                    col_deb=key
                                    break
                                
                            wsh[col_deb+'12'].value = "VALIDATION"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            for key, val in dict_lettre.items():
                                if val == deb+5:
                                    col_deb=key
                                    break
                                
                            wsh[col_deb+'12'].value = "CRED VAL"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            for key, val in dict_lettre.items():
                                if val == deb+6:
                                    col_deb=key
                                    break
                                
                            wsh[col_deb+'12'].value = "COTE"
                            wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                            wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                            wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            
                            
                            deb=deb+7
                        
                    ##### Partie des totaux, moyennes et resultats ######
                    
                    for key, val in dict_lettre.items():
                        if val == deb:
                            col_deb=key
                            break
                    wsh.merge_cells(col_deb+'8:'+col_deb+'11')
                    d=wsh[col_deb+'8']
                    d.value = "TOTAL"
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    
                    wsh[col_deb+'12'].value = " "
                    wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                    wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                    wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    for key, val in dict_lettre.items():
                        if val == deb+1:
                            col_deb=key
                            break
                    wsh.merge_cells(col_deb+'8:'+col_deb+'11')
                    d=wsh[col_deb+'8']
                    d.value = "MOY"
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    
                    wsh[col_deb+'12'].value = " "
                    wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                    wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                    wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    for key, val in dict_lettre.items():
                        if val == deb+2:
                            col_deb=key
                            break
                    wsh.merge_cells(col_deb+'8:'+col_deb+'11')
                    d=wsh[col_deb+'8']
                    d.value = "RESULTAT"
                    d.alignment = Alignment(horizontal="center" , vertical="center")
                    d.font  = Font(b=True, color="000000")
                    d.border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                    
                    wsh[col_deb+'12'].value = " "
                    wsh[col_deb+'12'].alignment = Alignment(horizontal="center" , vertical="center")
                    wsh[col_deb+'12'].font  = Font(b=True, color="000000")
                    wsh[col_deb+'12'].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                    
                        
                        ######### Fin des entêtes des cases  ##########
                        
                    debut=12
                    i=0
                    nbre_releve=0
                    
                    for etd in liste_etudiant :
                        nbre_valide=0
                        
                        debut=debut+1
                        i=i+1
                        wsh['A'+str(debut)].value = i
                        wsh['A'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        wsh['A'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        wsh['B'+str(debut)].value = etd.nom
                        wsh['B'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        wsh['B'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        wsh['C'+str(debut)].value = etd.prenom
                        wsh['C'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        wsh['C'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        wsh['D'+str(debut)].value = etd.date_naissance
                        wsh['D'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        wsh['D'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        wsh['E'+str(debut)].value = etd.lieu_naissance
                        wsh['E'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        wsh['E'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        wsh['F'+str(debut)].value = etd.matricule
                        wsh['F'+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        wsh['F'+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        deb=7
                        total_pond=0
                        total_coef=0
                        moy=0
                        for ue in liste_ue :
                            nb=0
                            for ec in liste_matiere:
                                if ec.uniteEnseign == ue :
                                    nb = nb+1
                                    
                                
                            if nb == 1:
                                
                                total_coef = total_coef+ue.coef
                                for note in liste_note :
                                    if note.course.uniteEnseign == ue and note.student == etd:
                                        
                                        total_pond=total_pond+note.pondere
                                        
                                        for key, val in dict_lettre.items():
                                            if val == deb:
                                                col_deb=key
                                                break
                                            
                                        wsh[col_deb+''+str(debut)].value = note.note
                                        wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                        wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                        
                                        if note.note < 10 :
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+1:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = ''
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+2:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = "00"
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+3:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = "E"
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                        elif note.note >= 10 and note.note < 12 :
                                            
                                            nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                            for key, val in dict_lettre.items():
                                                if val == deb+1:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = "Validé"
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+2:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = note.course.coef
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+3:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = "D"
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                        elif note.note >= 12 and note.note < 14 :
                                            
                                            nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                            for key, val in dict_lettre.items():
                                                if val == deb+1:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = "Validé"
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+2:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = note.course.coef
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+3:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = "C"
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                        elif note.note >= 14 and note.note < 16 :
                                            
                                            nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                            for key, val in dict_lettre.items():
                                                if val == deb+1:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = "Validé"
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+2:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = note.course.coef
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+3:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = "B"
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                        elif note.note >= 16 and note.note <= 20 :
                                            
                                            nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                            for key, val in dict_lettre.items():
                                                if val == deb+1:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = "Validé"
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+2:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = note.course.coef
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                            for key, val in dict_lettre.items():
                                                if val == deb+3:
                                                    col_deb=key
                                                    break
                                                
                                            wsh[col_deb+''+str(debut)].value = "A"
                                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                            
                                        
                            
                                deb = deb+4
                                
                            elif nb == 2:
                                pos=deb-1
                                pond=0
                                moy_ue=0
                                total_coef=total_coef+ue.coef
                                
                                for ec in liste_matiere:
                                    if ec.uniteEnseign == ue :
                                        for note in liste_note :
                                            if note.student == etd and note.course == ec :
                                                
                                                total_pond = total_pond + note.pondere
                                                
                                                pos=pos+1
                                                pond = pond + note.pondere
                            
                                                for key, val in dict_lettre.items():
                                                    if val == pos:
                                                        col_deb=key
                                                        break
                                                    
                                                wsh[col_deb+''+str(debut)].value = note.note
                                                wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                                wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                                ### Fin affichage des notes dans les ec ####
                                                
                                pos=deb+2
                                moy_ue=pond/ue.coef
                                
                                for key, val in dict_lettre.items():
                                    if val == pos:
                                        col_deb=key
                                        break
                                    
                                wsh[col_deb+''+str(debut)].value =float(format(moy_ue, '0.2f'))
                                wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                
                                if moy_ue < 10 :
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+1:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = ''
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+2:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "00"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+3:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "E"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                elif moy_ue >= 10 and moy_ue < 12 :
                                    
                                    nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                    for key, val in dict_lettre.items():
                                        if val == pos+1:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "Validé"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+2:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = note.course.coef
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+3:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "D"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                elif moy_ue >= 12 and moy_ue < 14 :
                                    
                                    nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                    for key, val in dict_lettre.items():
                                        if val == pos+1:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "Validé"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+2:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = note.course.coef
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+3:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "C"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                elif moy_ue >= 14 and moy_ue < 16 :
                                    
                                    nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                    for key, val in dict_lettre.items():
                                        if val == pos+1:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "Validé"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+2:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = note.course.coef
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+3:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "B"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                elif moy_ue >= 16 and moy_ue <= 20 :
                                    
                                    nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                    for key, val in dict_lettre.items():
                                        if val == pos+1:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "Validé"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+2:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = note.course.coef
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+3:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "A"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                deb=deb+6
                            
                            elif nb == 3:
                                pos=deb-1
                                pond=0
                                moy_ue=0
                                total_coef=total_coef+ue.coef
                                
                                for ec in liste_matiere:
                                    if ec.uniteEnseign == ue :
                                        for note in liste_note :
                                            if note.student == etd and note.course == ec :
                                                
                                                total_pond = total_pond + note.pondere
                                                
                                                pos=pos+1
                                                pond = pond + note.pondere
                            
                                                for key, val in dict_lettre.items():
                                                    if val == pos:
                                                        col_deb=key
                                                        break
                                                    
                                                wsh[col_deb+''+str(debut)].value = note.note
                                                wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                                wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                                ### Fin affichage des notes dans les ec ####
                                                
                                pos=deb+3
                                moy_ue=pond/ue.coef
                                
                                
                                for key, val in dict_lettre.items():
                                    if val == pos:
                                        col_deb=key
                                        break
                                    
                                wsh[col_deb+''+str(debut)].value = float(format(moy_ue, '0.2f'))
                                wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                
                                if moy_ue < 10 :
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+1:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = ''
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+2:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "00"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+3:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "E"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                elif moy_ue >= 10 and moy_ue < 12 :
                                    
                                    nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                    for key, val in dict_lettre.items():
                                        if val == pos+1:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "Validé"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+2:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = note.course.coef
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+3:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "D"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                elif moy_ue >= 12 and moy_ue < 14 :
                                    
                                    nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                    for key, val in dict_lettre.items():
                                        if val == pos+1:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "Validé"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+2:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = note.course.coef
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+3:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "C"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                elif moy_ue >= 14 and moy_ue < 16 :
                                    
                                    nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                    for key, val in dict_lettre.items():
                                        if val == pos+1:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "Validé"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+2:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = note.course.coef
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+3:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "B"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                elif moy_ue >= 16 and moy_ue <= 20 :
                                    
                                    nbre_valide = nbre_valide+1 #### Ppour connaitre le nombre de crédits validés
                                    for key, val in dict_lettre.items():
                                        if val == pos+1:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "Validé"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+2:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = note.course.coef
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                                    
                                    for key, val in dict_lettre.items():
                                        if val == pos+3:
                                            col_deb=key
                                            break
                                        
                                    wsh[col_deb+''+str(debut)].value = "A"
                                    wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                                    wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                            
                                deb=deb+7
                                    
                    
                            ##### Fin de l'affichage des notes et des moyennes par ue
                        
                        deb=n_col-2
                        moy = total_pond/total_coef
                        
                        for key, val in dict_lettre.items():
                            if val == deb:
                                col_deb=key
                                break
                            
                        wsh[col_deb+''+str(debut)].value = total_pond
                        wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        for key, val in dict_lettre.items():
                            if val == deb+1:
                                col_deb=key
                                break
                            
                        wsh[col_deb+''+str(debut)].value =float(format(moy, '0.2f'))
                        wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                        wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        if moy >= 10 :
                            
                            for key, val in dict_lettre.items():
                                if val == deb+2:
                                    col_deb=key
                                    break
                                
                            wsh[col_deb+''+str(debut)].value = "Admis(e)"
                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        elif moy < 10 :
                            
                            for key, val in dict_lettre.items():
                                if val == deb+2:
                                    col_deb=key
                                    break
                                
                            wsh[col_deb+''+str(debut)].value = " "
                            wsh[col_deb+''+str(debut)].border = Border(top=thin, left=thin, right=thin, bottom=thin)
                            wsh[col_deb+''+str(debut)].alignment = Alignment(horizontal="center", vertical="center")
                        
                        #### Sauvegarde dans la table Note_ue pour les relevés ###
                        
                        if type_delib == 'Définitive':
                            if nbre_valide > 0:
                                
                                nbre_releve=nbre_releve+1
                                
                                
                                for ue in liste_ue :
                                    moy_note_ue = 0
                                    pond_note_ue =0
                                
                                    nb=0
                                    cote=''
                                    for ec in liste_matiere:
                                        if ec.uniteEnseign == ue :
                                            nb=nb+1
                                    
                                    if nb == 1:
                                        for note in liste_note :
                                            if note.course.uniteEnseign == ue and note.student == etd:
                                                if note.note < 10 :
                                                    cote="E"
                                                elif note.note >= 10 and note.note <12:
                                                    cote='D'
                                                elif note.note >= 12 and note.note <14:
                                                    cote='C'
                                                elif note.note >= 14 and note.note <16:
                                                    cote='B'
                                                elif note.note >= 16 and note.note <= 20:
                                                    cote='A'
                                                
                                                
                                                note_ue=Note_UE(ue=ue, student=etd, moy=note.note, cote=cote, anneeaca=an_ac,session=session)
                                                note_ue.save()
                                    elif nb > 1 :
                                        
                                        for ec in liste_matiere:
                                            if ec.uniteEnseign == ue :
                                                for note in liste_note :
                                                    if note.student == etd and note.course == ec :
                                                        pond_note_ue = pond_note_ue + note.pondere
                                                        
                                        moy_note_ue = pond_note_ue/ue.coef
                                        
                                        if moy_note_ue < 10 :
                                            cote="E"
                                        elif moy_note_ue >= 10 and moy_note_ue <12:
                                            cote='D'
                                        elif moy_note_ue >= 12 and moy_note_ue <14:
                                            cote='C'
                                        elif moy_note_ue >= 14 and moy_note_ue <16:
                                            cote='B'
                                        elif moy_note_ue >= 16 and moy_note_ue <= 20:
                                            cote='A'
                                        
                                        note_ue=Note_UE(ue=ue, student=etd, moy=float(format(moy_note_ue, '0.2f')), cote=cote, anneeaca=an_ac,session=session)
                                        note_ue.save()
                    
                    #### Fin de la sauvegarde des notes par ue pour les etudiants dont on doit imprimer les rélévés ####
                    
                    ### Sauvegarde de l'existence de relevé ####
                    
                    if type_delib == "Définitive":
                    
                        releve = Releve(deliberation=deliberation, nbre_releve=nbre_releve, is_actif = True)
                        
                        releve.save()
                    
                    #### Envoi de mail au chef de la scolarité pour information d'une nouvelle délibération ####
                    
                    """
                    En attente de la confirmation de MON SUPERVISEUR OU MAITRE DE STAGE
                    """
                        
                    ######## Fin du tableau ##########
                    
                    ### Affichage du jury ########
                    
                    ######### La signature ######
                    
                    nbre_etd=len(liste_etudiant)
                    
                    ctre = int(n_col/2)
                    
                    for key, val in dict_lettre.items():
                        if val == ctre:
                            col_deb=key
                            break
                        
                    deca =nbre_etd+2+12
                    wsh[col_deb+''+str(deca)].value = "Ouagadougou, le "+header.cleaned_data['date_delib']
                    wsh[col_deb+''+str(deca)].font  = Font(b=True, color="000000")
                    
                    #### Récupération de la liste des membres du jury #####
                    
                    liste_membre_jury=Formation_Jury.objects.filter(jury__id=idJury)
                    
                    deca = deca+1
                    
                    wsh['C'+str(deca)].value = "Les Membres du jury"
                    cpter = deca
                    for mbre in liste_membre_jury :
                        if mbre.fonction != "Président" :
                            cpter=cpter+1
                            wsh['C'+str(cpter)].value = mbre.enseignant.nom+' '+mbre.enseignant.prenom
                    
                    
                    
                    for key, val in dict_lettre.items():
                        if val == n_col-2: 
                            col_deb=key
                            break
                    
                    wsh[col_deb+''+str(deca)].value = "Président du jury"
                    cpter=deca
                    for mbre in liste_membre_jury :
                        if mbre.fonction == "Président" :
                            cpter=cpter+1
                            wsh[col_deb+''+str(cpter)].value = mbre.enseignant.nom+' '+mbre.enseignant.prenom
                    
                
                
                wb.save('../../PV_de_délibération.xlsx')
                
                
                if type_delib == 'Définitive' :
                
                    if Niv.abrege == "L1S1":
                        niv_proch=Niveau.objects.get(abrege="L1S2")
                    elif Niv.abrege == "L1S2":
                        niv_proch=Niveau.objects.get(abrege="L2S3")
                    elif Niv.abrege == "L2S3":
                        niv_proch=Niveau.objects.get(abrege="L2S4")
                    elif Niv.abrege == "L2S4":
                        niv_proch=Niveau.objects.get(abrege="L3S5")
                    elif Niv.abrege == "L3S5":
                        niv_proch=Niveau.objects.get(abrege="L3S6")
                    elif Niv.abrege == "L3S6":
                        niv_proch=Niveau.objects.get(abrege="M1S1")
                    elif Niv.abrege == "M1S1":
                        niv_proch=Niveau.objects.get(abrege="M1S2")
                    elif Niv.abrege == "M1S2":
                        niv_proch=Niveau.objects.get(abrege="M2S3")
                    elif Niv.abrege == "M2S3":
                        niv_proch=Niveau.objects.get(abrege="M2S4")
                    
                    
                    for adm in liste_admis : ### verification si l'étudiant est admis 
                        cursus=Cursus.objects.get(anneeAca=an_ac, niveau=Niv, departement= Depart, etudiant=adm)
                        cursus.statut="Validé"
                        
                        CoefT=0
                        pondT=0
                        MoyT=0
                        
                        for note in liste_note:
                            if note.student == adm:
                                pondT= pondT + note.pondere
                                CoefT=CoefT + note.course.coef
                                note.is_actif=False
                                note.save()
                                
                                
                        MoyT = pondT/CoefT
                        
                        cursus.moyenne=float(format(MoyT, '0.2f'))
                        cursus.save()
                        an_proch=AnneeAcad.objects.get(fin = an_ac.fin+1)
                        
                        ### Modification des données de l'étudiant
                        
                        if adm.niveau.abrege == "M2S4": ### Fin de cycle de master
                            adm.is_actif=False
                            adm.is_ancien=True
                            adm.save()
                        elif adm.niveau.abrege == "L1S2" or adm.niveau.abrege == "L2S4" or adm.niveau.abrege == "L3S6" or adm.niveau.abrege == "M1S2" :
                            adm.anneeAca = an_proch
                            adm.niveau = niv_proch
                            adm.save()
                            
                            #### Enregistrement d'un nouveau cursus                   
                            curs=Cursus(anneeAca=adm.anneeAca,statut='En cours', niveau=adm.niveau, etudiant=adm, departement=adm.departement)
                            curs.save()
                            
                        else:
                            adm.niveau = niv_proch
                            adm.save()
                                
                            #### Enregistrement d'un nouveau cursus                   
                            curs=Cursus(anneeAca=adm.anneeAca,statut='En cours', niveau=adm.niveau, etudiant=adm, departement=adm.departement)
                            curs.save()
                
                    
                    if session.session == '1ère Session':
                         
                        """
                         les traitements si nous sommes à la prémière session
                        """
                        
                        
                        for ajr in liste_ajourne :
                            
                            for note in liste_note:
                                if note.student == ajr:
                                    if note.note < 10:
                                        note.delete() ### Supprimer la note de l'étudiant dans la matière non validée 
                        
                    
                    elif session.session == '2ème Session':
                        
                        """
                        les traitements relatives a la 2ème session
                        """
                        for ajr in liste_ajourne :
                            
                            
                            cursus=Cursus.objects.get(anneeAca=an_ac, niveau=Niv, departement= Depart, etudiant=ajr)
                            cursus.statut="Non Validé"
                            CoefT=0
                            pondT=0
                            MoyT=0
                            
                            for note in liste_note:
                                if note.student == ajr:
                                    pondT=note.pondere
                                    coefT=CoefT + note.course.coef
                                    
                                    
                            MoyT = pondT/coefT
                            
                            cursus.moyenne=float(format(MoyT, '0.2f'))
                            cursus.save()
                            
                            ajr.anneeAca= an_proch
                            ajr.save()
                            
                            curs=Cursus(anneeAca=ajr.anneeAca,statut='En cours', niveau=ajr.niveau, etudiant=ajr, departement=ajr.departement)
                            curs.save()
                
                            #### Faire un parcours des ue et garder les notes dans les ues validés
                            for ue in liste_ue:
                                moy_ue=0
                                pond=0
                                for ec in liste_matiere:
                                    if ec.uniteEnseign == ue:
                                        for note in liste_note:
                                            if note.course == ec  and note.student == ajr:
                                                pond = pond+note.pondere
                                                
                                moy_ue = pond/ue.coef
                                
                                if moy_ue < 10: ### Si la moyenne est inferieur à 10 dans l'ue, et que nous sommes en 2ème session
                                    for note in liste_note :
                                        if note.course.uniteEnseign == ue and note.student == ajr :
                                            note.is_actif=False
                                            note.save()
                                
                                else :
                                    for note in liste_note :
                                        if note.course.uniteEnseign == ue and note.student == ajr :
                                            note.anneeAca = ajr.anneeAca
                                            note.save()
                    
                    for item in liste_note_save:
                        item.is_actif= False
                        item.save()
                                              
                        
                    ### Fin des traitements si nous sommes à la délibération définitive ###
                
                
                liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
                nbre_notif=len(liste_notif)
                
                liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
                
                context={'dict_admis':dict_admis, 'liste_etudiant':liste_etudiant, 'liste_membre_jury':liste_membre_jury, 'niveau':niv, 'departement':dep,
                         'session':session.session, 'total_admis':total_admis, 'total_etd':total_etd, 'type_delib':type_delib,
                         'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                         'jury_id':idJury}
                
                return render(request, 'cfm_deliberation.html', context)
            
        
        #dict_note={}     
        
                    
                

        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                  'header':header, 'button':button, 'liste_note':liste_note,'liste_etudiant':liste_etudiant, 
                  'liste_matiere':liste_matiere, 'niveau':niv, 'departement':dep,'jury_id':idJury,
                  'note_save':nbre_note_save, 'mat_total':nbre_matiere_total,'dict_mat_save':dict_mat_save}
        
        return render(request, 'deliberation.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def Modifier_Note_Etd(request, idE, idJ):
    """
    Cette vue permet de modifier les notes d'un seul étudiant dans une ou plusieurs matières
    """
    logged_user=form_user(request)
    if logged_user:
        message=""
        suc=""
        etd=Etudiant.objects.get(id=idE)
        if request.POST:
            
            idM=request.POST['id_matiere']
            matiere=Matiere.objects.get(id=idM)
            val_note=request.POST['nvl_note']
            
            if val_note!='':
                # SI ce n'est pas une chaine ou si la note n'est pas >20 ou <0
                etd_note=Note.objects.get(student=etd, course=matiere, anneeAca=etd.anneeAca)
                
                if float(val_note) != etd_note.note:
                    
                    etd_note.note=val_note
                    etd_note.pondere=float(val_note) * float (matiere.coef) 
                    etd_note.save()
                    
                    message="SUCCES : Note en "+matiere.intitule+" modifiée avec succès !"
                    suc="succes"
                
                else:
                    message=" ATTENTION : Vous avez entré la même note pour la modification en "+matiere.intitule+" !"
                    suc="attention"
                
        
        liste_matiere=Matiere.objects.filter(is_actif=True, uniteEnseign__niveau=etd.niveau, uniteEnseign__departement=etd.departement)            
        liste_note=Note.objects.filter(is_actif=True, student=etd, anneeAca=etd.anneeAca)
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'liste_matiere':liste_matiere, 'liste_note':liste_note, 'message':message, 'etd':etd, 'idj':idJ, 'succes':suc}
        
        return render(request, 'modifier_note_etud.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))            
            
def releve_pdf(request, idRel):
    """
    Cette vue permet d'imprimer les Rélévés de notes des étudiants
    """
    logged_user=form_user(request)
    if logged_user:
        
        releve = Releve.objects.get(id=idRel)
        
        
        niv=releve.deliberation.niveau
        niveau=niv.abrege
        dep=releve.deliberation.departement
        
        liste_note_ue=Note_UE.objects.filter(ue__niveau= niv, ue__departement = dep, session = releve.deliberation.session, anneeaca = releve.deliberation.anneeaca)
        
        sessi = releve.deliberation.session.session
        
        if sessi == '1ère Session':
            sess = "Session normale"
        elif sessi == "2ème Session":
            sess= "Session de rattrapage"
            
        
        depart=dep.intitule.upper()
        sou_titre1=''
        sou_titre2=''
        
        print(niveau)
        if niveau == "L1S1" or niveau == "L1S2":
            
            sou_titre1="1ère Année SCIENCES ET TECHNOLOGIES (ST)"
            if niveau == "L1S1":
                sou_titre2="SEMESTRE I (S1)"
            elif niveau == "L1S2":
                sou_titre2="SEMESTRE II (S2)"
                
        elif niveau == "L2S3" or niveau == "L2S4":
            sou_titre1="LICENCE II (L2) : "+depart
            
            if niveau == "L2S3":
                sou_titre2="SEMESTRE III (S3)"
                
            elif niveau == "L2S4":
                sou_titre2="SEMESTRE IV (S4)"
                
        elif niveau == "L3S5" or niveau == "L3S6" :
            sou_titre1="LICENCE III (L3) : "+depart
            if niveau == "L3S5":
                sou_titre2="SEMESTRE V (S5)"
               
            elif niveau == "L3S6" :
                sou_titre2="SEMESTRE VI (S6)"
               
        elif niveau == "M1S1" or niveau == "M1S2":
            sou_titre1="MASTER I (M1) : "+depart
            if niveau == "M1S1":
                sou_titre2="SEMESTRE I (S1)"
                
            elif niveau == "M1S2":
                sou_titre2="SEMESTRE II (S2)"
                
        elif niveau == "M2S3" or niveau == "M2S4":
            sou_titre1="MASTER II (M2) : "+depart
            if niveau == "M2S3":
                sou_titre2="SEMESTRE III (S3)"
                
            elif niveau == "M2S4":
                sou_titre2="SEMESTRE IV (S4)"
        
        dict_etd={}
        i=0
        for item in liste_note_ue:
            if item.student not in dict_etd:
                i=i+1
                dict_etd[item.student]=i
                
        
        context={'sous_titre1': sou_titre1, 'sous_titre2':sou_titre2, 'liste': liste_note_ue, 'session':sess,
                 'dict_etd':dict_etd, 'releve':releve}
        
        #return render(request,"releve_de_note_pdf.html", context)
        
        return PDFTemplateResponse(request=request, template='releve_de_note_pdf.html', context = context,
                                   filename='releve_de_note_'+str(niv)+'_'+str(dep)+'.pdf', show_content_in_browser = True)
        
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def supp_releve(request, idRel):
    """
    """
    
    logged_user=form_user(request)
    if logged_user:
        releve = Releve.objects.get(id=idRel)
        releve.is_actif =  False 
        
        releve.save()
        
        return redirect('/deliberation/impression-releve')
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def imprimer_Releve(request):
    """
    Cette vue présente les différents relevés de notes disponible qui peuvent être imprimés
    """
    
    logged_user=form_user(request)
    message=''
    if logged_user:
        form =  ImpressionForm(data=request.POST or None)
        if request.POST:
            if form.is_valid():
                date_imp=form.cleaned_data['date_imp']
                signataire=form.cleaned_data['signataire']
                idRel = request.POST['id_releve']
                
                message = 'Relevé Imprimer au format pdf avec succès !'
                
                releve = Releve.objects.get(id=idRel)
        
        
                niv=releve.deliberation.niveau
                niveau=niv.abrege
                dep=releve.deliberation.departement
                sessi = releve.deliberation.session.session
                
                liste_note_ue=Note_UE.objects.filter(ue__niveau= niv, ue__departement = dep, session = releve.deliberation.session, anneeaca = releve.deliberation.anneeaca)
                
                
                if sessi == '1ère Session':
                    sess = "Session normale"
                elif sessi == "2ème Session":
                    sess= "Session de rattrapage"
                    
                
                depart=dep.intitule.upper()
                sou_titre1=''
                sou_titre2=''
                
                print(niveau)
                if niveau == "L1S1" or niveau == "L1S2":
                    
                    sou_titre1="1ère Année SCIENCES ET TECHNOLOGIES (ST)"
                    if niveau == "L1S1":
                        sou_titre2="SEMESTRE I (S1)"
                    elif niveau == "L1S2":
                        sou_titre2="SEMESTRE II (S2)"
                        
                elif niveau == "L2S3" or niveau == "L2S4":
                    sou_titre1="LICENCE II (L2) : "+depart
                    
                    if niveau == "L2S3":
                        sou_titre2="SEMESTRE III (S3)"
                        
                    elif niveau == "L2S4":
                        sou_titre2="SEMESTRE IV (S4)"
                        
                elif niveau == "L3S5" or niveau == "L3S6" :
                    sou_titre1="LICENCE III (L3) : "+depart
                    if niveau == "L3S5":
                        sou_titre2="SEMESTRE V (S5)"
                       
                    elif niveau == "L3S6" :
                        sou_titre2="SEMESTRE VI (S6)"
                       
                elif niveau == "M1S1" or niveau == "M1S2":
                    sou_titre1="MASTER I (M1) : "+depart
                    if niveau == "M1S1":
                        sou_titre2="SEMESTRE I (S1)"
                        
                    elif niveau == "M1S2":
                        sou_titre2="SEMESTRE II (S2)"
                        
                elif niveau == "M2S3" or niveau == "M2S4":
                    sou_titre1="MASTER II (M2) : "+depart
                    if niveau == "M2S3":
                        sou_titre2="SEMESTRE III (S3)"
                        
                    elif niveau == "M2S4":
                        sou_titre2="SEMESTRE IV (S4)"
                
                dict_etd={}
                i=0
                
                for item in liste_note_ue:
                    if item.student not in dict_etd:
                        i=i+1
                        dict_etd[item.student]=i
                    
                    
                    
                ### Calcul de la moyenne de tout les étudiants dont on fera les rélévés
                
                dict_moy={}
                for key, val in dict_etd.items():
                    credits_T=0
                    moy_g=0
                    coef_T=0
                    moy=0
                    for note_ue in liste_note_ue:
                        if note_ue.student == key :
                            moy_g=moy_g+note_ue.moy*note_ue.ue.coef 
                            coef_T=coef_T+note_ue.ue.coef
                        
                            credits_T=credits_T+note_ue.ue.coef    
                            
                    moy=moy_g/coef_T
                    
                    dict_moy[key]=float(format(moy, '0.2f'))
                
                
                context={'sous_titre1': sou_titre1, 'sous_titre2':sou_titre2, 'liste': liste_note_ue, 'session':sess,
                         'dict_etd':dict_etd, 'dict_moy':dict_moy, 'credits':credits_T, 'date_impression' : date_imp,
                         'signataire': signataire,'releve':releve}
                
                
                #return render_to_pdf_response(request=request, template='test_releve1.html', context=context, download_filename='releve_de_note.pdf', content_type='application/pdf', response_class=HttpResponse)
                
                #return render(request,'releve_de_note_pdf.html', context=context)
            
                return PDFTemplateResponse(request=request, template='releve_de_note_pdf.html', context = context,
                                        filename='releve_de_note_'+str(niv)+'_'+str(dep)+'.pdf', 
                                        footer_template='footer_template.html', show_content_in_browser = False)
                
        
        
        liste_releve_dispo=Releve.objects.filter(is_actif=True)
        dispo=len(liste_releve_dispo)
        
        liste_releve_supp=Releve.objects.filter(is_actif=False)
        supp=len(liste_releve_supp)
        
        if logged_user.type_user == 'Enseignant':
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                'supp':supp, 'dispo':dispo, 'liste_releve':liste_releve_dispo, 'form':form, 'message':message}

            
        elif logged_user.type_user == 'Scolarite':
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
        
            context={'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                'supp':supp, 'dispo':dispo, 'liste_releve':liste_releve_dispo, 'form':form, 'message':message}

        
        return render(request, 'imprimer_releve.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def imprimer_membre_Jury(request, idJury):
    """
    Cette vue permtet d'imprimer laliste des membres d'un jury
    """
    
    logged_user=form_user(request)
    if logged_user:
        
        jury=Jury.objects.get(id=idJury)
        liste_membre_jury=Formation_Jury.objects.filter(jury=jury)
        
        
        context={'liste_membre_jury':liste_membre_jury, 'jury':jury}
        return PDFTemplateResponse(request=request, template='liste_membre_jury_pdf.html', context = context,footer_template='footer_template.html',
                                        filename='Membre_de_jury_'+str(jury.niveau)+'_'+str(jury.departement)+'.pdf', show_content_in_browser = False)
                
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
 
def imprimer_Note(request, idM):
    """
    Cette vue permtet d'imprimer les notes des étudiants dans une matière
    """
    
    logged_user=form_user(request)
    if logged_user:
        
        mat=Matiere.objects.get(id=idM)
        liste_etudiant=Etudiant.objects.filter(departement=mat.uniteEnseign.departement,niveau=mat.uniteEnseign.niveau, is_ancien=False, is_actif=True).order_by('nom', 'prenom')
        etudiant=Etudiant.objects.filter(departement=mat.uniteEnseign.departement,niveau=mat.uniteEnseign.niveau, is_ancien=False, is_actif=True).order_by('nom', 'prenom')[0]
        liste_note=Note.objects.filter(course=mat, anneeAca=etudiant.anneeAca,course__is_actif=True, is_actif=True)
        
        
        context=dict(liste_etudiant=liste_etudiant, matiere=mat, liste_note=liste_note)
        return PDFTemplateResponse(request=request, template='consulter_note_pdf.html', context = context,
                                   filename='note_etudiant.pdf', footer_template='footer_template.html',
                                   cmd_options={'orientation':'landscape'}, show_content_in_browser = False)
   
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
 




































