from django.apps import AppConfig


class DeliberationConfig(AppConfig):
    name = 'deliberation'
