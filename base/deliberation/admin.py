from django.contrib import admin
from deliberation.models import Note, Cursus, Jury, Formation_Jury, Note_Save,\
    Releve, Deliberation, Option, Session
# Register your models here.

admin.site.register(Note)
admin.site.register(Cursus)
admin.site.register(Jury)
admin.site.register(Formation_Jury)
admin.site.register(Note_Save)
admin.site.register(Releve)
admin.site.register(Deliberation)
admin.site.register(Option)
admin.site.register(Session)