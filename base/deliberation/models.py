from django.db import models
from base.models import Etudiant, Niveau,AnneeAcad, Departement, Enseignant,\
    Matiere, UE
#from base.models import AnneeUniv#, Ufr
#from gestion_enseignement.models import NouvelleEnseignants
# Create your models here.

class Session(models.Model):
    """
    """
    psession, dsession= '1ère Session', '2ème Session'
    
    sess=((psession, '1ère Session'),(dsession, '2ème Session'))
    
    session=models.CharField(choices=sess, default='1ère Session', null=False, max_length=15)
    
    def __str__(self):
        """
        """
        return self.session


class Note(models.Model):
    anneeAca=models.ForeignKey(AnneeAcad, verbose_name="Année Académique", on_delete=models.DO_NOTHING)
    note= models.FloatField()
    student=models.ForeignKey(Etudiant,on_delete = models.DO_NOTHING)
    course=models.ForeignKey(Matiere,on_delete = models.DO_NOTHING)
    pondere=models.FloatField(default=0)
    is_actif=models.BooleanField(null=False, default=True)
    def __str__(self):
        return self.student.nom+' '+self.student.prenom+' =>'+' '+self.course.intitule+' : '+str(self.note)
    
  
    
class Cursus(models.Model):
    """
    """
    anneeAca=models.ForeignKey(AnneeAcad, verbose_name="Année Académique", on_delete=models.DO_NOTHING)
    valide,non_valide,en_cour='Validé','Non Validé','En cours'
    STATUT=((valide, 'Validé'),(non_valide, ' Non Validé'), (en_cour, 'En cours'))
    statut= models.CharField(choices=STATUT, max_length=20, default='En cours', null=False)
    niveau=models.ForeignKey(Niveau,on_delete=models.DO_NOTHING)
    moyenne=models.FloatField(null=True, blank=True)
    etudiant=models.ForeignKey(Etudiant, on_delete=models.DO_NOTHING, null=False)
    departement=models.ForeignKey(Departement, on_delete=models.DO_NOTHING, null=False)
    
    def __str__(self):
        return "Année académique : "+str(self.anneeAca)+" Niveau: "+str(self.niveau)+" => Etudiant : "+str(self.etudiant)+" => Statut : "+self.statut+" => Moyenne : "+str(self.moyenne)

class Jury(models.Model):
    """
    """
    anneeUniv=models.ForeignKey(AnneeAcad, verbose_name="Année", on_delete=models.DO_NOTHING, null=True)
    intitule=models.CharField("Nom du Jury", max_length=50)
    niveau=models.ForeignKey(Niveau, on_delete=models.DO_NOTHING)
    departement=models.ForeignKey(Departement, on_delete=models.DO_NOTHING)
    nbre_membre=models.IntegerField("Nombre de membres", default=5)
    is_actif=models.BooleanField(default=True)
    
    def __str__(self):
        return "Jury "+self.intitule+' Niveau :'+str(self.niveau)+' Depart: '+str(self.departement)

class Formation_Jury(models.Model):
    """
    """
    jury=models.ForeignKey(Jury, on_delete=models.DO_NOTHING, unique=False)
    enseignant=models.ForeignKey(Enseignant, on_delete=models.DO_NOTHING)
    secretaire, president, membre='Secrétaire', 'Président', 'Membre'
    FONCTION=((secretaire, 'Secrétaire'),(president, 'Président'),(membre, 'Membre'))
    fonction= models.CharField(choices=FONCTION, max_length=50, null=False,blank=False)
    
    def __str__(self):
        return "Enseignant :"+self.enseignant.prenom+' '+self.enseignant.nom+' => Fonction : '+self.fonction+' Dans jury : '+str(self.jury)

    
class Option(models.Model):
    """
    """
    abrege=models.CharField(max_length=50, null=True, unique=True)
    intitule=models.CharField(max_length=200, null=False, unique=True)
    
    def __str__(self):
        return self.intitule
    
class Header(models.Model):
    """
    """
    
    universite=models.CharField(max_length=255, null=False)
    titre=models.CharField(max_length=200)
    service=models.CharField(max_length=200)
    session=models.ForeignKey(Session, on_delete=models.DO_NOTHING)
    option=models.ForeignKey(Option, on_delete=models.DO_NOTHING, null=False)
    date_delib=models.CharField("Date de Délibération", null=False, max_length=50)
    tel=models.CharField("Téléphone", null=True, max_length=100, default="(+226) 25 30 70 64 / 65", blank=True)
    adresse=models.CharField("Adresse", null=True, max_length=100, default="03 BP 7021 OUAGADOUGOU 03", blank=True)
    fax=models.CharField("Fax", null=True, max_length=100, default="(+226) 25 30 72 42", blank=True)
    telex=models.CharField("Télex", null=True, max_length=100, default="5270 BF", blank=True)
    imp_pv=models.BooleanField("PV de délibération" ,null=False, default=True)
    imp_adm=models.BooleanField("Liste des admis" ,null=False, default=True)
    imp_jur=models.BooleanField("Liste du jury" ,null=False, default=True)
    imp_pvd=models.BooleanField("PV détaillé",null=False, default=True)
    
    
    provi, defini= '1ère Délibération', 'Définitive'
    
    type=((provi, '1ère Délibération'),(defini, 'Définitive'))
    
    type_delib=models.CharField("" ,choices=type, default='1ère Délibération', null=False, max_length=17)
    
    
    def __str__(self):
        """
        """
        return self.universite+" "+self.titre+" "+self.session+" "+self.niveau+" "+self.departement+" "+self.option
    
    
class Deliberation(models.Model):
    """
    """
    niveau=models.ForeignKey(Niveau, on_delete=models.DO_NOTHING, null=False)
    departement=models.ForeignKey(Departement, on_delete=models.DO_NOTHING, null=False)
    anneeaca=models.ForeignKey(AnneeAcad, on_delete=models.DO_NOTHING, null=False)
    session=models.ForeignKey(Session, on_delete=models.DO_NOTHING, null=False)
    admis=models.IntegerField()
    ajourne=models.IntegerField()
    
    def __str__(self):
        """
        """
        return str(self.niveau)+' '+str(self.departement)+' '+str(self.session)
    
class Releve(models.Model):
    """
    """
    deliberation=models.ForeignKey(Deliberation, on_delete=models.DO_NOTHING, null=False)
    nbre_releve = models.IntegerField(default=0, null=False)
    is_actif=models.BooleanField( null=False, default=True)
    
    def __str__(self):
        return str(self.nbre_releve)
    
    
class Impression(models.Model):
    signataire=models.ForeignKey(Enseignant, on_delete=models.DO_NOTHING, null=False)
    date_imp=models.CharField("Date d'Impression", null=False, max_length=50)
    releve=models.ForeignKey(Releve, on_delete=models.DO_NOTHING, null=False)
    
    def __str__(self):
        return str(self.date_imp)+" relevé n° "+str(self.releve.id)
    
class Note_Save(models.Model):
    """
    """
    is_actif=models.BooleanField(default=True, null=False)
    anneeaca=models.ForeignKey(AnneeAcad, on_delete=models.DO_NOTHING, null=False)
    matiere=models.ForeignKey(Matiere, on_delete=models.DO_NOTHING, null=False)
    
    def __str__(self):
        """
        """
        return "Année académique "+str(self.anneeaca)+" Matière : "+str(self.matiere)

class Note_UE(models.Model):
    moy= models.FloatField()
    student=models.ForeignKey(Etudiant,on_delete = models.DO_NOTHING)
    ue=models.ForeignKey(UE,on_delete = models.DO_NOTHING)
    cote=models.CharField(max_length=5, null=False)
    anneeaca=models.ForeignKey(AnneeAcad, on_delete=models.DO_NOTHING, null=False)
    session=models.ForeignKey(Session, on_delete=models.DO_NOTHING, null=False)
    
    
    def __str__(self):
        return self.student.nom+' '+self.student.prenom+' =>'+' '+self.ue.intitule+' : '+str(self.moy)+' en '+str(self.anneeaca)+' '+str(self.session.session)
      
    
    
    
    
    