# -*- coding: utf8 -*-

from django import forms
from deliberation.models import Jury, Header#, Impression
from base.models import Enseignant, Departement, Matiere, Niveau
from crispy_forms.layout import Div, Layout
from crispy_forms.helper import FormHelper
from bootstrap_datepicker_plus import DatePickerInput

class Creer_jury(forms.ModelForm):
    """
    """
    class Meta:
        model=Jury
        fields='__all__'
        
        widgets={
            'intitule':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Entrez le nom du Jury'}),
            'anneeUniv':forms.Select(attrs={'class':'form-control', 'placeholder':'Choisissez l\'année'}),
            'niveau':forms.Select(attrs={'class':'form-control', 'placeholder':'Choisissez le niveau du Jury'}),
            'departement':forms.Select(attrs={'class':'form-control', 'placeholder':'Choisissez le departement du Jury'}),
            'nbre_membre':forms.NumberInput(attrs={'class':'form-control', 'placeholder':'Entrez le nombre de membre'})
            }
        
    def clean(self):
        cleaned_data=super(Creer_jury, self).clean()
        
        if cleaned_data:
            return cleaned_data
        else :
            raise forms.ValidationError('Erreur, le formulare n\'est pas correctement remplit')
        
class Membre_jury(forms.Form):
    """
    """
    ens1=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}))
    ens2=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}))
    ens3=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}))
    ens4=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}))
    ens5=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}))
    ens6=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}), required=False)
    ens7=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}), required=False)
    ens8=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}), required=False)
    ens9=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}), required=False)
    ens10=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}), required=False)
    ens11=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True), label="Enseignant ", widget=forms.Select(attrs={'class':'form-control'}), required=False)
    membre, secretaire, president='Membre', 'Secrétaire', 'Président'
    FONCTION=((membre, 'Membre'), (secretaire, 'Secrétaire'),(president, 'Président'))
    fonction1= forms.ChoiceField(choices=FONCTION, label="Fontion")
    fonction2= forms.ChoiceField(choices=FONCTION, label="Fontion")
    fonction3= forms.ChoiceField(choices=FONCTION, label="Fontion")
    fonction4= forms.ChoiceField(choices=FONCTION, label="Fontion")
    fonction5= forms.ChoiceField(choices=FONCTION, label="Fontion")
    fonction6= forms.ChoiceField(choices=FONCTION, label="Fontion", required=False)
    fonction7= forms.ChoiceField(choices=FONCTION, label="Fontion", required=False)
    fonction8= forms.ChoiceField(choices=FONCTION, label="Fontion", required=False)
    fonction9= forms.ChoiceField(choices=FONCTION, label="Fontion", required=False)
    fonction10= forms.ChoiceField(choices=FONCTION, label="Fontion", required=False)
    fonction11= forms.ChoiceField(choices=FONCTION, label="Fontion", required=False)
    
    def clean(self):
        """
        """
        cleaned_data=super(Membre_jury, self).clean()
        
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError('Erreur le formulaire est mal remplit')

class ImpressionForm(forms.Form):
    """
    """
    signataire=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True, titre = "Chef de département"), label="", widget=forms.Select(attrs={'class':'form-control'}))
    date_imp=forms.CharField(label="", widget=DatePickerInput(options={'format':'DD/MM/YYYY'}, attrs={'data-toggle':'popover','data-content':'Veuillez choisir la date de d\'impression',
                'class':"form-control", 'placeholder':'DD/MM/YYYY'}))
                
    def clean(self):
        """
        """
        cleaned_data=super(ImpressionForm, self).clean()
        
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError('Erreur le formulaire est mal remplit')

    
    
    
class Saisie_note(forms.Form):
    """
    """
    
    departement=forms.ModelChoiceField(queryset=Departement.objects.all(), label="Département ", widget=forms.Select(attrs={'class':'form-control'}))
    niveau=forms.ModelChoiceField(queryset=Niveau.objects.all(), label="Niveau ", widget=forms.Select(attrs={'class':'form-control'}))
    matiere=forms.ModelChoiceField(queryset=Matiere.objects.all(), label="Matière ", widget=forms.Select(attrs={'class':'form-control'}))
    
    def clean(self):
        """
        """
        cleaned_data=super(Saisie_note, self).clean()
        
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError('Erreur le formulaire est mal remplit')
        
        
class HeaderForm(forms.ModelForm):
    """
    Le formulaire d'entête du PV
    """
    
    class Meta:
        model=Header
        fields = '__all__' 
        
        widgets={
            'universite':forms.TextInput(attrs={'class':'form-control','placeholder':'nom de l\'université','data-toggle':'popover','data-content':'Entrer le nom de l\'université'}),
            'titre':forms.TextInput(attrs={'class':'form-control','placeholder':'titre du pv','data-toggle':'popover','data-content':'Entrer le titre'}),
            'service':forms.TextInput(attrs={'class':'form-control','placeholder':'service universitaire','data-toggle':'popover','data-content':'Entrer le service en charge'}),
            'session':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir la session'}),
            'option':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir l\'option'}),
            'type_delib':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le type de délibération'}),
            'tel':forms.TextInput(attrs={'class':'form-control','placeholder':'N° de téléphone','data-toggle':'popover','data-content':'Entrer le numero de téléphone'}),
            'telex':forms.TextInput(attrs={'class':'form-control','placeholder':'N° telex ','data-toggle':'popover','data-content':'Entrer le numero telex'}),
            'fax':forms.TextInput(attrs={'class':'form-control','placeholder':'N° fax','data-toggle':'popover','data-content':'Entrer le fax'}),
            'adresse':forms.TextInput(attrs={'class':'form-control','placeholder':'adresse postale','data-toggle':'popover','data-content':'Entrer l\'adresse postale'}),
            'date_delib': DatePickerInput(options={'format':'DD/MM/YYYY'}, attrs={'data-toggle':'popover','data-content':'Veuillez entrez la date de délibération',
                'class':"form-control", 'placeholder':'DD/MM/YYYY'}),
            'imp_pv':forms.CheckboxInput(attrs={'class':'','data-toggle':'popover','data-content':'Voulez vous imprimer le PV ?'}),
            'imp_jur':forms.CheckboxInput(attrs={'class':'','data-toggle':'popover','data-content':'Voulez vous imprimer le PV ?'}),
            'imp_adm':forms.CheckboxInput(attrs={'class':'','data-toggle':'popover','data-content':'Voulez vous imprimer le PV ?'}),
            'imp_pvd':forms.CheckboxInput(attrs={'class':'','data-toggle':'popover','data-content':'Voulez vous imprimer le PV ?'}),
            
            }
        
        
    @property
    def helper(self):
        helper=FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.label_class=''
        helper.form_class='col-12'
        helper.layout=Layout(
            Div(
                Div(Div('universite', css_class="col-6"),Div('titre',css_class="col-6"),css_class="row"),
                Div(Div('service', css_class="col-6"),Div('adresse',css_class="col-6"),css_class="row"),
                Div(Div('option',css_class="col-4"),Div('date_delib', css_class="col-4"),Div('session', css_class="col-4"),css_class="row"),
                Div(Div('imp_pv',css_class="col-3"),Div('imp_adm', css_class="col-3"),Div('imp_jur', css_class="col-3"),Div('imp_pvd', css_class="col-3"),css_class="row"),
                Div(Div('tel',css_class="col-4"),Div('telex', css_class="col-4"),Div('fax', css_class="col-4"),css_class="row")
                ,css_class='form-group')
        )
        return helper
        
        
    def clean(self):
        
        cleaned_data=super(HeaderForm, self).clean()
        
        if cleaned_data:
            
            return cleaned_data
        else:
            raise forms.ValidationError("Veuillez remplir correctement le formulaire")
        
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        