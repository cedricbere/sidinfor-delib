# Generated by Django 2.2.3 on 2019-10-10 23:28

import base.other
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AnneeAcad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('debut', models.IntegerField(default=2000)),
                ('fin', models.IntegerField(default=2001)),
                ('an', models.CharField(max_length=10, unique=True, verbose_name='Année Académique')),
            ],
        ),
        migrations.CreateModel(
            name='AnneeUniv',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('debut', models.IntegerField(default=2000)),
                ('fin', models.IntegerField(default=2001)),
                ('an', models.CharField(max_length=10, unique=True, verbose_name='Année Universitaire')),
            ],
        ),
        migrations.CreateModel(
            name='Departement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('intitule', models.CharField(max_length=20, verbose_name='Nom du département ')),
                ('couleur', models.CharField(max_length=10, verbose_name='Couleur du departement')),
            ],
        ),
        migrations.CreateModel(
            name='Niveau',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('intitule', models.CharField(max_length=25, verbose_name='Niveau (En Toute Lettre)')),
                ('abrege', models.CharField(max_length=10, verbose_name='Niveau (En Abrégé) ')),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=100, verbose_name='Nom de Famille')),
                ('prenom', models.CharField(max_length=150, verbose_name='Prénom(s)')),
                ('matricule', models.CharField(max_length=20, unique=True, verbose_name='Numero Matricule')),
                ('sexe', models.CharField(choices=[('Homme', 'Homme'), ('Femme', 'Femme')], default='Homme', max_length=10)),
                ('date_naissance', models.CharField(blank=True, max_length=50, null=True, verbose_name='Date de Naissance')),
                ('lieu_naissance', models.CharField(blank=True, max_length=50, null=True, verbose_name='Lieu de Naissance')),
                ('num_tel', models.CharField(blank=True, max_length=50, null=True, unique=True, verbose_name='Numéro de téléphone')),
                ('profil', models.ImageField(blank=True, null=True, upload_to=base.other.chemin_de_sauvegarde_image_profile, verbose_name='Photo de profil')),
            ],
        ),
        migrations.CreateModel(
            name='Ufr',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('intitule', models.CharField(max_length=50, verbose_name='Unité de Formation et de Recherche (UFR)')),
                ('code', models.CharField(max_length=10, verbose_name='Abréviation UFR ')),
            ],
        ),
        migrations.CreateModel(
            name='AgentScolarite',
            fields=[
                ('person_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='base.Person')),
                ('titre', models.CharField(choices=[('Chef de la scolarité', 'Chef de la scolarité'), ('Agent', 'Agent')], default='Agent', max_length=25)),
                ('compte', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
                ('ufr', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='base.Ufr')),
            ],
            bases=('base.person',),
        ),
        migrations.CreateModel(
            name='Enseignant',
            fields=[
                ('person_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='base.Person')),
                ('grade', models.CharField(choices=[('Professeur Titulaire', 'Professeur Titulaire'), ('Maître de Conférence', 'Maître de Conférence'), ('Maître Assistant(e)', 'Maître Assistant(e)'), ('Monsieur', 'Monsieur'), ('Assistant', 'Assistant')], default='Monsieur', max_length=20)),
                ('titre', models.CharField(choices=[('Chef de département', 'Chef de département'), ('Enseignant', 'Enseignant'), ('Directeur', 'Directeur')], default='Enseignant', max_length=25)),
                ('statut', models.CharField(choices=[('Permanent', 'Permanent'), ('Non permanent', 'Non permanent')], default='Permanent', max_length=15)),
                ('compte', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            bases=('base.person',),
        ),
        migrations.CreateModel(
            name='UserCode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=8, unique=True)),
                ('user', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Code utilisateur',
                'verbose_name_plural': 'Codes utilisateurs',
            },
        ),
        migrations.CreateModel(
            name='UE',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('intitule', models.CharField(max_length=200, verbose_name="Unité d'enseignement ")),
                ('coef', models.IntegerField(verbose_name='Coefficient')),
                ('code', models.CharField(max_length=25, verbose_name="Code de l'UE ")),
                ('volHoraire', models.IntegerField(verbose_name='Volume horaire')),
                ('is_actif', models.BooleanField(default=True)),
                ('departement', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='base.Departement')),
                ('niveau', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='base.Niveau')),
            ],
        ),
        migrations.CreateModel(
            name='Matiere',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('intitule', models.CharField(max_length=200, verbose_name='Nom de la Matière ')),
                ('code', models.CharField(max_length=25, verbose_name='Code de la matière ')),
                ('coef', models.IntegerField(verbose_name='Coefficient')),
                ('volHoraire', models.IntegerField(verbose_name='Volume horaire')),
                ('is_actif', models.BooleanField(default=True)),
                ('is_affecte', models.BooleanField(default=False)),
                ('uniteEnseign', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='base.UE', verbose_name="Unité d'enseignement")),
            ],
        ),
        migrations.AddField(
            model_name='departement',
            name='ufr',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='base.Ufr', verbose_name='UFR'),
        ),
        migrations.CreateModel(
            name='Notifications',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('information', models.CharField(max_length=200, verbose_name='Nouvelle')),
                ('url', models.URLField(max_length=300)),
                ('date', models.DateField(auto_now=True, verbose_name="Date de l'evenement")),
                ('is_actif', models.BooleanField(default=True)),
                ('enseignant', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='base.Enseignant')),
                ('scolarite', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='base.AgentScolarite')),
            ],
        ),
        migrations.CreateModel(
            name='Etudiant',
            fields=[
                ('person_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='base.Person')),
                ('statut', models.CharField(choices=[('Boursier(ière)', 'Boursier(ière)'), ('Non boursier(ière)', ' Non Boursier(ière)')], default='Boursier(ière)', max_length=20)),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='Adresse Email')),
                ('is_actif', models.BooleanField(default=False)),
                ('is_ancien', models.BooleanField(default=False)),
                ('anneeAca', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='base.AnneeAcad', verbose_name='Année académique')),
                ('departement', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='base.Departement')),
                ('niveau', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='base.Niveau')),
            ],
            bases=('base.person',),
        ),
        migrations.AddField(
            model_name='enseignant',
            name='departement',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='base.Departement'),
        ),
    ]
