# -*- coding: utf8 -*-

from base.other import chemin_de_sauvegarde_image_profile
from django.db import models
from django.contrib.auth.models import User

"""
class EnsUser(User) :
    
    type_user=models.CharField("Type Utilisateur", max_length=50,null=False, default="enseignant")
    
class adminUser(User):
    
    type_user=models.CharField("Type Utilisateur", max_length=50,null=False, default="admin")

class SecreJuryUser(User):
    
    type_user=models.CharField("Type Utilisateur", max_length=50, null=False, default="secrétaire")
    
class PresiJuryUser(User):
    
    type_user=models.CharField("Type Utilisateur", max_length=50, null=False, default="président")
    
class scolUser(User):
    
    type_user=models.CharField("Type Utilisateur", max_length=50, null=False, default="scolarite")
    
"""
    
    
    


class Person(models.Model):
    """
    """
    nom = models.CharField("Nom de Famille",max_length=100, null=False, blank=False)
    prenom=models.CharField("Prénom(s)", max_length=150, null=False,blank=False)
    matricule=models.CharField("Numero Matricule",max_length=20, null= False, blank=False,unique=True)
    Homme, Femme='Homme','Femme'
    SEXE=( (Homme, 'Homme'),(Femme, 'Femme') )
    sexe=models.CharField(choices=SEXE, max_length=10, default="Homme", null=False, blank=False)
    #age=models.IntegerField(null=True, blank=True)
    date_naissance=models.CharField("Date de Naissance", null=False, max_length=50)
    lieu_naissance=models.CharField("Lieu de Naissance",max_length=50, null=False)
    #email=models.EmailField("Adresse Email", unique=True, null=False)
    num_tel=models.CharField("Numéro de téléphone", max_length=50, unique=True, null=True, blank=True)
    
    profil=models.ImageField(verbose_name="Photo de profil", upload_to=chemin_de_sauvegarde_image_profile, null=True, blank=True, unique=False)
    type_user=''
    
    def __str__(self):
        return self.prenom+" "+self.nom
    
    
class Etudiant(Person):
    """
    """
    departement=models.ForeignKey('Departement', on_delete = models.DO_NOTHING,null=False)
    boursier, non_boursier ='Boursier(ière)','Non boursier(ière)'
    STATUT=((boursier,'Boursier(ière)'),( non_boursier,' Non Boursier(ière)'))
    statut= models.CharField(choices=STATUT, max_length=20, default='Boursier(ière)', null=False,blank=False)
    
    email=models.EmailField("Adresse Email", null = False, blank=False,unique=True)
    niveau= models.ForeignKey('Niveau', on_delete = models.DO_NOTHING, null=False)
    anneeAca=models.ForeignKey('AnneeAcad',verbose_name="Année académique", on_delete = models.DO_NOTHING, null=False)
    is_actif=models.BooleanField(default=False)
    is_ancien=models.BooleanField(default=False)
    type_user='Etudiant'
    
    def __str__(self):
        return "Etudiant(e) "+self.prenom+" "+self.nom+" Matricule : "+self.matricule+" Niveau : "+str(self.niveau)
    

class Enseignant(Person):
    """
    """ 
    departement=models.ForeignKey('Departement', on_delete = models.DO_NOTHING,null=False)
    prof, m_conference, m_assistant, monsieur, assist = 'Professeur Titulaire','Maître de Conférence','Maître Assistant(e)','Monsieur', 'Assistant'
    GRADE=(
        (prof, 'Professeur Titulaire'),
        (m_conference, 'Maître de Conférence'),
        (m_assistant,'Maître Assistant(e)'),
        (monsieur, 'Monsieur'),
        (assist, 'Assistant')
        )
    grade=models.CharField(choices=GRADE, max_length=20, default='Monsieur')
    chef_dep, enseign, direct= 'Chef de département', 'Enseignant', 'Directeur'
    TITRE=(
        (chef_dep, 'Chef de département'),
        (enseign, 'Enseignant'),
        (direct, 'Directeur')
        )
    titre=models.CharField(choices=TITRE, max_length=25, null=False, default='Enseignant')
    compte=models.OneToOneField(User, on_delete=models.DO_NOTHING,null=False)
    permanent, non_permanent='Permanent','Non permanent'
    STAT=( (permanent, 'Permanent'),(non_permanent, 'Non permanent') )
    statut=models.CharField(choices=STAT, max_length=15, default="Permanent", null=False, blank=False)
    is_ancien=models.BooleanField(default=False)
    type_user='Enseignant'
    
    def __str__(self):
        
        return self.grade+" "+self.prenom+" "+self.nom


class AgentScolarite(Person):
    """
    """
    ufr=models.ForeignKey('Ufr', on_delete = models.DO_NOTHING,null=False)
    agent, chef_sco='Agent', 'Chef de la scolarité'
    TITRE=(
        (chef_sco, 'Chef de la scolarité'),
        (agent, 'Agent')
        )
    titre=models.CharField(choices=TITRE, max_length=25, null=False, default='Agent')
    compte=models.OneToOneField(User, on_delete=models.DO_NOTHING,null=False)
    is_ancien=models.BooleanField(default=False)
    type_user='Scolarite'
    

class UE(models.Model):
    
    intitule= models.CharField("Unité d'enseignement ", max_length=200)
    coef=models.IntegerField("Coefficient")
    code = models.CharField("Code de l'UE ", max_length=25,null=False)
    volHoraire=models.IntegerField("Volume horaire")
    niveau=models.ForeignKey('Niveau', on_delete = models.DO_NOTHING)
    departement=models.ForeignKey('Departement',on_delete = models.DO_NOTHING)
    is_actif=models.BooleanField(default=True)
    
    def __str__(self):
        return "Unité d'enseignement : " +self.intitule+ " Coef: "+str(self.coef) 
    

class Matiere(models.Model):
    
    intitule= models.CharField("Nom de la Matière ", max_length=200)
    code = models.CharField("Code de la matière ", max_length=25, null=False)
    coef= models.IntegerField("Coefficient")
    volHoraire= models.IntegerField("Volume horaire")
    #teacher= models.ForeignKey(Enseignant ,verbose_name="Enseignant", on_delete = models.DO_NOTHING)
    uniteEnseign=models.ForeignKey(UE, verbose_name="Unité d'enseignement",on_delete = models.DO_NOTHING)
    is_actif=models.BooleanField(default=True)
    is_affecte=models.BooleanField(default=False)
    
    def __str__(self):
        return self.intitule+ " Coef: "+ str(self.coef)
    

    
class Ufr(models.Model):
    intitule = models.CharField("Unité de Formation et de Recherche (UFR)", max_length=50)
    code = models.CharField("Abréviation UFR ", max_length=10)
    
    def __str__(self):
        return self.code

    
class Departement(models.Model):
    
    intitule = models.CharField("Nom du département ", max_length=20)
    couleur = models.CharField("Couleur du departement",max_length=10)
    ufr=models.ForeignKey(Ufr,verbose_name="UFR",on_delete = models.DO_NOTHING)
    
    def __str__(self):
        return self.intitule
    
    
class Niveau (models.Model):
    intitule = models.CharField( "Niveau (En Toute Lettre)", max_length=25)
    abrege= models.CharField("Niveau (En Abrégé) ", max_length=10)

    
    def __str__(self):
        return self.abrege
    
class AnneeUniv(models.Model):
    """
    """
    debut=models.IntegerField(null=False, default=2000)
    fin=models.IntegerField(null=False, default=2001)
    an=models.CharField("Année Universitaire", max_length=10, unique=True)
     
    def __str__(self):
        return self.an
    
class AnneeAcad(models.Model):
    """
    """
    debut=models.IntegerField(null=False, default=2000)
    fin=models.IntegerField(null=False, default=2001)
    an=models.CharField("Année Académique", max_length=10, unique=True)
     
    def __str__(self):
        return self.an
    
    
    
class UserCode(models.Model):
    """
    """
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)
    code = models.CharField(max_length=8, unique=True, null=False)
    
    def __str__(self):
        return self.user.__str__() 

    class Meta:
        verbose_name = 'Code utilisateur'
        verbose_name_plural = 'Codes utilisateurs'
        

class Notifications(models.Model):
    """
    """
    scolarite=models.ForeignKey(AgentScolarite, on_delete=models.DO_NOTHING,null=True)
    enseignant=models.ForeignKey(Enseignant, on_delete=models.DO_NOTHING,null=True)
    information=models.CharField("Nouvelle", max_length=200)
    url=models.URLField(max_length=300)
    date=models.DateField(auto_now=True, verbose_name="Date de l'evenement")
    is_actif=models.BooleanField(null=False, default=True)
    
    def __str__(self):
        
        return self.information+" => "+str(self.enseignant)





    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
