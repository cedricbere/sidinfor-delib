#!/usr/bin/env python
# -*- coding: utf8 -*-


from django.shortcuts import render, redirect
from base.forms import EtudiantForm, EnseignantForm, LoginForm, Create_userForm,ScolariteForm#UEForm,UfrForm,DepartementForm
from django.contrib.auth.models import User, Group
from gestion_enseignement.models import Enseign_An_Matiere, Vol_Statutaire
from base.models import Enseignant,Etudiant,Person,\
    AgentScolarite#, UserCode
from django.http import JsonResponse#, HttpResponse
from django.core.serializers import serialize
#from base.forms import Rech_specifique, Rech_speci_ufr, Rech_speci_departement,\
from base.forms import    Recherche_form, AnneeForm, MatiereForm, UEForm,\
    Recherche_Matiere
from django import forms
#from wkhtmltopdf.views import PDFTemplateView
#from wkhtmltopdf.utils import wkhtmltopdf
from wkhtmltopdf.views import PDFTemplateResponse
from deliberation.models import Cursus, Formation_Jury#, Jury
from base.other import envoyer_Mail_Inscrit, envoi_Mail_Admin
from django.views.generic.dates import timezone_today
from base.models import AnneeUniv, Notifications, Matiere, Departement#, Ufr
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from base.models import UE, Niveau, Ufr, AnneeAcad
from _datetime import datetime
from deliberation.models import Note, Releve

def index(request):
    """
    """
    #an=AnneeUniv.objects.all()
    #nbreAn=len(an)
    anU=AnneeAcad.objects.last()
    return render(request,'index.html',{'annee_univ':anU})

def annee_Univ(request):
    """
    """
    
    if request.POST:
        
        anneeform=AnneeForm(request.POST)
        if anneeform.is_valid():
            anneeform.save()
    
    liste_annee=AnneeUniv.objects.all()
    anU=AnneeUniv.objects.last()
    anneeform=AnneeForm()
    context={'anneeform':anneeform,'liste_annee':liste_annee,'annee_univ':anU}
    return render(request, 'annee_univ.html', context)

def etudiant_pdf(request, nom, prenom, matricule, ufr, departement, niveau):
    """
    Cette vue permet d'imprimer la liste des étudiants en fonction du tri
    
    """
    logged_user=form_user(request)
    if logged_user:
        if nom == "n":
            nom=""
        
        if prenom == 'p':
            prenom =""
            
        if matricule == 'm':
            matricule = ""
        
        if ufr == "u":
            ufr=None
        elif ufr != "u":
            uf=Ufr.objects.get(code=ufr)
            ufr =uf
        
        if departement == "d":
            departement=None
        elif departement != "d":
            dep=Departement.objects.get(intitule = departement)
            departement=dep
        
        if niveau == "n":
            niveau =None
        elif niveau != "n":
            niv=Niveau.objects.get(abrege=niveau)
            niveau=niv
        
        if nom!="" and prenom!="" and matricule!="" and departement!=None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement,departement__ufr=ufr,niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
            
        # si aucun element n'est null
        
        elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, departement=departement,departement__ufr=ufr,niveau=niveau, is_actif=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+" Departement : "+departement+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+" Departement : "+departement+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Departement : "+departement+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule ,departement__ufr=ufr,niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom!="" and matricule!="" and departement!=None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+" Departement : "+str(departement)+""+" Niveau : "+str(niveau)
          
        elif nom!="" and prenom!="" and matricule!="" and departement!=None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+" Departement : "+str(departement)+""+" Niveau : "+str(niveau)
            
        # fin de si un element est null
        
        elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter( matricule=matricule, departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Matricule : "+matricule+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter( prenom=prenom, departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Prenom : "+prenom+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter( prenom=prenom, matricule=matricule, departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, departement=departement, niveau=niveau, is_actif=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+" Departement : "+str(departement)+""+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, departement=departement, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+" Departement : "+str(departement)+""+" Ufr : "+str(ufr)
        
        elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom,  departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement=departement, niveau=niveau, is_actif=True)
            titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+", Departement : "+str(departement)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement=departement, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+" Departement : "+str(departement)+""+" Ufr : "+str(ufr)
        
        elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement=departement, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Departement : "+str(departement)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement=departement, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Departement : "+str(departement)+""+" Ufr : "+str(ufr)
        
        elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule,departement__ufr=ufr, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+""+" Ufr : "+str(ufr)
        
        elif nom!="" and prenom!="" and matricule!="" and departement!=None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+""+" Departement : "+str(departement)
            
        # combinaison de deux champs vide
        
        elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Ufr : "+str(ufr)+" Departement : "+str(departement)+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(matricule=matricule, departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Matricule : "+matricule+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(matricule=matricule, departement=departement, niveau=niveau, is_actif=True)
            titre_recherche=" Matricule : "+matricule+" Departement : "+str(departement)+" Niveau : "+str(niveau)
            
        elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(matricule=matricule, departement=departement, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Matricule : "+matricule+" Departement : "+str(departement)+" Ufr : "+str(ufr)
        
        elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(prenom=prenom,departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Prenom : "+prenom+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter( prenom=prenom, departement=departement, niveau=niveau, is_actif=True)
            titre_recherche=" Prenom : "+prenom+" Departement : "+str(departement)+""+" Niveau : "+str(niveau)
            
        elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(prenom=prenom, departement=departement, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Nom : "+nom+" Ufr : "+str(ufr)+""+" Departement : "+str(departement)
        
        elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom,departement__ufr=ufr, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, departement=departement,niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Departement : "+str(departement)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, departement=departement,departement__ufr=ufr, is_actif=True)
            titre_recherche=" Nom : "+nom+" Departement : "+str(departement)+""+" Ufr : "+str(ufr) #ici
        
        elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+""+" Niveau : "+str(niveau)
            
        elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, niveau=niveau, is_actif=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+""+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+""+" Ufr : "+str(ufr)
        
        elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, departement=departement, is_actif=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+""+" Departement : "+str(departement)
        
        elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, matricule=matricule,niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+""+" Ufr : "+str(ufr)
        
        elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement=departement, is_actif=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+""+" Departement : "+str(departement)
        
        elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Ufr"+str(ufr)+" Departement : "+str(departement)
        
        elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement=departement, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Departement : "+str(departement)
        
        elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, is_actif=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule
        
        # combinaison de trois champs vide
        
        elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr!=None and niveau!=None:
            liste=Etudiant.objects.filter(departement__ufr=ufr,niveau=niveau, is_actif=True)
            titre_recherche=" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter( departement=departement,niveau=niveau, is_actif=True)
            titre_recherche=" Departement : "+str(departement)+""+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(prenom=prenom, niveau=niveau, is_actif=True)
            titre_recherche=" Prenom : "+prenom+""+" Niveau : "+str(niveau)
        
        elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(matricule=matricule, niveau=niveau, is_actif=True)
            titre_recherche=" Matricule : "+matricule+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(nom=nom, niveau=niveau, is_actif=True)
            titre_recherche=" Nom : "+nom+""+" Niveau : "+str(niveau)
        #ici
        
        elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(departement__ufr=ufr, departement=departement, is_actif=True)
            titre_recherche=" Ufr : "+str(ufr)+""+" Departement : "+str(departement)
        
        elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter( departement=departement,prenom=prenom, is_actif=True)
            titre_recherche=" Prenom : "+prenom+""+" Departement : "+str(departement)+""
        
        elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(prenom=prenom,niveau=niveau, is_actif=True)
            titre_recherche=" Prenom : "+prenom+""+" Niveau : "+str(niveau)+""
        
        elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, is_actif=True)
            titre_recherche=" Prenom : "+prenom+""+" Matricule : "+matricule+"" #la
        
        elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(matricule=matricule, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Matricule : "+matricule+""+" Ufr : "+str(ufr)
        
        #ici
        elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(departement__ufr=ufr,niveau=niveau, is_actif=True)
            titre_recherche=" Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
        
        elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, departement=departement, is_actif=True)
            titre_recherche=" Nom : "+nom+""+" Departement : "+str(departement)
        
        elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, departement__ufr=ufr, is_actif=True)
            titre_recherche=" Nom : "+nom+""+" Ufr : "+str(ufr)
        
        elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, matricule=matricule, is_actif=True)
            titre_recherche=" Nom : "+nom+""+" Matricule : "+matricule
        
        elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, prenom=prenom, is_actif=True)
            titre_recherche=" Nom : "+nom+""+" Prenom : "+prenom
        
        # combinaison de 4 champs vide
        
        elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(nom=nom, is_actif=True)
            titre_recherche=" Nom : "+nom
        
        elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(prenom=prenom, is_actif=True)
            titre_recherche=" Prenom : "+prenom
        
        elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(matricule=matricule, is_actif=True)
            titre_recherche=" Matricule : "+matricule
        
        elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr==None and niveau==None:
            print(departement.id)
            liste=Etudiant.objects.filter(departement=departement, is_actif=True)
            titre_recherche=" Departement : "+str(departement)
        
        elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr!=None and niveau==None:
            liste=Etudiant.objects.filter(departement__ufr=ufr, is_actif=True)
            titre_recherche=" Ufr : "+str(ufr)
        
        elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr==None and niveau!=None:
            liste=Etudiant.objects.filter(niveau=niveau, is_actif=True)
            titre_recherche=" Niveau : "+str(niveau)
        
        # combinaison de 5 champs vide
            
        # Tous les champs sont vide
            
        elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr==None and niveau==None:
            liste=Etudiant.objects.filter(is_actif=True)
            titre_recherche="Tous les Étudiants"


        context={'liste': liste, 'stitre':titre_recherche}
        
        return PDFTemplateResponse(request=request, template='liste_etudiants_pdf.html', context = context,
                                   filename='liste_etudiant'+titre_recherche+'.pdf', 
                                   cmd_options={'orientation':'landscape'}, show_content_in_browser = False)
   
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def enseignant_pdf(request, nom, prenom, matricule, ufr, departement):
    
    """
    Cette vue permet d'imprimer la liste des étudiants en fonction du tri
    
    """
    logged_user=form_user(request)
    if logged_user:
        if nom == "n":
            nom=""
        
        if prenom == 'p':
            prenom =""
            
        if matricule == 'm':
            matricule = ""
        
        if ufr == "u":
            ufr=None
        elif ufr != "u":
            uf=Ufr.objects.get(code=ufr)
            ufr =uf
        
        if departement == "d":
            departement=None
        elif departement != "d":
            dep=Departement.objects.get(intitule = departement)
            departement=dep
    
    
        if nom!="" and prenom!="" and matricule!="" and departement!=None and ufr!=None:
            liste=Enseignant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""
            
        # si aucun element n'est null
        
        elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr!=None:
            liste=Enseignant.objects.filter(prenom=prenom, matricule=matricule, departement=departement,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""
        
        elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr!=None:
            liste=Enseignant.objects.filter(nom=nom, matricule=matricule, departement=departement,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""
        
        elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr!=None:
            liste=Enseignant.objects.filter(nom=nom, prenom=prenom, departement=departement,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""
        
        elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr!=None:
            liste=Enseignant.objects.filter(nom=nom, prenom=prenom, matricule=matricule ,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+" Ufr : "+str(ufr)+""
        
        elif nom!="" and prenom!="" and matricule!="" and departement!=None and ufr==None:
            liste=Enseignant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+" Departement : "+str(departement)+""
            
        # fin de si un element est null
        
        elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr!=None:
            liste=Enseignant.objects.filter( matricule=matricule, departement=departement,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Matricule : "+matricule+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""
        
        elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr!=None:
            liste=Enseignant.objects.filter( prenom=prenom, departement=departement,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""
        
        elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr!=None:
            liste=Enseignant.objects.filter( prenom=prenom, matricule=matricule, departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+" Ufr : "+str(ufr)+""
        
        elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr==None:
            liste=Enseignant.objects.filter(prenom=prenom, matricule=matricule, departement=departement, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+" Departement : "+str(departement)+""
        
        elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr!=None:
            liste=Enseignant.objects.filter(nom=nom,  departement=departement,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Departement : "+str(departement)+" Ufr : "+str(ufr)+""
        
        elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr!=None:
            liste=Enseignant.objects.filter(nom=nom, matricule=matricule, departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+" Ufr : "+str(ufr)+""
        
        elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr==None:
            liste=Enseignant.objects.filter(nom=nom, matricule=matricule, departement=departement, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+" Departement : "+str(departement)+""
        
        elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr!=None:
            liste=Enseignant.objects.filter(nom=nom, prenom=prenom, departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Ufr : "+str(ufr)+""
        
        elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr==None:
            liste=Enseignant.objects.filter(nom=nom, prenom=prenom, departement=departement, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Departement : "+str(departement)+""
        
        elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr==None:
            liste=Enseignant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule+""
            
        # combinaison de deux champs vide
        
        elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr!=None:
            liste=Enseignant.objects.filter(departement=departement,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Ufr : "+str(ufr)+" Departement : "+str(departement)
        
        elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr!=None:
            liste=Enseignant.objects.filter(matricule=matricule, departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Matricule : "+matricule+" Ufr : "+str(ufr)+""
        
        elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr==None:
            liste=Enseignant.objects.filter(matricule=matricule, departement=departement, compte__is_active=True)
            titre_recherche=" Matricule : "+matricule+" Departement : "+str(departement)
        
        elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr!=None:
            liste=Enseignant.objects.filter(prenom=prenom,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom+" Ufr : "+str(ufr)+""
        
        elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr==None:
            liste=Enseignant.objects.filter( prenom=prenom, departement=departement, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom+" Departement : "+str(departement)+""
            
        elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr!=None:
            liste=Enseignant.objects.filter(nom=nom,ufr=ufr, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Ufr : "+str(ufr)+""
        
        elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr==None:
            liste=Enseignant.objects.filter(nom=nom, departement=departement, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Departement : "+str(departement)+""
        
        elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr==None:
            liste=Enseignant.objects.filter(nom=nom, prenom=prenom, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+""
            
        elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr==None:
            liste=Enseignant.objects.filter(prenom=prenom, matricule=matricule, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule+""
        
        elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr==None:
            liste=Enseignant.objects.filter(nom=nom, matricule=matricule, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+""
        
        # combinaison de trois champs vide
        
        elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr!=None:
            liste=Enseignant.objects.filter(departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Ufr : "+str(ufr)+""
        
        elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr==None:
            liste=Enseignant.objects.filter( departement=departement, compte__is_active=True)
            titre_recherche=" Departement : "+str(departement)+""
        
        elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr==None:
            liste=Enseignant.objects.filter(prenom=prenom, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom+""
        
        elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr==None:
            liste=Enseignant.objects.filter(matricule=matricule, compte__is_active=True)
            titre_recherche=" Matricule : "+matricule+""
        
        elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr==None:
            liste=Enseignant.objects.filter(nom=nom, compte__is_active=True)
            titre_recherche=" Nom : "+nom+""
            
        elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr==None:
            liste=Enseignant.objects.filter(compte__is_active=True)
            titre_recherche="Tous les Enseignants"
            
    
        context=dict(liste=liste, stitre=titre_recherche)
        return PDFTemplateResponse(request=request, template='liste_enseignants_pdf.html', context = context,
                                   filename='liste_'+titre_recherche+'.pdf', show_content_in_browser = False,
                                   footer_template='footer_template.html')

    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def scolarite_pdf(request, nom, prenom, matricule, ufr):
    """
    Cette vue permet d'imprimer la liste des agents de la scolarité en fonction du tri
    
    """
    logged_user=form_user(request)
    if logged_user:
        if nom == "n":
            nom=""
        
        if prenom == 'p':
            prenom =""
            
        if matricule == 'm':
            matricule = ""
        
        if ufr == "u":
            ufr=None
        elif ufr != "u":
            uf=Ufr.objects.get(code=ufr)
            ufr =uf
        
        if nom=="" and prenom=="" and matricule=="" and ufr==None:
            liste=AgentScolarite.objects.filter(compte__is_active=True)
            titre_recherche="Tous les agents de la scolarité"
            
        elif nom!="" and prenom=="" and matricule==""  and ufr==None:
            liste=AgentScolarite.objects.filter(nom=nom, compte__is_active=True)
            titre_recherche=" Nom : "+nom
        
        elif nom=="" and prenom!="" and matricule=="" and ufr==None:
            liste=AgentScolarite.objects.filter(prenom=prenom, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom
            
        elif nom=="" and prenom=="" and matricule!="" and ufr==None:
            liste=AgentScolarite.objects.filter(matricule=matricule, compte__is_active=True)
            titre_recherche=" Matricule : "+matricule
            
        elif nom=="" and prenom=="" and matricule=="" and ufr!=None:
            liste=AgentScolarite.objects.filter(departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Ufr : "+str(ufr)
        
        elif nom!="" and prenom!="" and matricule==""  and ufr==None:
            liste=AgentScolarite.objects.filter(nom=nom,prenom=prenom, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom
        
        elif nom=="" and prenom!="" and matricule!="" and ufr==None:
            liste=AgentScolarite.objects.filter(prenom=prenom, matricule=matricule, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom+" Matricule : "+matricule
            
        elif nom=="" and prenom=="" and matricule!="" and ufr!=None:
            liste=AgentScolarite.objects.filter(matricule=matricule, departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Matricule : "+matricule+" Ufr : "+str(ufr)
            
        elif nom=="" and prenom!="" and matricule=="" and ufr!=None:
            liste=AgentScolarite.objects.filter(prenom=prenom,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Prenom : "+prenom+" Ufr : "+str(ufr)
        
        elif nom!="" and prenom=="" and matricule=="" and ufr!=None:
            liste=AgentScolarite.objects.filter(nom=nom,departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Ufr : "+str(ufr)
        
        elif nom!="" and prenom=="" and matricule!="" and ufr==None:
            liste=AgentScolarite.objects.filter(nom=nom,matricule=matricule, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule
            
        elif nom!="" and prenom!="" and matricule!="" and ufr==None:
            liste=AgentScolarite.objects.filter(nom=nom,matricule=matricule, prenom=prenom, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Matricule : "+matricule
        
        elif nom!="" and prenom!="" and matricule=="" and ufr!=None:
            liste=AgentScolarite.objects.filter(nom=nom,departement__ufr=ufr, prenom=prenom, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Prenom : "+prenom+" Ufr : "+str(ufr)
        
        elif nom!="" and prenom=="" and matricule!="" and ufr!=None:
            liste=AgentScolarite.objects.filter(nom=nom,matricule=matricule, departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Nom : "+nom+" Matricule : "+matricule+" Ufr : "+str(ufr)
        
        elif nom=="" and prenom!="" and matricule!="" and ufr!=None:
            liste=AgentScolarite.objects.filter(prenom=prenom,matricule=matricule, departement__ufr=ufr, compte__is_active=True)
            titre_recherche=" Pre;om : "+prenom+" Matricule : "+matricule+" Ufr : "+str(ufr)
            
        elif nom!="" and prenom!="" and matricule!="" and ufr!=None:
            liste=AgentScolarite.objects.filter(nom=nom, prenom=prenom,matricule=matricule, departement__ufr=ufr, compte__is_active=True )
            titre_recherche=" Nom : "+nom+ " Prenom : "+prenom+" Matricule : "+matricule+" Ufr : "+str(ufr)
          
        
        
        
    
        context=dict(liste=liste, stitre=titre_recherche)
        return PDFTemplateResponse(request=request, template='liste_enseignants_pdf.html', context = context,
                                   filename='liste_'+titre_recherche+'.pdf', show_content_in_browser = False, footer_template='footer_template.html')

    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

"""
def save(request):
    
    info ="Bienvenue, vueillez effectuer votre choix ! "
    #etudiantForm =EtudiantForm(request.POST, request.FILES if (request.POST and (request.POST['profileType']=='etudiant')) else None, prefix="etd")
    
    #enseignantForm=EnseignantForm(request.POST, request.FILES if (request.POST and (request.POST['profileType']=='enseignant')) else None, prefix="ens")
    #create_userForm1=Create_userForm(request.POST if (request.POST and (request.POST['profileType']=='enseignant')) else None, prefix="ens")
    
    #scolariteForm=ScolariteForm(request.POST, request.FILES if (request.POST and (request.POST['profileType']=='scolarite')) else None, prefix="sco")
    #create_userForm2=Create_userForm(request.POST if (request.POST and (request.POST['profileType']=='scolarite')) else None, prefix="sco")
    
    
    if request.POST :
        
        if request.POST['profileType']=='etudiant':
            
            etudiantForm =EtudiantForm((request.POST, request.FILES),prefix="etd")
            
            if etudiantForm.is_valid():
                #print('Form valide !')
                
                etudiantForm.save()
                
                info="Sauvegarde effectuée avec succès ! :)"
            else :
                #print (etudiantForm.errors)
                info="Erreur dans le formulaire, vueillez vérifier !"
        
        elif request.POST['profileType']=='enseignant':
            
            enseignantForm=EnseignantForm(request.POST, request.FILES,prefix="ens")
            create_userForm1=Create_userForm(request.POST,prefix="ens")
            
            if enseignantForm.is_valid() and create_userForm1.is_valid() :
            
                #print('form en validé')
                user=User.objects._create_user(username=enseignantForm.cleaned_data['prenom']+'_'+enseignantForm.cleaned_data['matricule'], email=create_userForm1.cleaned_data['email'], 
                                              password=create_userForm1.cleaned_data['psswd'], is_staff=False, is_active=False, is_superuser=False,
                                              first_name=enseignantForm.cleaned_data['nom'],last_name=enseignantForm.cleaned_data['prenom'])
            
                
                #Envoyer un mail pour informer l'enseignant qu'il a bien crée un compte mais non actif pour le moment 
                
                
                
                #referencer l'instance de compte de l'enseignant
                enseignantForm.instance.compte=user
                
                #sauvegarde de l'enseignant
                enseignantForm.save()
                info="Sauvegarde effectuée avec succès ! :)"
            else :
                print('form ens invla1')
                print(enseignantForm.errors)
                #print(create_userForm1.errors)
                print('form ens')
            
        elif request.POST['profileType']=='scolarite':
            
            scolariteForm=ScolariteForm(request.POST, request.FILES,prefix="sco")
            create_userForm2=Create_userForm(request.POST,prefix="sco")
            
            if scolariteForm.is_valid() and create_userForm2.is_valid() :
                
                #print('form sco valide')
                
                user=User.objects._create_user(username=scolariteForm.cleaned_data['prenom']+'_'+scolariteForm.cleaned_data['matricule'],email=create_userForm2.cleaned_data['email'],
                                               password=create_userForm2.cleaned_data['psswd'],is_staff=False, is_active=False,is_superuser=False,
                                               first_name=scolariteForm.cleaned_data['nom'],last_name=scolariteForm.cleaned_data['prenom'])
            
                
                #Envoyer un mail pour informer l'enseignant qu'il a bien crée un compte mais non actif pour le moment 
                
                
                
                
                # referencer l'user créé
                scolariteForm.instance.compte=user
                
                #enregistrement de l'enseignant
                
                scolariteForm.save()
                
                info="Sauvegarde effectuée avec succès ! :)"
            else:
                print('form  scolarité invalide1')
                #print(scolariteForm.errors)
                #print(create_userForm2.errors)
                #print('form inal2')
        
        else :
            info="Aucune donnée enregistrée ! :)"
    
                
    scolariteForm=ScolariteForm(prefix="sco")
    create_userForm2=Create_userForm(prefix="sco")
    enseignantForm=EnseignantForm(prefix="ens")
    create_userForm1=Create_userForm(prefix="ens")
    etudiantForm =EtudiantForm(prefix="etd")
    
    return render (request, 'sauvegarde.html', {'etudiantForm':etudiantForm, 'enseignantForm': enseignantForm, 'scolariteForm': scolariteForm, 'create_userForm1':create_userForm1, 'create_userForm2':create_userForm2, 'info':info})
 """
 
def save_etudiant(request):
    """
    """
    info="Veuillez remplir le formulaire"
    if request.POST :
            
        etudiantForm =EtudiantForm(request.POST, request.FILES)
        
        if etudiantForm.is_valid():
            #print('Form valide !')
            m=etudiantForm.cleaned_data['matricule']
            e=etudiantForm.cleaned_data['email']
            
            etudiantForm.save()
            
            
            cursus=Cursus(anneeAca=etudiantForm.cleaned_data['anneeAca'],statut='En cours', niveau=etudiantForm.cleaned_data['niveau'], etudiant=Etudiant.objects.get(matricule=m,email=e), departement=etudiantForm.cleaned_data['departement'])
            cursus.save()
            
            etd=Etudiant.objects.get(matricule=m, email=e)
            
            """
            Envoyer un mail pour informer l'étudiant de son enregisrement sur la plateforme 
            """
            
            mail_envoye = envoyer_Mail_Inscrit(type_mail = 'Enregistrement', regime = 'Etudiant', destinataire=etudiantForm.cleaned_data['email'])
            
            envoi_Mail_Admin(type_mail = 'Enregistrement', inscrit = etd, mail_envoye = mail_envoye, email=etudiantForm.cleaned_data['email'] ,date=timezone_today())
          
            
            info="Sauvegarde effectuée avec succès ! :)"
        else :
            #print (etudiantForm.errors)
            info="Erreur dans le formulaire, vueillez vérifier !"
    
    etudiantForm=EtudiantForm()
    return render(request, 'save_etudiant.html', {'etudiantForm':etudiantForm, 'info': info}) 

def save_enseignant(request):
    """
    """
    info="Veuillez remplir le formulaire"
    if request.POST:
            
        enseignantForm=EnseignantForm(request.POST, request.FILES)
        create_userForm1=Create_userForm(request.POST)
        
        if enseignantForm.is_valid() and create_userForm1.is_valid() :
        
            #print('form en validé')
            user=User.objects._create_user(username=create_userForm1.cleaned_data['username'], email=create_userForm1.cleaned_data['email'], 
                                          password=create_userForm1.cleaned_data['psswd'], is_staff=False, is_active=False, is_superuser=False,
                                          first_name=enseignantForm.cleaned_data['nom'],last_name=enseignantForm.cleaned_data['prenom'])
        
            
            #referencer l'instance de compte de l'enseignant
            enseignantForm.instance.compte=user
            
            #sauvegarde de l'enseignant
            enseignantForm.save()
            
            ens=Enseignant.objects.get(compte__email = user.email)
            """
            Envoyer un mail pour informer l'enseignant qu'il a bien crée un compte mais non actif pour le moment 
            """
            
            mail_envoye = envoyer_Mail_Inscrit(type_mail = 'Enregistrement', regime = 'Enseignant', destinataire=user.email)
            
            envoi_Mail_Admin(type_mail = 'Enregistrement', inscrit = ens, mail_envoye = mail_envoye, email=user.email ,date=timezone_today())
          
            
            info="Sauvegarde effectuée avec succès ! :)"
        else :
            print('form ens invla1')
            print(enseignantForm.errors)
            #print(create_userForm1.errors)
            print('form ens')
    
    enseignantForm=EnseignantForm()
    create_userForm1=Create_userForm()
    
    return render(request, 'save_enseignant.html', {'enseignantForm': enseignantForm,'create_userForm1':create_userForm1, 'info':info})

def save_scolarite(request):
    """
    """
    info="Veuillez remplir le formulaire"
    
    if request.POST:
            
        scolariteForm=ScolariteForm(request.POST, request.FILES)
        create_userForm2=Create_userForm(request.POST)
        
        if scolariteForm.is_valid() and create_userForm2.is_valid() :
            
            #print('form sco valide')
            
            user=User.objects._create_user(username=create_userForm2.cleaned_data['email'],email=create_userForm2.cleaned_data['email'],
                                           password=create_userForm2.cleaned_data['psswd'],is_staff=False, is_active=False,is_superuser=False,
                                           first_name=scolariteForm.cleaned_data['nom'],last_name=scolariteForm.cleaned_data['prenom'])
            
            
            # referencer l'user créé
            scolariteForm.instance.compte=user
            
            #enregistrement de l'enseignant
            
            scolariteForm.save()
            
            agt=AgentScolarite.objects.get(compte__email = user.email)
            
            """
            Envoyer un mail pour informer l'enseignant qu'il a bien crée un compte mais non actif pour le moment 
            """
            mail_envoye = envoyer_Mail_Inscrit(type_mail = 'Enregistrement', regime = 'Scolarite', destinataire=user.email)
            
            envoi_Mail_Admin(type_mail = 'Enregistrement', inscrit = agt, mail_envoye = mail_envoye, email=user.email ,date=timezone_today())
          
            
            
            info="Sauvegarde effectuée avec succès ! :)"
        else:
            print('form  scolarité invalide1')
            print(scolariteForm.errors)
    
    
    scolariteForm=ScolariteForm()
    create_userForm2=Create_userForm()
    
    return render(request, 'save_scolarite.html',{'scolariteForm':scolariteForm, 'create_userForm2':create_userForm2, 'info':info})
    
"""
def consulter(request):
    
    if request.POST:
        if request.POST['type_consultation']=='l_ens_all':
            liste=Enseignant.objects.all()
            Enseign=True
            titre='Liste de tout les enseignants'
            return render (request, 'consulter.html', {'liste':liste, 'Enseignant':Enseign, 'titre':titre})
        elif request.POST['type_consultation']=='l_ens_all_ufr':
            
            return 1
        elif request.POST['type_consultation']=='d_ens':
            return 1
        
        elif request.POST['type_consultation']=='l_etd_all':
            liste=Etudiant.objects.all()
            Enseign=False
            titre='Liste de tous les étudiants'
            return render (request, 'consulter.html', {'liste':liste, 'Enseignant':Enseign, 'titre':titre})
    else :
        titre=''
        liste=False
        Enseign=True
        return render (request, 'consulter.html', {'liste':liste, 'Enseignant':Enseign, 'titre':titre})
"""
"""    
def consult(request):
    
    liste=False
    dep=''
    ufr=''
    rech_specifique1=Rech_specifique()
    rech_specifique2=Rech_specifique()
    rech_speci_ufr=Rech_speci_ufr()
    rech_speci_departement1=Rech_speci_departement()
    rech_speci_departement2=Rech_speci_departement()
    Enseign=False
    if request.POST:
        # s'il s'agit de retrouver un seul enseignant ou etudiant
        affiche=True
        
        
                
        if request.POST['profileType']=='enseignant':
            rech_specifique1=Rech_specifique(request.POST)
            if rech_specifique1.is_valid():
                matr=rech_specifique1.cleaned_data['matricule']
                Enseign=True
                titre='Enseignant'
                liste=Enseignant.objects.filter(matricule=matr)
                
        elif request.POST['profileType']=='etudiant':
            rech_specifique2=Rech_specifique(request.POST)
            if rech_specifique2.is_valid():
                matr=rech_specifique2.cleaned_data['matricule']
                Enseign=False
                titre='Etudiant'
                liste=Etudiant.objects.filter(matricule=matr)
        
        # Lorsque la recherche concerne tous les enseignants
           
        elif request.POST['profileType']=='enseignant_tout':
            liste=Enseignant.objects.all()
            Enseign=True
            titre='Liste de tous les enseignant !'
        
        # lorsque la recherche conserne les enseignants d'un departement
        
        elif request.POST['profileType']=='enseignant_dep':
            rech_speci_departement2=Rech_speci_departement(request.POST)
            if rech_speci_departement2.is_valid():
                dep=rech_speci_departement2.cleaned_data['departement']
                liste=Enseignant.objects.filter(departement=dep)
                Enseign=True
                titre='Liste des enseignant du département '
        
        #lorsque la recherche conserne tout les étudiants
        
        elif request.POST['profileType']=='etudiant_tout':
            liste=Etudiant.objects.all()
            Enseign=False
            titre='Liste de tous les etudiants '
            
        #lorsque la recherche conserne les etudiants d'un departement
            
        elif request.POST['profileType']=='etudiant_dep':
            rech_speci_departement1=Rech_speci_departement(request.POST)
            if rech_speci_departement1.is_valid():
                dep=rech_speci_departement1.cleaned_data['departement']
                liste=Etudiant.objects.filter(departement=dep)
                Enseign=False
                titre='Liste des etudiants du département '
        
        # La recherche conserne les étudiants d'une unité de formation et de recherche
        
        elif request.POST['profileType']=='etudiant_ufr':
            rech_speci_ufr=Rech_speci_ufr(request.POST)
            if rech_speci_ufr.is_valid():
                ufr=rech_speci_ufr.cleaned_data['ufr']
                liste=Etudiant.objects.filter(departement__ufr=ufr)
                Enseign=False
                titre='Liste des etudiants de l\' UFR-'
        
        return render(request, 'consult.html', {'ufr':ufr,'departement':dep,'liste':liste, 'Enseignan':Enseign, 'titre':titre,'affiche':affiche,'rech_specifique1':rech_specifique1, 'rech_speci_ufr':rech_speci_ufr, 'rech_speci_departement1':rech_speci_departement1,'rech_specifique2':rech_specifique2, 'rech_speci_departement2':rech_speci_departement2}) 

    else:
        titre=''
        liste=False
        Enseign=True
        affiche=False
        rech_specifique1=Rech_specifique()
        rech_specifique2=Rech_specifique()
        rech_speci_ufr=Rech_speci_ufr()
        rech_speci_departement1=Rech_speci_departement()
        rech_speci_departement2=Rech_speci_departement()
        return render(request, 'consult.html', {'liste':liste, 'Enseignan':Enseign, 'titre':titre,'affiche':affiche,'rech_specifique1':rech_specifique1, 'rech_speci_ufr':rech_speci_ufr, 'rech_speci_departement1':rech_speci_departement1,'rech_specifique2':rech_specifique2, 'rech_speci_departement2':rech_speci_departement2})
"""

def consult_etudiant(request):
    """
    
    """
    liste=None
    
    titregle='Liste des étudiants'
    
    recherche_form=Recherche_form()
    statut_recherche="etudiant"
    titre_recherche=""
    logged_user=form_user(request)
    if logged_user:
        no='n'
        pr='p'
        ma='m'
        de='d'
        uf='u'
        ni='n'
        if request.POST :
            
            recherche_form=Recherche_form(request.POST)
            if recherche_form.is_valid():
                nom=recherche_form.cleaned_data['nom']
                prenom=recherche_form.cleaned_data['prenom']
                matricule=recherche_form.cleaned_data['matricule']
                departement=recherche_form.cleaned_data['departement']
                ufr=recherche_form.cleaned_data['ufr']
                niveau=recherche_form.cleaned_data['niveau']
                
                print(recherche_form.cleaned_data)
                
                if nom!="" and prenom!="" and matricule!="" and departement!=None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement,departement__ufr=ufr,niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                    
                # si aucun element n'est null
                
                elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, departement=departement,departement__ufr=ufr,niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+", Departement : "+departement+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                
                elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+", Departement : "+departement+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                
                elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Departement : "+departement+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                
                elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule ,departement__ufr=ufr,niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                
                elif nom!="" and prenom!="" and matricule!="" and departement!=None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+", Departement : "+str(departement)+""+" Niveau : "+str(niveau)
                  
                elif nom!="" and prenom!="" and matricule!="" and departement!=None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+", Departement : "+str(departement)+""+" Niveau : "+str(niveau)
                    
                # fin de si un element est null
                
                elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter( matricule=matricule, departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                
                elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter( prenom=prenom, departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                
                elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter( prenom=prenom, matricule=matricule, departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                
                elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, departement=departement, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+", Departement : "+str(departement)+""+" Niveau : "+str(niveau)
                
                elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, departement=departement, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+", Departement : "+str(departement)+""+", Ufr : "+str(ufr)
                
                elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom,  departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                
                elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                
                elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement=departement, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+", Departement : "+str(departement)+""+" Niveau : "+str(niveau)
                
                elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement=departement, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+", Departement : "+str(departement)+""+", Ufr : "+str(ufr)
                
                elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Ufr : "+str(ufr)+""+" Niveau : "+str(niveau)
                
                elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement=departement, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Departement : "+str(departement)+""+" Niveau : "+str(niveau)
                
                elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement=departement, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Departement : "+str(departement)+""+", Ufr : "+str(ufr)
                
                elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+""+", Niveau : "+str(niveau)
                
                elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule,departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+""+", Ufr : "+str(ufr)
                
                elif nom!="" and prenom!="" and matricule!="" and departement!=None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+""+", Departement : "+str(departement)
                    
                # combinaison de deux champs vide
                
                elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(departement=departement,departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Ufr : "+str(ufr)+", Departement : "+str(departement)+", Niveau : "+str(niveau)
                
                elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(matricule=matricule, departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule+", Ufr : "+str(ufr)+""+", Niveau : "+str(niveau)
                
                elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(matricule=matricule, departement=departement, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule+", Departement : "+str(departement)+", Niveau : "+str(niveau)
                    
                elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(matricule=matricule, departement=departement, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule+", Departement : "+str(departement)+", Ufr : "+str(ufr)
                
                elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(prenom=prenom,departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Ufr : "+str(ufr)+""+", Niveau : "+str(niveau)
                
                elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter( prenom=prenom, departement=departement, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Departement : "+str(departement)+""+", Niveau : "+str(niveau)
                    
                elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(prenom=prenom, departement=departement, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Ufr : "+str(ufr)+""+", Departement : "+str(departement)
                
                elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom,departement__ufr=ufr, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Ufr : "+str(ufr)+""+", Niveau : "+str(niveau)
                
                elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, departement=departement,niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Departement : "+str(departement)+""+", Niveau : "+str(niveau)
                
                elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, departement=departement,departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Departement : "+str(departement)+""+", Ufr : "+str(ufr) #ici
                
                elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+""+", Niveau : "+str(niveau)
                    
                elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+""+", Niveau : "+str(niveau)
                
                elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+""+", Ufr : "+str(ufr)
                
                elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, departement=departement, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+""+", Departement : "+str(departement)
                
                elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, matricule=matricule,niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+""+", Niveau : "+str(niveau)
                
                elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+""+", Ufr : "+str(ufr)
                
                elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, matricule=matricule, departement=departement, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+""+", Departement : "+str(departement)
                
                elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+""+", Niveau : "+str(niveau)
                
                elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Ufr"+str(ufr)+", Departement : "+str(departement)
                
                elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, departement=departement, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Departement : "+str(departement)
                
                elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule
                
                # combinaison de trois champs vide
                
                elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr!=None and niveau!=None:
                    liste=Etudiant.objects.filter(departement__ufr=ufr,niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Ufr : "+str(ufr)+""+", Niveau : "+str(niveau)
                
                elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter( departement=departement,niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Departement : "+str(departement)+""+", Niveau : "+str(niveau)
                
                elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(prenom=prenom, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+""+", Niveau : "+str(niveau)
                
                elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(matricule=matricule, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule+""+", Niveau : "+str(niveau)
                
                elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(nom=nom, niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+""+", Niveau : "+str(niveau)
                #ici
                
                elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(departement__ufr=ufr, departement=departement, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Ufr : "+str(ufr)+""+", Departement : "+str(departement)
                
                elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter( departement=departement,prenom=prenom, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+""+" Departement : "+str(departement)+""
                
                elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(prenom=prenom,niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+""+" Niveau : "+str(niveau)+""
                
                elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(prenom=prenom, matricule=matricule, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+""+" Matricule : "+matricule+"" #la
                
                elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(matricule=matricule, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule+""+", Ufr : "+str(ufr)
                
                #ici
                elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(departement__ufr=ufr,niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Ufr : "+str(ufr)+""+", Niveau : "+str(niveau)
                
                elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, departement=departement, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+""+", Departement : "+str(departement)
                
                elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+""+", Ufr : "+str(ufr)
                
                elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, matricule=matricule, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+""+", Matricule : "+matricule
                
                elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, prenom=prenom, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+""+", Prenom : "+prenom
                
                # combinaison de 4 champs vide
                
                elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(nom=nom, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom
                
                elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(prenom=prenom, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom
                
                elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(matricule=matricule, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule
                
                elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(departement=departement, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Departement : "+str(departement)
                
                elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr!=None and niveau==None:
                    liste=Etudiant.objects.filter(departement__ufr=ufr, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Ufr : "+str(ufr)
                
                elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr==None and niveau!=None:
                    liste=Etudiant.objects.filter(niveau=niveau, is_actif=True)
                    titre_recherche="Critère de recherche =>"+" Niveau : "+str(niveau)
                
                # combinaison de 5 champs vide
                    
                # Tous les champs sont vide
                    
                elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr==None and niveau==None:
                    liste=Etudiant.objects.filter(is_actif=True)
                    titre_recherche="Critère de recherche => Aucun "
                    
                
                nb=len(liste)
                titregle='Liste des étudiants'
                
                if nom!="":
                    no=nom
                
                if prenom!="":
                    pr=prenom
                
                if matricule!="":
                    ma=matricule
                    
                if ufr !=None:
                    uf=ufr
                
                if departement != None:
                    de=departement
                
                if niveau !=None:
                    ni=niveau
                
                
        else:
            
            liste=Etudiant.objects.filter(is_actif=True)
            titregle='Liste des étudiants'
            titre_recherche=''
            nb=len(liste)
        
        
        if logged_user.type_user == 'Enseignant':
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'liste':liste, 'statut_recherche':statut_recherche, 'nombre':nb, 'titre_recherche':titre_recherche,
                 'recherche_form':recherche_form,'titregle':titregle,
                 'nom':no, 'prenom':pr, 'matricule':ma, 'ufr':uf, 'departement':de, 'niveau':ni}
        
            
            
        elif logged_user.type_user == 'Scolarite':
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            context={'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'liste':liste, 'statut_recherche':statut_recherche, 'nombre':nb, 'titre_recherche':titre_recherche,
                 'recherche_form':recherche_form,'titregle':titregle,
                 'nom':no, 'prenom':pr, 'matricule':ma, 'ufr':uf, 'departement':de, 'niveau':ni}
        
        
        
        
        
        return render(request,'tous_etudiants.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def consult_enseignant(request):

    """
    """

    liste=None
    
    titregle='Liste des enseignants'
    
    recherche_form=Recherche_form()
    statut_recherche="etudiant"
    titre_recherche=""
    
    logged_user=form_user(request)
    if logged_user:
        no="n"
        pr="p"
        ma="m"
        de="d"
        uf="u"
        if request.POST :
            #print(request.POST)
            recherche_form=Recherche_form(request.POST)
            if recherche_form.is_valid():
                nom=recherche_form.cleaned_data['nom']
                prenom=recherche_form.cleaned_data['prenom']
                matricule=recherche_form.cleaned_data['matricule']
                departement=recherche_form.cleaned_data['departement']
                ufr=recherche_form.cleaned_data['ufr']
                
                #print(recherche_form.cleaned_data)
                
                if nom!="" and prenom!="" and matricule!="" and departement!=None and ufr!=None:
                    liste=Enseignant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement,departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""
                    
                # si aucun element n'est null
                
                elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr!=None:
                    liste=Enseignant.objects.filter(prenom=prenom, matricule=matricule, departement=departement,departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""
                
                elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr!=None:
                    liste=Enseignant.objects.filter(nom=nom, matricule=matricule, departement=departement,departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""
                
                elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr!=None:
                    liste=Enseignant.objects.filter(nom=nom, prenom=prenom, departement=departement,departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""
                
                elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr!=None:
                    liste=Enseignant.objects.filter(nom=nom, prenom=prenom, matricule=matricule ,departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+", Ufr : "+str(ufr)+""
                
                elif nom!="" and prenom!="" and matricule!="" and departement!=None and ufr==None:
                    liste=Enseignant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, departement=departement, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+", Departement : "+str(departement)+""
                    
                # fin de si un element est null
                
                elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr!=None:
                    liste=Enseignant.objects.filter( matricule=matricule, departement=departement,departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""
                
                elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr!=None:
                    liste=Enseignant.objects.filter( prenom=prenom, departement=departement,departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""
                
                elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr!=None:
                    liste=Enseignant.objects.filter( prenom=prenom, matricule=matricule, departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+", Ufr : "+str(ufr)+""
                
                elif nom=="" and prenom!="" and matricule!="" and departement!=None and ufr==None:
                    liste=Enseignant.objects.filter(prenom=prenom, matricule=matricule, departement=departement, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+", Departement : "+str(departement)+""
                
                elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr!=None:
                    liste=Enseignant.objects.filter(nom=nom,  departement=departement,departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Departement : "+str(departement)+", Ufr : "+str(ufr)+""
                
                elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr!=None:
                    liste=Enseignant.objects.filter(nom=nom, matricule=matricule, departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+", Ufr : "+str(ufr)+""
                
                elif nom!="" and prenom=="" and matricule!="" and departement!=None and ufr==None:
                    liste=Enseignant.objects.filter(nom=nom, matricule=matricule, departement=departement, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+", Departement : "+str(departement)+""
                
                elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr!=None:
                    liste=Enseignant.objects.filter(nom=nom, prenom=prenom, departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Ufr : "+str(ufr)+""
                
                elif nom!="" and prenom!="" and matricule=="" and departement!=None and ufr==None:
                    liste=Enseignant.objects.filter(nom=nom, prenom=prenom, departement=departement, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Departement : "+str(departement)+""
                
                elif nom!="" and prenom!="" and matricule!="" and departement==None and ufr==None:
                    liste=Enseignant.objects.filter(nom=nom, prenom=prenom, matricule=matricule, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule+""
                    
                # combinaison de deux champs vide
                
                elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr!=None:
                    liste=Enseignant.objects.filter(departement=departement,departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Ufr : "+str(ufr)+", Departement : "+str(departement)
                
                elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr!=None:
                    liste=Enseignant.objects.filter(matricule=matricule, departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule+", Ufr : "+str(ufr)+""
                
                elif nom=="" and prenom=="" and matricule!="" and departement!=None and ufr==None:
                    liste=Enseignant.objects.filter(matricule=matricule, departement=departement, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule+", Departement : "+str(departement)
                
                elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr!=None:
                    liste=Enseignant.objects.filter(prenom=prenom,departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Ufr : "+str(ufr)+""
                
                elif nom=="" and prenom!="" and matricule=="" and departement!=None and ufr==None:
                    liste=Enseignant.objects.filter( prenom=prenom, departement=departement, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Departement : "+str(departement)+""
                    
                elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr!=None:
                    liste=Enseignant.objects.filter(nom=nom,ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Ufr : "+str(ufr)+""
                
                elif nom!="" and prenom=="" and matricule=="" and departement!=None and ufr==None:
                    liste=Enseignant.objects.filter(nom=nom, departement=departement, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Departement : "+str(departement)+""
                
                elif nom!="" and prenom!="" and matricule=="" and departement==None and ufr==None:
                    liste=Enseignant.objects.filter(nom=nom, prenom=prenom, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Prenom : "+prenom+""
                    
                elif nom=="" and prenom!="" and matricule!="" and departement==None and ufr==None:
                    liste=Enseignant.objects.filter(prenom=prenom, matricule=matricule, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+", Matricule : "+matricule+""
                
                elif nom!="" and prenom=="" and matricule!="" and departement==None and ufr==None:
                    liste=Enseignant.objects.filter(nom=nom, matricule=matricule, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+", Matricule : "+matricule+""
                
                # combinaison de trois champs vide
                
                elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr!=None:
                    liste=Enseignant.objects.filter(departement__ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Ufr : "+str(ufr)+""
                
                elif nom=="" and prenom=="" and matricule=="" and departement!=None and ufr==None:
                    liste=Enseignant.objects.filter( departement=departement, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Departement : "+str(departement)+""
                
                elif nom=="" and prenom!="" and matricule=="" and departement==None and ufr==None:
                    liste=Enseignant.objects.filter(prenom=prenom, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Prenom : "+prenom+""
                
                elif nom=="" and prenom=="" and matricule!="" and departement==None and ufr==None:
                    liste=Enseignant.objects.filter(matricule=matricule, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Matricule : "+matricule+""
                
                elif nom!="" and prenom=="" and matricule=="" and departement==None and ufr==None:
                    liste=Enseignant.objects.filter(nom=nom, compte__is_active=True)
                    titre_recherche="Critère de recherche =>"+" Nom : "+nom+""
                    
                elif nom=="" and prenom=="" and matricule=="" and departement==None and ufr==None:
                    liste=Enseignant.objects.filter(compte__is_active=True)
                    titre_recherche="Critère de recherche => Aucun "
                    
                
                nb=len(liste)
                titregle='Liste des enseignants'
                
                if nom!="":
                    no=nom
                
                if prenom!="":
                    pr=prenom
                
                if matricule!="":
                    ma=matricule
                    
                if ufr !=None:
                    uf=ufr
                
                if departement != None:
                    de=departement
                
        else:
            
            liste=Enseignant.objects.filter(compte__is_active=True)
            titregle='Liste des enseignants'
            titre_recherche=''
            nb=len(liste)
        
        if logged_user.type_user == 'Enseignant':
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'liste':liste, 'statut_recherche':statut_recherche, 'nombre':nb, 'titre_recherche':titre_recherche,
                 'recherche_form':recherche_form,'titregle':titregle,
                 'nom':no, 'prenom':pr, 'matricule':ma, 'ufr':uf, 'departement':de}
        
            
        elif logged_user.type_user == 'Scolarite':
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            context={'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'liste':liste, 'statut_recherche':statut_recherche, 'nombre':nb, 'titre_recherche':titre_recherche,
                 'recherche_form':recherche_form,'titregle':titregle,
                 'nom':no, 'prenom':pr, 'matricule':ma, 'ufr':uf, 'departement':de}
        
        
        
        return render(request,'tous_enseignants.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def consult_scolarite(request):

    """
    """

    liste=None
    
    titregle='Liste des agents de la scolarité'
    
    recherche_form=Recherche_form()
    statut_recherche="scolarite"
    titre_recherche=""
    logged_user=form_user(request)
    if logged_user:
        no="n"
        pr="p"
        ma="m"
        uf="u"
        if request.POST :
            print(request.POST)
            recherche_form=Recherche_form(request.POST)
            if recherche_form.is_valid():
                nom=recherche_form.cleaned_data['nom']
                prenom=recherche_form.cleaned_data['prenom']
                matricule=recherche_form.cleaned_data['matricule']
                ufr=recherche_form.cleaned_data['ufr']
                
                if nom=="" and prenom=="" and matricule=="" and ufr==None:
                    liste=AgentScolarite.objects.filter(compte__is_active=True)
                    titre_recherche="Critère de recherche => Aucun "
                    
                elif nom!="" and prenom=="" and matricule==""  and ufr==None:
                    liste=AgentScolarite.objects.filter(nom=nom, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Nom : "+nom
                
                elif nom=="" and prenom!="" and matricule=="" and ufr==None:
                    liste=AgentScolarite.objects.filter(prenom=prenom, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Prenom : "+prenom
                    
                elif nom=="" and prenom=="" and matricule!="" and ufr==None:
                    liste=AgentScolarite.objects.filter(matricule=matricule, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Matricule : "+matricule
                    
                elif nom=="" and prenom=="" and matricule=="" and ufr!=None:
                    liste=AgentScolarite.objects.filter(ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Ufr : "+str(ufr)
                
                elif nom!="" and prenom!="" and matricule==""  and ufr==None:
                    liste=AgentScolarite.objects.filter(nom=nom,prenom=prenom, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Nom : "+nom+", Prenom : "+prenom
                
                elif nom=="" and prenom!="" and matricule!="" and ufr==None:
                    liste=AgentScolarite.objects.filter(prenom=prenom, matricule=matricule, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Prenom : "+prenom+", Matricule : "+matricule
                    
                elif nom=="" and prenom=="" and matricule!="" and ufr!=None:
                    liste=AgentScolarite.objects.filter(matricule=matricule, ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Matricule : "+matricule+", Ufr : "+str(ufr)
                    
                elif nom=="" and prenom!="" and matricule=="" and ufr!=None:
                    liste=AgentScolarite.objects.filter(prenom=prenom,ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Prenom : "+prenom+", Ufr : "+str(ufr)
                
                elif nom!="" and prenom=="" and matricule=="" and ufr!=None:
                    liste=AgentScolarite.objects.filter(nom=nom,ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Nom : "+nom+", Ufr : "+str(ufr)
                
                elif nom!="" and prenom=="" and matricule!="" and ufr==None:
                    liste=AgentScolarite.objects.filter(nom=nom,matricule=matricule, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Nom : "+nom+", Matricule : "+matricule
                    
                elif nom!="" and prenom!="" and matricule!="" and ufr==None:
                    liste=AgentScolarite.objects.filter(nom=nom,matricule=matricule, prenom=prenom, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Nom : "+nom+", Prenom : "+prenom+", Matricule : "+matricule
                
                elif nom!="" and prenom!="" and matricule=="" and ufr!=None:
                    liste=AgentScolarite.objects.filter(nom=nom,ufr=ufr, prenom=prenom, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Nom : "+nom+", Prenom : "+prenom+", Ufr : "+str(ufr)
                
                elif nom!="" and prenom=="" and matricule!="" and ufr!=None:
                    liste=AgentScolarite.objects.filter(nom=nom,matricule=matricule, ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Nom : "+nom+", Matricule : "+matricule+", Ufr : "+str(ufr)
                
                elif nom=="" and prenom!="" and matricule!="" and ufr!=None:
                    liste=AgentScolarite.objects.filter(prenom=prenom,matricule=matricule, ufr=ufr, compte__is_active=True)
                    titre_recherche="Critère de recherche => "+" Pre;om : "+prenom+", Matricule : "+matricule+", Ufr : "+str(ufr)
                    
                elif nom!="" and prenom!="" and matricule!="" and ufr!=None:
                    liste=AgentScolarite.objects.filter(nom=nom, prenom=prenom,matricule=matricule, ufr=ufr, compte__is_active=True )
                    titre_recherche="Critère de recherche => "+ " Nom : "+nom+ ", Prenom : "+prenom+", Matricule : "+matricule+", Ufr : "+str(ufr)
               
                nb=len(liste)
                
                if nom!="":
                    no=nom
                
                if prenom!="":
                    pr=prenom
                
                if matricule!="":
                    ma=matricule
                    
                if ufr !=None:
                    uf=ufr
                
                
                
        else:
            
            liste=AgentScolarite.objects.filter(compte__is_active=True)
            titregle='Liste des agents de la scolarité'
            titre_recherche=''
            nb=len(liste)
        if logged_user.type_user == 'Enseignant':
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'liste':liste, 'statut_recherche':statut_recherche, 'nombre':nb, 'titre_recherche':titre_recherche,
                 'recherche_form':recherche_form,'titregle':titregle,
                 'nom':no, 'prenom':pr, 'matricule':ma, 'ufr':uf}
        
            
        elif logged_user.type_user == 'Scolarite':
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            context={'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'liste':liste, 'statut_recherche':statut_recherche, 'nombre':nb, 'titre_recherche':titre_recherche,
                 'recherche_form':recherche_form,'titregle':titregle,
                 'nom':no, 'prenom':pr, 'matricule':ma, 'ufr':uf}
        
        
        
        return render(request,'tous_scolarite.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def form_user(request):
    """
    """
    if request.user.is_authenticated:
        if len(Enseignant.objects.filter(compte=request.user))==1:
            return Enseignant.objects.get(compte=request.user)
        elif len(AgentScolarite.objects.filter(compte=request.user))==1:
            return AgentScolarite.objects.get(compte=request.user)
        
        else:
            return None
        
    return None
         
def login_user(request):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        return redirect('/tableau_bord')
        
    form=LoginForm(data=request.POST or None)
    if form.is_valid():
        pseudo=form.cleaned_data['username']
        passwd=form.cleaned_data['password']
        
        user=authenticate(request, username=pseudo, password=passwd)
        
        if user is not None:
            
            if user.is_active:
                
                login(request, user)
                
                if user.is_superuser:
                    return redirect('/admin')
                
                url = request.META['HTTP_REFERER'].split('=')
                if len(url) == 2:
                    return redirect(url[1])
                return redirect('/tableau_bord')
            
        info="Pseudonyme et/ou mot de passe incorrect !"
        return render(request, 'login_form.html', {'form':form, 'info':info})
    
    return render(request, 'login_form.html', {'form':form, 'info':''})
            
def tableau_bord(request):
    """
    
    """
    logged_user=form_user(request)
    nbre_ens_dep=0
    nbre_ens_ufr=0
    if logged_user:
        
        if logged_user.type_user=='Enseignant':
            uf=logged_user.departement.ufr.code
            
            liste_matiere=Matiere.objects.all()
            
            liste_enseignement=Enseign_An_Matiere.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les différentes matières enseignées
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)                                 # pour afficher le nombre de notifications reçu
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user, jury__is_actif=True) #pour l'ensemble des jurys d'appartenance
            
            nbre_mat=len(liste_enseignement)
            nbre_j=len(liste_jury)
            #nbre_depart=len(Departement.objects.all())
            
            if logged_user.titre=='Directeur':
                
                nbre_ens_ufr=len(Enseignant.objects.filter(departement__ufr=logged_user.departement.ufr, compte__is_active=True))
            elif logged_user.titre=='Chef de département':
                nbre_ens_dep=len(Enseignant.objects.filter(departement=logged_user.departement))
                
            #,'nbre_depart':nbre_depart"
            
            context={'nbre_ens_dep':nbre_ens_dep,'nbre_ens_ufr':nbre_ens_ufr,'user':logged_user, 'liste_matiere':liste_matiere, 'liste_enseigne':liste_enseignement, 'nbre_notif':nbre_notif, 'liste_jury':liste_jury, 'liste_notif':liste_notif, 'nbre_matiere':nbre_mat, 'nbre_jury':nbre_j}
            
        elif logged_user.type_user=='Scolarite':
            
            uf=logged_user.ufr.code
            
            nbre_enr=len(Etudiant.objects.filter(is_actif=False, is_ancien =False, departement__ufr=logged_user.ufr))
            nbre_rel=len(Releve.objects.filter(is_actif=True))
            
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True)
            nbre_notif=len(liste_notif)
            
            context={'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif, 'nbre_enr':nbre_enr, 'nbre_rel':nbre_rel}
        
        if uf == 'SEA':
            
            liste_etd=Etudiant.objects.filter(departement__ufr__code = 'SEA', is_actif = True)
            liste_ens=Enseignant.objects.filter(departement__ufr__code = 'SEA', compte__is_active=True)
            
            nbre_ens_inf=0
            nbre_etd_inf=0
            nbre_etd_mat=0
            nbre_etd_phy=0
            nbre_etd_chi=0
            nbre_ens_mat=0
            nbre_ens_phy=0
            nbre_ens_chi=0
            
            for item in liste_etd :
                if item.departement.intitule == 'Informatique':
                    nbre_etd_inf = nbre_etd_inf+1
                elif item.departement.intitule == 'Physique':
                    nbre_etd_phy = nbre_etd_phy+1
                elif item.departement.intitule == 'Mathématiques':
                    nbre_etd_mat = nbre_etd_mat+1
                elif item.departement.intitule == 'Chimie':
                    nbre_etd_chi = nbre_etd_chi+1
                    
            for item in liste_ens :
                if item.departement.intitule == 'Informatique':
                    nbre_ens_inf = nbre_ens_inf+1
                elif item.departement.intitule == 'Physique':
                    nbre_ens_phy = nbre_ens_phy+1
                elif item.departement.intitule == 'Mathématiques':
                    nbre_ens_mat = nbre_ens_mat+1
                elif item.departement.intitule == 'Chimie':
                    nbre_ens_chi = nbre_ens_chi+1
                    
            context['nbre_etd_inf']=nbre_etd_inf
            context['nbre_etd_mat']=nbre_etd_mat
            context['nbre_etd_phy']=nbre_etd_phy
            context['nbre_etd_chi']=nbre_etd_chi
            
            context['nbre_ens_inf']=nbre_ens_inf
            context['nbre_ens_mat']=nbre_ens_mat
            context['nbre_ens_phy']=nbre_ens_phy
            context['nbre_ens_chi']=nbre_ens_chi
                
        
        return render(request, 'acceuil.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def logout_user(request):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        logout(request)
        
    return redirect('/index')    

def ajax_view_all(request):
    """
    """
    if request.is_ajax() and request.GET:
        if ('type' in request.GET) :
            if request.GET['type']=='Enseignant':
                liste=serialize(format='json', queryset=Enseignant.objects.all())
            elif request.GET['type']=='Etudiant':
                liste=serialize(format='json', queryset=Etudiant.objects.all())
            donnee={'liste':liste}
            return JsonResponse(donnee)
    return JsonResponse({'erreur':"Problème de connexion ! "})
     
     

# la vue de la page d'un enseignant

def detail_enseignant(request, idEnseignant):
    
    logged_user=form_user(request)
    if logged_user:
        
    
        # recuperation de l'enseignant dont l' ID est passé à la ffonction
        
        enseignant=Enseignant.objects.get(id=idEnseignant)
        
        liste_matiere_enseigne=Enseign_An_Matiere.objects.filter(enseignant=enseignant, is_actif=True)
        liste_jury_app=Formation_Jury.objects.filter(enseignant=enseignant, jury__is_actif = True)
        
        
        if logged_user.type_user == 'Enseignant':
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            context={'enseignant':enseignant,'user':logged_user, 'liste_matiere_enseigne':liste_matiere_enseigne,
                  'liste_jury':liste_jury,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'liste_jury_app':liste_jury_app}
        
            
            
        elif logged_user.type_user == 'Scolarite':
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            context={'enseignant':enseignant,'user':logged_user, 'liste_matiere_enseigne':liste_matiere_enseigne,
                  'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'liste_jury_app':liste_jury_app}
        
        
        
        return render(request, 'detail_enseignant.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    
def page_enseignant_connecte(request):
    """
    """
    logged_user=form_user(request)
    
    if logged_user:
        # ensuite je recupère la liste des matières dans lesquelles il dispense les cours
        if logged_user.type_user=='Enseignant':
            liste_matiere_enseigne=Enseign_An_Matiere.objects.filter(enseignant=logged_user, is_actif=True)
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            
            
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            context={'liste_jury':liste_jury,'user':logged_user, 'liste_matiere_enseigne':liste_matiere_enseigne, 'liste_jury':liste_jury,'nbre_notif':nbre_notif, 'liste_notif':liste_notif}
            return render(request, 'page_enseignant.html', context)
            
        elif logged_user.type_user=='Scolarite':
            
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            context={'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif}
            
            return render(request, 'page_scolarite.html', context)
            
        #return render(request, 'page_scolarite.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def page_etudiant(request, idEtudiant):
    """
    """
    logged_user=form_user(request)
    
    if logged_user:
    # recuperation de l'enseignant dont l' ID est passé à la ffonction
    
        etudiant=Etudiant.objects.get(id=idEtudiant,is_actif=True)
        # ensuite je recupère la liste des matières dans lesquelles il dispense les cours
        
        liste_cursus=Cursus.objects.filter(etudiant__id=idEtudiant)
        
        if logged_user.type_user == 'Enseignant':
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            nbre_notif=len(liste_notif)
            context={'liste_jury':liste_jury,'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif, 'etudiant':etudiant, 'liste_cursus':liste_cursus}
        
        
        else:
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            context={'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif, 'etudiant':etudiant, 'liste_cursus':liste_cursus}
        
        return render(request, 'page_etudiant.html', context)

    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
     
def ajax_verification(request):
    """
    """
    if request.is_ajax() and request.GET:
        if ('type' in request.GET) and ('valeur' in request.GET):
            message, statut, test = '', '', ''
            if request.GET['type'] == 'email':
                try:
                    test = forms.EmailField().clean(request.GET['valeur'])
                except forms.ValidationError:
                    message, statut = "Ceci n'est pas une adresse électronique.", 'erreur'
                else:
                    users = User.objects.all()
                    students=Etudiant.objects.all()
                    existe = False
                    for user in users:
                        if user.email == test:
                            existe = True
                            break;
                    
                    if existe==False:
                        for etd in students :
                            if etd.email==test:
                                existe=True
                                break;
                            
                    if existe:
                        message, statut = "Cette adresse électronique est déjà utilisé", 'warning'
            
            elif request.GET['type'] == 'pseudo':
                try:
                    test = forms.CharField(min_length=2).clean(request.GET['valeur'])
                except forms.ValidationError:
                    message, statut = "Ceci n'est pas un pseudo !", 'erreur'
                else:
                    if len(test)<4:
                        message, statut = "veuillez entrer un pseudo de 4 caractère au minimum", 'warning'
                    
                    else:
                        users = User.objects.all()
                   
                        existe = False
                        for user in users:
                            if user.username == test:
                                existe = True
                                break;
                        
                        
                        if existe:
                            message, statut = "Ce pseudo est déjà utilisé", 'warning'
                        
            elif request.GET['type'] == 'matricule':
                try:
                    test = forms.CharField(min_length=3).clean(request.GET['valeur'])
                except forms.ValidationError:
                    message, statut = "Ceci ne peux être un numero matricule", 'erreur'
                else:
                    persons=Person.objects.all()
                    existe=False
                    for per in persons:
                        if per.matricule==test:
                            existe=True
                            break;
                    
                    if existe:
                        message, statut = "Ce numéro matricule est déjà utilisé", 'warning'    
                        
            elif request.GET['type']=='password':
                try:
                    test=forms.CharField(min_length=3).clean(request.GET['valeur'])
                except forms.ValidationError:
                        message, statut = "Ceci ne peux être un mot de passe", 'erreur'
                else :
                    if len(test)<8:
                        message, statut = "veuillez entrer un mot de passe de 8 caractère au minimum", 'warning'
                            
            return JsonResponse({'statut': statut, 'message': message})
    return JsonResponse({'erreur': 'Problème de connexion au serveur'}) 

def consulter_Matiere(request): 
    
    logged_user=form_user(request)
    if logged_user:
        rech=Recherche_Matiere(data=request.POST or None)
        liste_matiere=Matiere.objects.all().order_by('code')
        liste_ue=UE.objects.all().order_by('niveau', 'departement')
        if request.POST:
            if rech.is_valid():
                if logged_user.titre == 'Chef de département':
                    niv=rech.cleaned_data['niveau']
                    if niv != None:
                        liste_ue=UE.objects.filter(niveau=niv).order_by('niveau', 'departement')
                        
                elif logged_user.titre == 'Directeur':
                    niv=rech.cleaned_data['niveau']
                    dep=rech.cleaned_data['departement']
                    print(dep)
                    if niv != None and dep!=None:
                        liste_ue=UE.objects.filter(niveau=niv, departement=dep).order_by('niveau', 'departement')
                    elif niv == None and dep!=None:
                        liste_ue=UE.objects.filter(departement=dep).order_by('niveau', 'departement')
                    elif niv!=None and dep == None:
                        liste_ue=UE.objects.filter(niveau=niv).order_by('niveau', 'departement')
        
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'liste_matiere':liste_matiere,'liste_ue':liste_ue,'rech':rech}
        
        return render(request, 'consulter_matiere.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
  
def voir_plus_UE(request, idUE):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        ue=UE.objects.get(id=idUE)
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'ue':ue}
 
        return render(request, 'voir_plus_ue.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def delete_UE(request, idUE):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        ue=UE.objects.get(id=idUE)
        liste_mat=Matiere.objects.filter(uniteEnseign=ue)
        
        for mat in liste_mat:
            mat.is_actif=False
            mat.save()
            
        ue.is_actif=False
        ue.save()
        
        return redirect('/consultation/matiere')
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
 
def delete_EC(request, idM):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        mat=Matiere.objects.get(id=idM)
        mat.is_actif=False
        mat.save()
        
        return redirect('/consultation/matiere')
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
 
def add_EC(request):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        mat=True
        message=''
        matForm=MatiereForm(data=request.POST or None)
        if request.POST:
            
            if matForm.is_valid():
               
                matForm.save()
                
                message='Element constitutifs de l\' '+str(matForm.cleaned_data['uniteEnseign'])+" enregistré avec succès !"
                
                matForm=MatiereForm()
                #return redirect('/consultation/matiere')
        
        liste_niv=Niveau.objects.all()
        liste_dep=Departement.objects.all()
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'form':matForm, 'mat':mat, 'message':message, 'liste_niv':liste_niv,'liste_dep':liste_dep}
        
        return render(request, 'save_matiere.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
 
def add_UE(request):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        mat=False
        ueForm=UEForm(data=request.POST or None)
        message=''
        if request.POST:
            if ueForm.is_valid():
                #print(ueForm.cleaned_data)
                ueForm.save()
                #ue=UE.objects.get(intitule=ueForm.cleaned_data['intitule'])
                
                message="Unité d'enseignement enregistrée avec succès "
                
                ueForm=UEForm()
                #return redirect('/consultation/matiere')
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'form':ueForm, 'mat':mat, 'message':message}
        
        return render(request, 'save_matiere.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
 
def get_EC(request, idEC):
    """
    """
    
    logged_user=form_user(request)
    if logged_user:
        mat=Matiere.objects.get(id=idEC)
        
        ue=UE.objects.get(id=mat.uniteEnseign.id)
        if ue.is_actif:
            mat.is_actif=True
            mat.save()
        
        else :
            ue.is_actif=True
            mat.is_actif=True
            
            ue.save()
            mat.save()
            
        return redirect('/consultation/matiere')
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
 
def get_UE(request, idUE):
    """
    """
    
    logged_user=form_user(request)
    if logged_user:
        
        ue=UE.objects.get(id=idUE)
        
        ue.is_actif=True
        ue.save()
            
        return redirect('/consultation/matiere')
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def modif_UE(request, idUE):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        ue=UE.objects.get(id=idUE)
        ueForm=UEForm( data=request.POST or None, instance=ue)
        
        if request.POST:
            if ueForm.is_valid():
                if ueForm.has_changed():
                    ueForm.save()
                    
                    
            return redirect('/consultation/matiere')
        
            
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'form':ueForm}
        
        return render(request,'modif_ue_ec.html', context )
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
             
def modifier_EC(request, idEC):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        mat=Matiere.objects.get(id=idEC)
        form=MatiereForm(data=request.POST or None, instance=mat)
        
        if request.POST:
            if form.is_valid():
                if form.has_changed():
                    form.save()
                    
            return redirect('/consultation/matiere')
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        context={'liste_jury':liste_jury,'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'form':form}
        
        return render(request,'modif_ue_ec.html', context )
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        
def activation_compte_ens_sco(request, username):
    """
    
    """
    logged_user=form_user(request)
    chef_departement=Group.objects.get(name='chef_departement')
    enseignant_simple=Group.objects.get(name='enseignant_simple')
    directeur=Group.objects.get(name='directeur')
    scolarite=Group.objects.get(name='scolarite')
    regime=''
    pers=''
    if logged_user:
        if logged_user.type_user:
            
            user = User.objects.get(username=username)
            user.is_active=True
            if len(Enseignant.objects.filter(compte=user))== 1:
                regime ='Enseignant'
                pers=Enseignant.objects.get(compte=user)
                user.groups.add(enseignant_simple)
        
                if pers.titre=='Chef de département':
                    user.groups.add(chef_departement)
                elif pers.titre=='Directeur':
                    user.groups.add(directeur)
            
            elif len(AgentScolarite.objects.filter(compte=user))==1:
                pers=AgentScolarite.objects.get(compte=user)
                regime ='Scolarite'
                user.groups.add(scolarite)
        
            user.save()
            
        
        inscrit = User.objects.get(username = username)
        mail_envoye = envoyer_Mail_Inscrit(type_mail = 'Confirmation', regime = regime, destinataire = inscrit)
        envoi_Mail_Admin(type_mail = 'Confirmation', inscrit = pers , mail_envoye = mail_envoye, email=inscrit.email, date=datetime)
        
        print(mail_envoye)
        
        return render(request, 'activation_compte.html', {'activation': True, 'pseudo':username})
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def del_compte_ens_sco(request, username):
    """
    """
    
    logged_user=form_user(request)
    if logged_user:
        user=User.objects.get(username=username)
        user.is_active=False
        user.groups.clear()
        user.save()
        
        # envoyer un mail a la personne
        return redirect('/validation/donnee')
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        
def confirmation_etudiant(request, idEtd):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        etd=Etudiant.objects.get(id=idEtd)
        etd.is_actif=True
        etd.save()
        
        return redirect('/validation/donnee')
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def del_etd(request, idEtd):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        etd=Etudiant.objects.get(id=idEtd)
        etd.is_actif=False
        etd.save()
        
        return redirect('/validation/donnee')
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
            
def validation_enregistrement(request):
    """
    """
    logged_user=form_user(request)
    liste_etudiant=False
    liste_scolarite=False
    liste_enseignant=False
    
    if logged_user:
        form = Recherche_Matiere(data=request.POST or None)
        if request.POST:
            if form.is_valid():
                
                if logged_user.type_user == 'Scolarite':
                    dep=form.cleaned_data['departement']
                    niv=form.cleaned_data['niveau']
                    if niv!= None and dep!=None:
                        liste_etudiant = Etudiant.objects.filter(is_actif=False, is_ancien=False, departement=dep, niveau=niv, departement__ufr= logged_user.ufr).order_by('niveau', 'departement', 'nom', 'prenom')
                        
                    elif niv==None and dep!=None:
                        liste_etudiant = Etudiant.objects.filter(is_actif=False, is_ancien=False, departement=dep, departement__ufr= logged_user.ufr).order_by('niveau', 'departement', 'nom', 'prenom')
                    
                    elif niv!=None and dep==None:
                        liste_etudiant = Etudiant.objects.filter(is_actif=False, is_ancien=False, niveau=niv, departement__ufr= logged_user.ufr).order_by('niveau', 'departement', 'nom', 'prenom')
                    
                    else:
                        liste_etudiant = Etudiant.objects.filter(is_actif=False, is_ancien=False, departement__ufr= logged_user.ufr).order_by('niveau', 'departement', 'nom', 'prenom')
                
                elif logged_user.titre == 'Directeur':
                    dep=form.cleaned_data['departement']
                    if dep!= None:
                        liste_enseignant = Enseignant.objects.filter(compte__is_active=False, is_ancien=False, departement=dep , departartement__ufr= logged_user.ufr).order_by('departement','nom', 'prenom')
                        liste_scolarite = AgentScolarite.objects.filter(compte__is_active=False, is_ancien=False,ufr=logged_user.departement.ufr)
                    
                    elif dep == None:
                        liste_enseignant = Enseignant.objects.filter(compte__is_active=False, is_ancien=False, departement__ufr=logged_user.departement.ufr).order_by('departement','nom', 'prenom')
                        liste_scolarite = AgentScolarite.objects.filter(compte__is_active=False, is_ancien=False,ufr=logged_user.departement.ufr)
        
        else :
            if logged_user.type_user == 'Scolarite':
                liste_etudiant = Etudiant.objects.filter(is_actif=False, is_ancien=False, departement__ufr= logged_user.ufr).order_by('niveau', 'departement', 'nom', 'prenom')
                
            elif logged_user.titre == 'Chef de département':
                liste_enseignant = Enseignant.objects.filter(compte__is_active=False, is_ancien=False, departement=logged_user.departement).order_by('nom', 'prenom')
                
                # enlever la liste des etudiants après dans le cas du chef de department
                #liste_etudiant = Etudiant.objects.filter(is_actif=False).order_by('niveau', 'departement', 'nom', 'prenom')
                
                
            elif logged_user.titre == 'Directeur':     
                liste_enseignant = Enseignant.objects.filter(compte__is_active=False, is_ancien=False, departement__ufr=logged_user.departement.ufr).order_by('departement','nom', 'prenom')
                liste_scolarite = AgentScolarite.objects.filter(compte__is_active=False, is_ancien=False, ufr=logged_user.departement.ufr)
                
        
        if logged_user.type_user == 'Enseignant':
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            context={'liste_jury':liste_jury,'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'form':form, 'liste_etudiant':liste_etudiant, 'liste_scolarite':liste_scolarite, 'liste_enseignant':liste_enseignant}
        
            
        elif logged_user.type_user == 'Scolarite':
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            context={'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'form':form, 'liste_etudiant':liste_etudiant, 'liste_scolarite':liste_scolarite, 'liste_enseignant':liste_enseignant}
        
        
        
        
        #context={'liste_jury':liste_jury,'user':logged_user, 'nbre_notif':nbre_notif, 'liste_notif':liste_notif,'form':form, 'liste_etudiant':liste_etudiant, 'liste_scolarite':liste_scolarite, 'liste_enseignant':liste_enseignant}
        
        return render(request,'validation_enregistrement.html', context )
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def Imprimer_infos_etd(request, idE):
    """
    Cette vue permet d'imprimer les informations d'un étudiant
    """
    logged_user = form_user(request)
    if logged_user:
        
        
        etudiant = Etudiant.objects.get(id=idE)
        
        
        if etudiant.profil:
            lien_profil = etudiant.profil.url
        else:
            lien_profil='/static/img/men.png'
        
        context={'etudiant':etudiant, 'profil':lien_profil}
        return PDFTemplateResponse(request=request, template='infos_perso_etd_pdf.html', context = context,
                                            filename='infos'+str(etudiant.nom)+'_'+str(etudiant.prenom)+'.pdf', 
                                            show_content_in_browser = False, footer_template='footer_template.html')
                    
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def Imprimer_liste_cursus_etd(request, idE):
    """
    Cette vue permet d'imprimer la liste des cursus d'un étudiant
    """
    logged_user = form_user(request)
    if logged_user:
        
        
        etudiant = Etudiant.objects.get(id=idE)
        liste_cursus = Cursus.objects.filter(etudiant=etudiant)
        
        
        if etudiant.profil:
            lien_profil = etudiant.profil.url
        else:
            lien_profil='/static/img/men.png'
        
        context={'etudiant':etudiant, 'profil':lien_profil, 'liste_cursus':liste_cursus}
        return PDFTemplateResponse(request=request, template='liste_cursus_etd_pdf.html', context = context,
                                            filename='Liste_Cursus_'+str(etudiant.nom)+'_'+str(etudiant.prenom)+'.pdf',
                                            show_content_in_browser = False, footer_template='footer_template.html')
                    
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def Imprimer_detail_cursus(request, idC):
    """
    Cette vue permet d'imprimer les deatils sur un cursus universitaire d'un étudiant
    """
    logged_user = form_user(request)
    if logged_user:
        
        cursus = Cursus.objects.get(id=idC)
        etudiant = cursus.etudiant
        
        liste_matiere=Matiere.objects.filter(uniteEnseign__niveau=cursus.niveau, uniteEnseign__departement=cursus.departement, is_actif=True)
        
        # liste des notes de l'étudiant dans les différentes matières
        
        liste_note=Note.objects.filter(course__uniteEnseign__niveau=cursus.niveau, course__uniteEnseign__departement=cursus.departement, anneeAca=cursus.anneeAca, student=etudiant) #, course__is_actif=True
        
        
        context={'etudiant':etudiant, 'liste_note':liste_note, 'liste_matiere':liste_matiere, 'cursus':cursus}
        return PDFTemplateResponse(request=request, template='detail_cursus_pdf.html', context = context,
                                            filename='Detail_Cursus_'+str(etudiant.nom)+'_'+str(etudiant.prenom)+'.pdf',
                                            show_content_in_browser = False, footer_template='footer_template.html')
                    
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def Imprimer_detail_ens(request, idE):
    """
    Cette vue permet d'imprimer le detail sur un enseignant ( matière enseignées, jurys d'apparténance, ...)
    """
    logged_user=form_user(request)
    if logged_user:
        
    
        # recuperation de l'enseignant dont l' ID est passé à la ffonction
        
        enseignant=Enseignant.objects.get(id=idE)
              
        liste_matiere_enseigne=Enseign_An_Matiere.objects.filter(enseignant=enseignant, is_actif=True)
        liste_jury_app=Formation_Jury.objects.filter(enseignant=enseignant, jury__is_actif = True)
        
     
        context={'enseignant':enseignant,'liste_jury':liste_jury_app, 'liste_matiere':liste_matiere_enseigne }
        return PDFTemplateResponse(request=request, template='detail_ens_pdf.html', context = context,
                                            filename='Detail_'+str(enseignant.nom)+'_'+str(enseignant.prenom)+'.pdf', 
                                            show_content_in_browser = False, footer_template='footer_template.html')
                    
        
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
   
def Imprimer_infos_ens(request, idE):
    """
    Cette vue permet d'imprimer les informations personnelles de l'enseignant
    """
    logged_user=form_user(request)
    if logged_user:
        
    
        # recuperation de l'enseignant dont l' ID est passé à la ffonction
        
        enseignant=Enseignant.objects.get(id=idE)
        
        if enseignant.profil:
            lien_profil = enseignant.profil.url
        else:
            lien_profil='/static/img/men.png'
        
        
        context={'enseignant':enseignant, 'profil':lien_profil }
        return PDFTemplateResponse(request=request, template='detail_infos_ens_pdf.html', context = context,
                                            filename='Infos_'+str(enseignant.nom)+'_'+str(enseignant.prenom)+'.pdf', 
                                            show_content_in_browser = False, footer_template='footer_template.html')
                    
        
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
   
def Imprimer_matiere_ens(request, idE):
    """
    Cette vue permet d'mprimer la fiche d'un étudiant avec les matières enseignées depuis le volet retrait de matière
    """
    
    logged_user = form_user(request)
    if logged_user:
        
        enseignant= Enseignant.objects.get(id=idE)
        
        liste_mat= Enseign_An_Matiere.objects.filter(is_actif = True, enseignant=enseignant)
        
        volStat=Vol_Statutaire.objects.get(grade=enseignant.grade)
        
        volume=0
        for item in liste_mat:
            
            volume = volume + item.matiere.volHoraire
            
        context = {'enseignant':enseignant, 'liste_mat_ens':liste_mat, 'volume':volume, 'volume_stat': volStat.volume}
        
        return PDFTemplateResponse(request=request, template='matiere_enseigne_pdf.html', context = context,
                                            filename='Matiere_Ens_'+str(enseignant.nom)+'_'+str(enseignant.prenom)+'.pdf', 
                                            cmd_options={'orientation':'landscape'} ,show_content_in_browser = False, 
                                            footer_template='footer_template.html')
                    
        
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def Imprimer_infos_sco(request, idS):
    """
    Cette vue permet d'imprimer le detail sur un agent
    """
    logged_user=form_user(request)
    if logged_user:
        
    
        # recuperation de l'enseignant dont l' ID est passé à la ffonction
        
        scolarite=AgentScolarite.objects.get(id=idS)
              
        if scolarite.profil:
            lien_profil = scolarite.profil.url
        else:
            lien_profil='/static/img/men.png'
        
     
        context={'scolarite':scolarite, 'profil':lien_profil}
        return PDFTemplateResponse(request=request, template='detail_infos_sco_pdf.html', context = context,
                                            filename='Detail_'+str(scolarite.nom)+'_'+str(scolarite.prenom)+'.pdf',
                                            show_content_in_browser = False, footer_template='footer_template.html')
                    
        
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
     
def detail_scolarite(request, idS):
    
    logged_user=form_user(request)
    if logged_user:
        
    
        # recuperation de l'enseignant dont l' ID est passé à la ffonction
        
        scolarite=AgentScolarite.objects.get(id=idS)
        
        if logged_user.type_user == 'Enseignant':
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            context={'scolarite':scolarite,'user':logged_user,
                  'liste_jury':liste_jury,'nbre_notif':nbre_notif, 'liste_notif':liste_notif}
        
            
        elif logged_user.type_user == 'Scolarite':
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            context={'scolarite':scolarite,'user':logged_user,
                  'nbre_notif':nbre_notif, 'liste_notif':liste_notif}
        
                
        return render(request, 'detail_scolarite.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
 

 



























