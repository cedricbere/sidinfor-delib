"""base URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from base.views import index, save_enseignant,save_etudiant, save_scolarite,page_etudiant#,ajax_view_all,consult, page_enseignant,save
from base.views import ajax_verification,consult_etudiant,etudiant_pdf, enseignant_pdf,\
    consult_enseignant,consult_scolarite,scolarite_pdf#,pdf
#import gestion_enseignement
from gestion_enseignement.views import charger_Enseignant, charger_Matiere,\
    charger_Enseignant_Jury,charger_annee, Filtre_UE
from base.views import annee_Univ, login_user, tableau_bord, logout_user,\
    page_enseignant_connecte, detail_enseignant,consulter_Matiere, voir_plus_UE,\
    delete_EC, delete_UE, add_EC, add_UE, get_UE, get_EC, modif_UE,\
    modifier_EC#,modif_EC
from . import settings#, views
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from base.views import validation_enregistrement, activation_compte_ens_sco,\
    confirmation_etudiant, del_etd, del_compte_ens_sco, Imprimer_infos_etd,\
    Imprimer_liste_cursus_etd, Imprimer_detail_cursus, Imprimer_detail_ens,\
    Imprimer_infos_ens, Imprimer_matiere_ens, Imprimer_infos_sco,\
    detail_scolarite
#from wkhtmltopdf.views import PDFTemplateView

app_name='base'
admin.site.site_header = 'Administration SIDINFOR'
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index),
    path('index/', index,name= 'index'),
    path('tableau_bord/', tableau_bord,name= 'tableau_bord'),
    path('login/', login_user, name='login_user'),
    path('logout/', logout_user, name='logout_user'),
    path('Année_universitaire/', annee_Univ,name= 'annee_Univ'),
    
    path('voir/ue/<idUE>/', voir_plus_UE, name='voir_plus_UE'),
    path('supprimer/ue/<idUE>/',delete_UE, name='delete_UE'),
    path('supprimer/ec/<idM>/',delete_EC, name='delete_EC'),
    path('Ajouter/matière/',add_EC,name='add_EC'),
    path('Ajouter/ue/',add_UE,name='add_UE'),
    path('recuperer/ue/<idUE>/',get_UE,name='get_UE' ),
    path('recuperer/ec/<idEC>/',get_EC,name='get_EC' ),
    path('modifier/ue/<idUE>/',modif_UE, name="modif_UE"),
    path('modifier/ec/<idEC>/',modifier_EC, name="modifier_EC"),
    
    path('validation/donnee/',validation_enregistrement, name="validation_enregistrement"),
    path('confirmation/ensegnant_scolarite/<str:username>',activation_compte_ens_sco,name='activation_compte_ens_sco'),
    path('confirmation/etudiant/<idEtd>/',confirmation_etudiant, name='confirmation_etudiant'),
    path('supprimer/etudiant/<idEtd>/', del_etd, name='del_etd'),
    path('supprimer/enseignant_scolarite/<str:username>/', del_compte_ens_sco,name='del_compte_ens_sco'),
    #path('consult/',consult, name= 'consult'),
    #path('save/',save, name = 'save'),
    #path('pdf/',pdf.as_view(), name = 'pdf'), 
    path('save/etudiant/', save_etudiant, name='save_etudiant'),
    path('save/enseignant/', save_enseignant, name='save_enseignant'),
    path('save/agent_scolarite/', save_scolarite, name='save_scolarite'),
    path('ajax/verification/', ajax_verification, name='ajax_verification'),
    path('consultation/etudiant/', consult_etudiant, name='consult_etudiant'),
    path('consultation/enseignant/', consult_enseignant, name='consult_enseignant'),
    path('consultation/scolarite/', consult_scolarite, name='consult_scolarite'),
    path('consultation/matiere/', consulter_Matiere, name='consulter_Matiere'),
    path('liste/etudiant/pdf/<nom>/<prenom>/<matricule>/<ufr>/<departement>/<niveau>/', etudiant_pdf, name='etudiant_pdf'),
    path('liste/enseignant/pdf/<nom>/<prenom>/<matricule>/<ufr>/<departement>/', enseignant_pdf, name='enseignant_pdf'),
    path('liste/scolarite/pdf/<nom>/<prenom>/<matricule>/<ufr>/', scolarite_pdf, name='scolarite_pdf'),
    #path('ajax/view_all',ajax_view_all, name='ajax_view_all'),
    path('detail/enseignant/<str:idEnseignant>/', detail_enseignant, name='detail_enseignant'),
    path('detail/scolarite/<str:idS>/', detail_scolarite, name='detail_scolarite'),
    path('page/enseignant/', page_enseignant_connecte, name='page_enseignant_connecte'),
    path('page/etudiant/<str:idEtudiant>/', page_etudiant, name='page_etudiant'),
    path('page/etudiant/<str:idEtudiant>/', page_etudiant, name='page_etudiant'),
    #path('deliberation/', include('deliberation.urls')),
    #path('gestion_enseignement/',include('gestion_enseignement.urls'))
    
    path('enseignement/', include('gestion_enseignement.urls')),
    path('deliberation/', include('deliberation.urls')),
    
    path('ajax/filtrer-ue/', Filtre_UE, name='Filtre_UE'),
    path('ajax/charger/matiere/', charger_Matiere, name='charger_matiere'),
    path('ajax/charger/enseignant/', charger_Enseignant, name='charger_enseignants'),
    path('ajax/charger/enseignant/jury/', charger_Enseignant_Jury, name='charger_enseignants_jury'),
    path('ajax/charger/annee/', charger_annee, name='charger_annee'),
    
    ##### Volet impression des fichiers ###
    
    path('imprimer-infos-perso/<idE>/', Imprimer_infos_etd, name="Imprimer_infos_etd"),
    path('imprimer-infos-curus/<idE>/', Imprimer_liste_cursus_etd, name="Imprimer_liste_cursus_etd"),
    path('imprimer-detail-curus/<idC>/', Imprimer_detail_cursus, name="Imprimer_detail_cursus"),
    path('imprimer-enseignant-detail/<idE>/', Imprimer_detail_ens, name="Imprimer_detail_ens"),
    path('imprimer-infos-enseignant/<idE>/', Imprimer_infos_ens, name="Imprimer_infos_ens"),
    path('imprimer-infos-scolarite/<idS>/', Imprimer_infos_sco, name="Imprimer_infos_sco"),
    path('imprimer-matiere-enseigne/<idE>/', Imprimer_matiere_ens, name="Imprimer_matiere_ens"),
    
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
