# -*- coding: utf8 -*-

from django import forms
from base.models import Etudiant, Enseignant,UE,Matiere,Ufr,Departement,AgentScolarite,\
    Niveau, AnneeUniv
#from bootstrap_datepicker_plus import DatePickerInput
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, Layout, Submit, Reset, Hidden
from bootstrap_datepicker_plus import DatePickerInput
from django.contrib.auth.models import User
#from django.db.models.query import QuerySet
#from crispy_forms.templatetags.crispy_forms_field import css_class
#from crispy_forms.templatetags.crispy_forms_field import css_class

#from django.forms import formset_factory
#from django.forms.formsets import formset_factory

#liste=Departement.objects.all()
#list=Ufr.objects.all()

class ScolariteForm(forms.ModelForm):
    """
    """
    class Meta:
        model=AgentScolarite
        exclude=('compte',)
        
        
        widgets ={
                'nom':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nom'}),
                'prenom':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Prénom(s)'}),
                'date_naissance': DatePickerInput(options={'format':'DD/MM/YYYY'}, attrs={'data-toggle':'popover','data-content':'Veuillez entrez votre date de naissance',
                'class':"form-control", 'placeholder':'DD/MM/YYYY'}),
                'lieu_naissance':forms.TextInput(attrs={'class':'form-control','placeholder':'Lieu de naissance'}),
                'matricule':forms.TextInput(attrs={'class':'form-control','placeholder':'Numéro matricule'}),
                'sexe':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le sexe'}),
                #'email':forms.EmailInput(attrs={'class':'form-control','placeholder':'L\'adresse email'}),
                'num_tel':forms.TextInput(attrs={'class':'form-control','placeholder':'Numéro de téléphone'}),
                'profil':forms.ClearableFileInput(attrs={'accept':'image/*'}),
                'ufr':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir l\'UFR'}),
                'titre':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir votre rôle'})
            }

    @property
    def helper(self):
        helper=FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.label_class=''
        helper.form_class='col-12'
        helper.layout=Layout(
            Div(
                Div(Div('nom', css_class="col-6"),Div('prenom',css_class="col-6"),css_class="row"),
                Div(Div('date_naissance', css_class="col-6"),Div('lieu_naissance',css_class="col-6"),css_class="row"),
                Div(Div('matricule',css_class="col-6"),Div('num_tel',css_class="col-6"),css_class="row"),
                Div(Div('sexe',css_class="col-4"),Div('ufr', css_class="col-4"),Div('profil', css_class="col-4"),css_class="row")
                ,css_class='form-group')
        )
        return helper
        
        
    def clean(self):
        
        cleaned_data=super(ScolariteForm, self).clean()
        
        if cleaned_data:
            
            return cleaned_data
        else:
            raise forms.ValidationError("Veuillez remplir correctement le formulaire")
        



class EtudiantForm (forms.ModelForm):
    """ 
    """
    @property
    def helper(self):
        helper=FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.label_class=''
        helper.form_class='col-12'
        helper.layout=Layout(
            Div(
                Div(Div('nom', css_class="col-6"),Div('prenom',css_class="col-6"),css_class="row"),
                Div(Div('date_naissance', css_class="col-6"),Div('lieu_naissance',css_class="col-6"),css_class="row"),
                Div(Div('sexe',css_class="col-3"),Div('statut',css_class="col-3"),Div('departement',css_class="col-3"),Div('niveau',css_class="col-3"),css_class="row"),
                Div(Div('anneeAca',css_class="col-4"),Div('matricule',css_class="col-4"),Div('num_tel', css_class="col-4"),css_class="row"),
                Div(Div('email',css_class="col-8"),Div('profil', css_class="col-4"),css_class="row")
                ,css_class='form-group')
                
        )
        return helper
    
    class Meta:
        model=Etudiant 
        #exclude=('num_tel',)
        fields = '__all__' 

        widgets ={
            'nom':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nom'}),
            'prenom':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Prénom(s)'}),
            'date_naissance': DatePickerInput(options={'format':'DD/MM/YYYY'}, attrs={'data-toggle':'popover','data-content':'Veuillez entrez votre date de naissance',
            'class':"form-control", 'placeholder':'DD/MM/YYYY'}),
            'lieu_naissance':forms.TextInput(attrs={'class':'form-control','placeholder':'Lieu de naissance'}),
            'matricule':forms.TextInput(attrs={'class':'form-control','placeholder':'Numéro matricule'}),
            'sexe':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le sexe'}),
            'email':forms.EmailInput(attrs={'class':'form-control','placeholder':'L\'adresse email'}),
            'num_tel':forms.TextInput(attrs={'class':'form-control','placeholder':'Numéro de téléphone'}),
            'statut':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le statut'}),
            'departement':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le département'}),
            'profil':forms.ClearableFileInput(attrs={'accept':'image/*'}),
            'niveau':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le niveau'}),
            'anneeAca':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez selectionner l\'année académique'})
        }
        # 'class':'form-control','data-toggle':'popover','data-content':'Veuillez entrez votre image de profil
        
        
    def clean(self):
        cleaned_data=super(EtudiantForm, self).clean()
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Veuillez remplir correctement le formulaire")
        


    


class EnseignantForm(forms.ModelForm):
    
    """
    """
    
    class Meta:
        model=Enseignant
        exclude=('compte',)
        
        
        widgets ={
                'nom':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nom'}),
                'prenom':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Prénom(s)'}),
                'date_naissance': DatePickerInput(options={'format':'DD/MM/YYYY'}, attrs={'data-toggle':'popover','data-content':'Veuillez entrez votre date de naissance',
                'class':"form-control", 'placeholder':'DD/MM/YYYY'}),
                'lieu_naissance':forms.TextInput(attrs={'class':'form-control','placeholder':'Lieu de naissance'}),
                'matricule':forms.TextInput(attrs={'class':'form-control','placeholder':'Numéro matricule'}),
                'sexe':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le sexe'}),
                'statut':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le statut'}),
                #'email':forms.EmailInput(attrs={'class':'form-control','placeholder':'L\'adresse email'}),
                'num_tel':forms.TextInput(attrs={'class':'form-control','placeholder':'Numéro de téléphone'}),
                'profil':forms.ClearableFileInput(attrs={'accept':'image/*'}),
                'departement':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le département'}),
                'grade':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le grade'}),
                'titre':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir votre rôle'})
            }

    @property
    def helper(self):
        helper=FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.label_class=''
        helper.form_class='col-12'
        helper.layout=Layout(
            Div(
                Div(Div('nom', css_class="col-6"),Div('prenom',css_class="col-6"),css_class="row"),
                Div(Div('matricule',css_class="col-4"),Div('date_naissance', css_class="col-4"),Div('lieu_naissance',css_class="col-4"),css_class="row"),
                Div(Div('statut',css_class="col-4"),Div('num_tel',css_class="col-4"),Div('sexe', css_class="col-4"),css_class="row"),
                Div(Div('grade',css_class="col-4"),Div('departement', css_class="col-4"),Div('profil',css_class="col-4"),css_class="row")
                ,css_class='form-group')
        )
        return helper
        
        
    def clean(self):
        
        cleaned_data=super(EnseignantForm, self).clean()
        
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Veuillez remplir correctement le formulaire")
        


"""
 L'objectif est de faire de tel sorte que lorsque l'on rensigne une UE, l'on renseigne dans le même temps les matière de cette UE
 
"""
class UEForm (forms.ModelForm):
    
    """
    """
    class Meta :
        model=UE
        exclude=('is_actif',) 
        
        widgets={
            'intitule':forms.TextInput(attrs={
                'class':'form-control', 'placeholder':'Titre-Unité d\'enseignement'
                }),
            'departement':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le département'}),
            'niveau':forms.Select(attrs={'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le niveau'}),
            'coef':forms.TextInput(attrs={
                'class':'form-control', 'placeholder':'Coefficient de la matière'
                }),
            'code':forms.TextInput(attrs={
                'class':'form-control','placeholder':'Le code de l\'unité de formation'
                }),
            'volHoraire':forms.TextInput(attrs={
                'class':'form-control','placeholder':'Volume horaire'
                })
            }
        
    @property
    def helper(self):
        helper=FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.form_class='col-6'
        helper.label_class=''
        helper.layout=Layout(
            Div(
                Div(Div('intitule',css_class="col-8"),Div('code',css_class="col-4"),css_class="row"),
                Div(Div('coef',css_class="col-6"),Div('volHoraire',css_class="col-6"),css_class="row"),
                Div(Div('niveau',css_class="col-6"),Div('departement',css_class="col-6"),css_class="row")
                ,css_class="form-group"
            )
        )
        return helper
        
        
    def clean(self):
        cleaned_data=super(UEForm, self).clean()
        
        if cleaned_data:
            ue=UE.objects.filter(intitule=cleaned_data['intitule'], niveau=cleaned_data['niveau'],departement=cleaned_data['departement'])       
            if len(ue)!=0:
                raise forms.ValidationError("Cette Unité d'enseignement est déjà enrgistrée !")
            else:
                return cleaned_data
            
        else:
            raise forms.ValidationError("Veuillez remplir correctement le formulaire d'unité d'enseignement !")
        
class MatiereForm(forms.ModelForm):
    """
    """
    
    class Meta:
        model=Matiere
        exclude=('is_actif',) 
        
        
        widgets={
            'intitule':forms.TextInput(attrs={
                'class':'form-control', 'placeholder':'Titre-Unité d\'enseignement'
                }),
            'coef':forms.TextInput(attrs={
                'class':'form-control', 'placeholder':'Coefficient de la matière'
                }),
            'code':forms.TextInput(attrs={
                'class':'form-control','placeholder':'Le code de l\'unité de formation'
                }),
            'volHoraire':forms.TextInput(attrs={
                'class':'form-control','placeholder':'Volume horaire'
                }),
            'uniteEnseign':forms.Select(attrs={
                'class':'form-control','placeholder':'unité d\'enseignement'
                })
            }
        
    @property
    def helper(self):
        helper=FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.form_class='col-6'
        helper.label_class=''
        helper.layout=Layout(
            Div(
                Div(Div('intitule',css_class="col-4"),Div('code',css_class="col-4"),Div('uniteEnseign',css_class="col-4"),css_class="row"),
                Div(Div('coef',css_class="col-6"),Div('volHoraire',css_class="col-6"),css_class="row")
                ,css_class="form-group"
            )
        )
        return helper
        
        
    def clean(self):
        
        cleaned_data=super(MatiereForm, self).clean()
        
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Veuillez bien remplir les données des matières de l'UE")
 
 
        
"""
 Faire de tel sorte que lorsqu'on renseigne une Unité de formatione et de Recherche, l'on renseigne aussitôt les départements qui composent celui-ci
"""        

class UfrForm(forms.ModelForm):
    
    """
    """
    
    class Meta:
        model=Ufr
        fields = '__all__' 
        
        widgets={
            'intitule':forms.TextInput(attrs={
                'class':'form-control', 'placeholder':'intitule de l\'ufr'
                }),
            'code':forms.TextInput(attrs={
                'class':'form-control', 'placeholder':'l\'ufr en abrégé'
                })
            }
        
    @property
    def helper(self):
        helper = FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.form_class='col-6'
        helper.label_class=''
        helper.layout=Layout(
            Div(
                Div(Div('intitule', css_class="col-12"),css_class="row"),
                Div(Div('code', css_class="col-12"),css_class="row"),
                css_class="form-group"
            ),
            Div(
                Hidden('profileType','ufr'),
                Submit('submit','Enregistrer'),
                Reset('reset', 'Reinitiaaliser')
                #,css_class="btn btn-group"
                )
            )
        return helper
        
    def clean(self):
        
        cleaned_data=super(UfrForm,self).clean()
        
        if cleaned_data:
            ufr=Ufr.objects.filter(intitule=cleaned_data['intitule'])
            if ufr!=0:
                raise forms.ValidationError("Cette Unité de Formation existe déjà !")
            else:
                return cleaned_data
        else:
            raise forms.ValidationError("Veuillez remplir correctement les données de l'UFR")
        

        
class DepartementForm(forms.ModelForm):
    """
    """
    
    class Meta:
        model=Departement
        exclude=('ufr',)
        
        widgets={
            'intitule':forms.TextInput(attrs={
                'class':'form-control', 'placeholder':'Intitule du departement'
                }),
            'couleur':forms.TextInput(attrs={
                'class':'form-control', 'placeholder':'La couleur du departement'
                })
            }
        
    @property
    def helper(self):
        helper = FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.form_class='col-6'
        helper.label_class=''
        helper.layout=Layout(
            Div(
                Div(Div('intitule', css_class="col-12"),css_class="row"),
                Div(Div('couleur', css_class="col-12"),css_class="row"),
                css_class="form-group"
            ),
            Div(
                #Hidden('formulaire_type','ufr_depart'),
                Submit('submit','Enregistrer'),
                Reset('reset', 'Reinitiaaliser')
                #,css_class="btn btn-group"
                )
            )
        
        return helper
        
    def clean(self):
        
        cleaned_data=super(DepartementForm,self).clean()
        
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Veuillez remplir correctement le formulaire des départements !")
        
class LoginForm(forms.Form):  
    
    username=forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder':'Pseudo', 'class':'form-control'}))
    password=forms.CharField(label="", widget=forms.PasswordInput(attrs={'placeholder':'Mot de passe', 'class':'form-control'}))
    
    def clean (self):
        
        cleaned_data=super(LoginForm, self).clean()
        
        if cleaned_data:
            
            return cleaned_data
            
        else:
            raise forms.ValidationError("Identifiant et/ou mot de passe incorrect")
    
class Create_userForm(forms.Form):
    
    username=forms.CharField(label="Pseudo", widget=forms.TextInput(attrs={
        'placeholder':'Entrez votre identifiant', 'class':'form-control'}))
    email=forms.CharField(label="Adresse Email", widget=forms.EmailInput(attrs={
        'placeholder':'votre adresse email', 'class':'form-control'}))
    psswd=forms.CharField(label="Mot de passe ", widget=forms.PasswordInput(attrs={
        'placeholder':'Entrez votre mot de passe', 'class':'form-control'
        }))
    confirm_pass=forms.CharField(label="Confirmation du Mot de passe ", widget=forms.PasswordInput(attrs={
        'placeholder':'Entrez votre mot de passe', 'class':'form-control'
        }))
    
    @property
    def helper(self):
        helper = FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.form_class='col-6'
        helper.label_class=''
        helper.layout=Layout(
            Div(
                Div(Div('email', css_class="col-3"),Div('username', css_class="col-3"),Div('psswd', css_class="col-3"),Div('confirm_pass', css_class="col-3"),css_class="row"),
                css_class="form-group"
            ))
            
        return helper
    
    def clean (self):
        
        cleaned_data=super(Create_userForm, self).clean()
        
        #username=cleaned_data('username')
        psswd=cleaned_data['psswd']
        c_passwd=cleaned_data['confirm_pass']
        
        #if username and psswd and c_passwd :
        #    res=User.objects.get(username=username)
        if psswd !=c_passwd :
            raise forms.ValidationError("Les deux mots de passe sont différents")
        #elif res!=0:
        #    raise forms.ValidationError("Ce nom d'utilisateur est déjà utilisé")
        else :
            return cleaned_data
            
class Rech_specifique(forms.Form):
    """
    """
    #Entrez le numéro matricule
    matricule=forms.CharField(label="", widget=forms.TextInput(attrs={
        'placeholder':'N° matricule : recherche spécifique', 'class':'form-control'}))
    
    
    @property
    def helper(self):
        helper = FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.form_class='col-6'
        helper.label_class = 'col-6'
        helper.field_class = 'col-6'
        helper.layout=Layout(
            Div(
                Div(Div('matricule', css_class="col-12"),css_class="row"),
                css_class="form-group"
            ))
            
        return helper
    
    def clean(self):
        
        cleaned_data=super(Rech_specifique, self).clean()
        if cleaned_data: 
            return cleaned_data
        else:
            raise forms.ValidationError("Donnée sisie incorrect")
        
        

class Rech_speci_departement(forms.Form):
    
    """
    """
    
    departement=forms.ModelChoiceField(queryset=Departement.objects.all(), label="choisissez le departement", widget=forms.Select(attrs={'class':'form-control'}))
    
    
    @property
    def helper(self):
        helper = FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.form_class='col-6'
        helper.label_class=''
        helper.layout=Layout(
            Div(
                Div(Div('departement', css_class="col-12"),css_class="row"),
                css_class="form-group"
            ))
            
        return helper
    
    def clean (self):
        
        cleaned_data=super(Rech_speci_departement,self).clean()
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Donnée incorrect !")


class Rech_speci_ufr(forms.Form):
    
    """
    """
    ufr=forms.ModelChoiceField(queryset=Ufr.objects.all(), label="Choisissez l'UFR ", widget=forms.Select(attrs={'class':'form-control'}))
    
    @property
    def helper(self):
        helper = FormHelper()
        helper.form_tag=False
        helper.disable_csrf=True
        helper.include_media=False
        helper.form_class='col-6'
        helper.label_class=''
        helper.layout=Layout(
            Div(
                Div(Div('ufr', css_class="col-12"),css_class="row"),
                css_class="form-group"
            ))
            
        return helper
    
    def clean (self):
        
        cleaned_data=super(Rech_speci_ufr,self).clean()
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Donnée incorrect !")
        
class Recherche_form(forms.Form):
    """
    """
    
    prenom=forms.CharField(label="", widget=forms.TextInput(attrs={
        'placeholder':'Entrez le prenom', 'class':'form-control','data-toggle':'popover','data-content':'Veuillez entrer le(s) prénom(s)',}),
        required=False)
    nom=forms.CharField(label="", widget=forms.TextInput(attrs={
        'placeholder':'Entrez le nom de famille', 'class':'form-control','data-toggle':'popover','data-content':'Veuillez entrer le Nom'}),
        required=False)
    matricule=forms.CharField(label="", widget=forms.TextInput(attrs={
        'placeholder':'Entrez le N° matricule', 'class':'form-control','data-toggle':'popover','data-content':'Veuillez entrer le matricule'}),
        required=False)
    ufr=forms.ModelChoiceField(queryset=Ufr.objects.all(), label="", widget=forms.Select(attrs={
        'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir l\'UFR'}),
        required=False)
    departement=forms.ModelChoiceField(queryset=Departement.objects.all(), label="", widget=forms.Select(attrs={
        'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le département'}),
        required=False)
    niveau=forms.ModelChoiceField(queryset=Niveau.objects.all(), label="", widget=forms.Select(attrs={
        'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le niveau'}),
        required=False)
    
    def clean (self):
        """
        """
        cleaned_data=super(Recherche_form,self).clean()
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Vous n'avez pas entré de bonne donnée !")
        
class Recherche_Matiere(forms.Form):
    """
    """
    departement=forms.ModelChoiceField(queryset=Departement.objects.all(), label= '' ,widget=forms.Select(attrs={
        'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le departement'}),
                                       required=False)
    niveau=forms.ModelChoiceField(queryset=Niveau.objects.all(), label='' ,widget=forms.Select(attrs={
        'class':'form-control','data-toggle':'popover','data-content':'Veuillez choisir le niveau'}),
        required=False)
    actif=forms.BooleanField(label='', required=False)
    
    def clean (self):
        """
        """
        cleaned_data=super(Recherche_Matiere,self).clean()
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Vous n'avez pas entré de bonne donnée !")
        
        
        
class AnneeForm(forms.ModelForm):
    """
    """
    
    class Meta:
        model=AnneeUniv
        fields='__all__'
        
        widgets={
            'an':forms.TextInput(attrs={
                'class':'form-control', 'placeholder':'Entrer une nouvelle année'
                })
            }
        
    def clean (self):
        
        """
        """
        cleaned_data=super(AnneeForm,self).clean()
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Vous n'avez pas entré de bonne donnée !")







        
        
        
        
                