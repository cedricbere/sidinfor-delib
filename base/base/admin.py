# -*- coding: utf8 -*-

from django.contrib import admin
#from django.contrib.auth.models import User
from base.models import Matiere, UE, Ufr, Departement, Enseignant,Etudiant,Niveau,\
    AnneeUniv, AgentScolarite, Notifications,AnneeAcad
#from django.contrib.admin.sites import AdminSite

admin.site.register(Matiere)
admin.site.register(UE)
admin.site.register(Departement)
admin.site.register(Ufr)
admin.site.register(Enseignant)
admin.site.register(Etudiant)
admin.site.register(Niveau)
admin.site.register(AnneeUniv)
admin.site.register(AnneeAcad)
admin.site.register(AgentScolarite)
admin.site.register(Notifications)
#admin.site.register(User)