#!/usr/bin/env python
# -*- coding: utf8 -*-
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import mail_admins
from django.core.mail.message import EmailMultiAlternatives


def chemin_de_sauvegarde_image_profile(instance, filename):
    
    """
    """
    #type=instance.person.type_user
    nom=instance.nom
    prenom=instance.prenom
    matricule=instance.matricule
    if instance.type_user == 'Etudiant':
        return 'Etudiant_{0}/{1}'.format(prenom+'_'+nom+'_'+matricule,filename)
    elif instance.type_user == 'Enseignant':
        return 'Eneignant_{0}/{1}'.format(prenom+'_'+nom+'_'+matricule,filename)
    elif instance.type_user=='Scolarite':
        return 'Agent_Scolarite_{0}/{1}'.format(prenom+'_'+nom+'_'+matricule,filename)
    else:
        return '{0}'.format(filename)
        
# fonction d'envoi de mail aux admins et lors des affectation, creation de jury, acctivation de compt

def envoi_Mail_Admin(type_mail = '', inscrit = '', mail_envoye = '', email='',date=''):
    """
    """
    if not inscrit:
        raise ValueError("%s n'est pas de type Personne" % (None))
    
    sujet = 'Nouvelle inscription'
    contenu_html = render_to_string(template_name = 'envoi_mail_enregistrement.html', context = {'type_mail': type_mail, 'regime': 'admin', 'identite': inscrit.prenom+' '+inscrit.nom,
            'email': email, 'date_inscription': date,
            'type': 'Enregistrement', 'statut': mail_envoye})

    contenu_texte = strip_tags(contenu_html)
    
    mail_admins(subject = sujet, message = contenu_texte, html_message = contenu_html)


def envoi_Admin_Aff_Jury(type_mail = '', detail = '', mail_envoye = ''):
    """
    """
    
    sujet = ''
    if type_mail=='Jury':
        sujet="Création de jury"
        
    elif type_mail=='Affectation':
        sujet="Nouvelle affectation de matière"
    
    elif type_mail == 'Retrait':
        sujet="Retrait de matière "

    contenu_html = render_to_string(template_name = 'envoi_mail_aff_jury_admin.html', context = {'type_mail': type_mail, 'detail': detail, 'statut': mail_envoye})

    contenu_texte = strip_tags(contenu_html)
    
    mail_admins(subject = sujet, message = contenu_texte, html_message = contenu_html)




def envoyer_Mail_Inscrit(type_mail = '', regime = '', destinataire=''):
    """
    Permet l'envoi de mail à un utilisateur nouvellement inscrit et aussi lors des affectations de matières et de jury.
    """
    if (regime == None or regime == '') and (destinataire == None or destinataire == ''):
        raise ValueError('Veuillez bien renseigner les valeurs des arguments')
    
    if not destinataire:
        raise ValueError("%s n'est pas de type django.contrib.auth.models.User")

    sujet,destinataire, envoyeur = 'Enregistrement sur SIDINFOR',destinataire, 'parice02@hotmail.com'

    contenu_texte, contenu_html = '', ''

    if regime == 'Etudiant':
        contenu_html = render_to_string(template_name = 'envoi_mail_enregistrement.html', context = {'type_mail': type_mail, 'regime': 'étudiant'})
        contenu_texte = strip_tags(contenu_html)
         
    elif regime == 'Enseignant' or regime == 'Scolarite':
        contenu_html = render_to_string(template_name = 'envoi_mail_enregistrement.html', context = {'type_mail': type_mail, 'regime': 'administration'})
        contenu_texte = contenu_html
    else:
        raise ValueError("Ce régime: '%s' n'est pas pris en compte." % (regime,))
        
    message_electronique = EmailMultiAlternatives(subject = sujet, body = contenu_texte, to = [destinataire], from_email = envoyeur)
    message_electronique.attach_alternative(contenu_html, 'text/html')
    mail_envoye = message_electronique.send()
    if mail_envoye != 0: return True
    else: return False


def envoyer_Mail_Affectation(type_mail='', destinataire=''):
    """
    Permet l'envoi de mail à un utilisateur lors des affectations de matières et de jury.
    """
    if (destinataire == None or destinataire == ''):
        raise ValueError('Veuillez bien reseigner les valeurs des arguments')
    
    if not destinataire:
        raise ValueError("%s n'est pas de type django.contrib.auth.models.User")
    
    if type_mail=='Affectation':
        sujet='Nouvelle affectation de matière'
    elif type_mail == 'Retrait':
        sujet="Retrait de matière "
        
    elif type_mail=="Jury":
        sujet="Nouveau jury d'appartenance"
    
    else:
        raise ValueError("Ce type: '%s' n'est pas pris en compte." % (type_mail,))
    
    destinataire, envoyeur = destinataire, 'parice02@hotmail.com'

    contenu_html = render_to_string(template_name = 'envoi_mail_aff_jury.html', context = {'type_mail': type_mail})
    contenu_texte = strip_tags(contenu_html) 
        
    message_electronique = EmailMultiAlternatives(subject = sujet, body = contenu_texte, to = [destinataire], from_email = envoyeur)
    message_electronique.attach_alternative(contenu_html, 'text/html')
    mail_envoye = message_electronique.send()
    if mail_envoye != 0: return True
    else: return False

