function script(){

	

		$('#id_ufr').change(function(){
			var ufrM=$('#id_ufr').val(),
				depM=$('#id_departement').val(),
				niveau=$('#id_niveau').val();
			chargerMatiere(ufrM, depM, niveau);
		});

		$('#id_departement').change(function(){
			var ufrM=$('#id_ufr').val(),
				depM=$('#id_departement').val(),
				niveau=$('#id_niveau').val();
			chargerMatiere(ufrM, depM, niveau);
		});


		$('#id_niveau').change(function(){
			var ufrM=$('#id_ufr').val(),
				depM=$('#id_departement').val(),
				niveau=$('#id_niveau').val();
			chargerMatiere(ufrM, depM, niveau);
		});

		$('#id_ufr1').change(function(){
			var ufrE=$('#id_ufr1').val(), depE=$('#id_departement1').val();
			chargerEnseignant(ufrE, depE);
		});

		$('#id_departement1').change(function(){
			var ufrE=$('#id_ufr1').val(), depE=$('#id_departement1').val();
			chargerEnseignant(ufrE, depE);
		});

	
}

function chargerMatiere(ufr, departement, niveau){

		$.get({
			url: '/ajax/charger/matiere/',
			data : {'ufr': ufr, 'departement': departement, 'niveau': niveau},
			dataType:'json',

			success:function(code_json, statut){

				if (code_json.erreur) throw code_json.erreur;

				var matiere=JSON.parse(code_json.matiere);
				var listeMatiere=$('#id_matiere');

				listeMatiere.empty();
				listeMatiere.append('<option value="" selected>--------</option>');
				for (var i = 0; i < matiere.length; i++) {
					listeMatiere.append('<option value="'+matiere[i].pk+'">'+matiere[i].fields.intitule+'</option>')
				}
			},
			error: function(resultat, statut, erreur){
				console.log(erreur);
			}
		});
	}

	function chargerEnseignant(ufr, departement){
		$.get({
			url: '/ajax/charger/enseignant/',
			data : {'ufr': ufr, 'departement': departement},
			dataType:'json',

			success:function(code_json, statut){

				if (code_json.erreur) throw code_json.erreur;

				var ens=JSON.parse(code_json.enseignant);
				console.log(ens);
				var liste=$('#id_enseignant');

				liste.empty();
				liste.append('<option value="" selected>--------</option>');
				for (var i = 0; i < ens.length; i++) {
					liste.append('<option value="'+ens[i].pk+'">'+ens[i].fields.grade+' '+ens[i].fields.prenom+' '+ens[i].fields.nom+'</option>')
				}
			},
			error: function(resultat, statut, erreur){
				console.log(erreur);
			}
		});
	}

$(document).ready(script);