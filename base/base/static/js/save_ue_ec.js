function script(){

		$('#departement').change(function(){
			var depM=$('#departement').val(),
				niveau=$('#niveau').val();
			Filtre_UE(depM, niveau);
		});


		$('#niveau').change(function(){
			var depM=$('#departement').val(),
				niveau=$('#niveau').val();
			Filtre_UE(depM, niveau);
		});
}


function Filtre_UE(departement, niveau){

		$.get({
			url: '/ajax/filtrer-ue/',
			data : {'departement': departement, 'niveau': niveau},
			dataType:'json',

			success:function(code_json, statut){

				if (code_json.erreur) throw code_json.erreur;

				var liste=JSON.parse(code_json.liste_ue);
				var listeUE=$('#id_uniteEnseign');

				listeUE.empty();
				listeUE.append('<option value="" selected>--------</option>');
				for (var i = 0; i < liste.length; i++) {
					listeUE.append('<option value="'+liste[i].pk+'">'+liste[i].fields.intitule+' Coef: '+liste[i].fields.coef+'</option>')
				}
			},
			error: function(resultat, statut, erreur){
				console.log(erreur);
			}
		});
	}

$(document).ready(script);