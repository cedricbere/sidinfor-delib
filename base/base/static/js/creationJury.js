function script(){

	$('#id_departement').change(function(){
		var dep=$('#id_departement').val();
		chargerEnseignant_Jury(dep);
	});

/*
	$('#id_nbre_membre').change(function(){
		var nbre=$('#id_nbre_membre').val();
		afficherMembres(nbre);
	}); */
	// charger enseignant dans Jury 

	function chargerEnseignant_Jury(departement){
		$.get({
			url: '/ajax/charger/enseignant/jury/',
			data : {'departement': departement},
			dataType:'json',

			success:function(code_json, statut){

				if (code_json.erreur) throw code_json.erreur;
				
				var ens=JSON.parse(code_json.enseignant);
				var liste1=$('#id_ens1'), liste2=$('#id_ens2'), liste3=$('#id_ens3');

				liste1.empty();
				liste2.empty();
				liste3.empty();
				liste1.append('<option value="" selected>--------</option>');
				liste2.append('<option value="" selected>--------</option>');
				liste3.append('<option value="" selected>--------</option>');
				for (var i = 0; i < ens.length; i++) {
					
					liste1.append('<option value="'+ens[i].pk+'">'+ens[i].fields.grade+' '+ens[i]+' '+ens[i].nom+'</option>')
					liste2.append('<option value="'+ens[i].pk+'">'+ens[i].fields.grade+' '+ens[i].fields.prenom+' '+ens[i].fields.nom+'</option>')
					liste3.append('<option value="'+ens[i].pk+'">'+ens[i].fields.grade+' '+ens[i].fields.prenom+' '+ens[i].fields.nom+'</option>')
				}
			},
			error: function(resultat, statut, erreur){
				console.log(erreur);
			}
		});
	}

	function afficherMembres(nombre){
		//
		//
		//
		if (nombre<3 || nombre>10) {
			var classe='bg-danger', mess='Le nombre de membres est inferieur à 3 : Impossible', message = null;
			message = "<p class='"+classe+"' style='color:red;'>"+mess+"<p>";
				$('#nbmembre').parent().children('p.pseudo').remove();
				$('#nbmembre').before(message)
		} else {
			var mbre=null;
			for (var i = nombre; i>3; i--) {
				$('#ajouter'+i+'').remove();
			}
			for (var i = 4; i <=nombre; i++) {
				mbre="<div class='row' id='ajouter"+i+"' ><fieldset><legend><b>"+i +"ème membre</b></legend><div class='row'><div class='col-lg-8 col-sm-8 col-md-8 col-xs-8'><p> {{formation_membre.ens4|as_crispy_field}} </p></div><div class='col-lg-8 col-sm-8 col-md-8 col-xs-8'><p> {{formation_membre.fonction4|as_crispy_field}} </p></div></div></fieldset></div> ";
				$('#ajouterMembre').after(mbre);
			}
		}
	}
}

$(document).ready(script);