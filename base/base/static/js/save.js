function scriptSave(){

	$('#id_psswd').popover({delay: {show: 400, hide: 200}, trigger: 'hover'}).on({
		focusout: function() {
			verification('password', $(this).val());
		} ,
	});
	
	$('#id_email').popover({delay: {show: 400, hide: 200}, trigger: 'hover'}).on({
		focusout: function() {
			verification('email', $(this).val());
		} ,
	});
	$('#id_matricule').popover({delay: {show: 400, hide: 200}, trigger: 'hover'}).on({
		focusout: function() {
			verification('matricule', $(this).val());
		} ,
	});
	$('#id_username').popover({delay: {show: 400, hide: 200}, trigger: 'hover'}).on({
		focusout: function() {
			verification('pseudo', $(this).val());
		} ,
	});

	function verification(type, valeur){
	// body...
		$.get({
			url: '/ajax/verification/',
			//url: '{% url verification %}',
			data: {'type': type, 'valeur': valeur},
			dataType: 'json',
			success: function(code_json, statut){
				if (code_json.erreur) throw code_json.erreur;

				var classe = null, message = null;
				if (code_json.statut == 'erreur') classe = 'bg-danger';
				else if (code_json.statut == 'warning') classe = 'bg-warning';
				
				if (type == 'email'){
					message = "<p class='"+classe+" email' style='text-align:center;'>"+code_json.message+"<p>";
					$('#div_id_email').parent().children('p.email').remove();
					$('#div_id_email').before(message);
				}
				else if (type == 'password'){
					message = "<p class='"+classe+" password' style='text-align:center;'>"+code_json.message+"<p>";
					$('#div_id_psswd').parent().children('p.password').remove();
					$('#div_id_psswd').before(message)
				}
				else if (type == 'pseudo'){
					message = "<p class='"+classe+" pseudo' style='text-align:center;'>"+code_json.message+"<p>";
					$('#div_id_username').parent().children('p.pseudo').remove();
					$('#div_id_username').before(message)
				}
				else if (type == 'matricule'){
					message = "<p class='"+classe+" matricule' style='text-align:center;'>"+code_json.message+"<p>";
					$('#div_id_matricule').parent().children('p.matricule').remove();
					$('#div_id_matricule').before(message)
				}
			},
			error: function(resultat, statut, erreur){
				console.log(erreur);
			}
		});
	}
}

$(document).ready(scriptSave);