function script(){

		$('#id_button').click(function(e){

			e.preventDefault();

			var depM=$('#departement').val(),
				niveau=$('#niveau').val();
			RemplirTb(depM, niveau);
		});
}


function RemplirTb(departement, niveau){

		$.get({
			url: '/ajax/remplirTableau/',
			data : {'departement': departement, 'niveau': niveau},
			dataType:'json',

			success:function(code_json, statut){

				if (code_json.erreur) throw code_json.erreur;

				var liste_ue=JSON.parse(code_json.liste_ue),
					liste_ec=JSON.parse(code_json.liste_ec),
					liste_ens=JSON.parse(code_json.liste_ens);
				var tbody=$('#id_tbody'),
					thead=$('#id_thead');

				tbody.empty();
				thead.empty();
				thead.append('<p> </p>')
				tbody.append('<option value="" selected>--------</option>');
				for (var i = 0; i < liste_ue.length; i++) {
					var nb=0;
					for (var j = 0; j < liste_ec.length; j++) {
						if (liste_ec[j].fields.uniteEnseign == liste_ue[i]) {
							nb=nb+1;
						} 
					}
					tbody.append('<tr><td rowspan="'+nb+'">'+liste_ue[i].fields.code'</td><td rowspan="'+nb+'">'+liste_ue[i].fields.intitule'</td><td rowspan="'+nb+'">'+liste_ue[i].fields.coef'</td>'+
						+'for (var j = 0; j < liste_ec.length; j++) {'+
							+ 'if ('+liste_ec[j].fields.uniteEnseign == liste_ue[i]+')'+ 
							+'{<td>'+liste_ec[j].fields.intitule+'</td><td>'+liste_ec[j].fields.code+'</td><td>'+liste_ec[j].fields.coef+'</td><td>'+liste_ec[j].fields.coef+ (2/3)*liste_ec[j].fields.coef +'</td><td>'+liste_ec[j].fields.coef+'</td><td>'+(2/3)*liste_ec[j].fields.coef+'</td> }'+
						+'}'+
						+'</tr>');
				
				}
			},
			error: function(resultat, statut, erreur){
				console.log(erreur);
			}
		});
	}

$(document).ready(script);