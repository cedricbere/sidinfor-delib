function script(){

    function displayCorrectlyForm(valeur){
        if ($('#profileType').val()=='etudiant') {
            $('#enseigForm').hide();
            $('#matForm').hide();
            $('#etudForm').show();
        }else if($('#profileType').val()=='matiere'){
            $('#enseigForm').hide();
            $('#etudForm').hide();
            $('#matForm').show();
        }else if($('#profileType').val()=='enseignant'){
            $('#matForm').hide();
            $('#etudForm').hide();
            $('#enseigForm').show();
        }
        else {
            $('#matForm').hide();
            $('#etudForm').hide();
            $('#enseigForm').hide();
        }
    }

    displayCorrectlyForm()

    $('#profileType').change(displayCorrectlyForm);
}

$(document).ready(script);
