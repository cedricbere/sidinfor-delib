function script (){

	function UE_UFR(){
		var profile = $('#profileType').val();

		if (profile =="ue"){
			$('#div_ue_mat').show();
			$('#div_ufr_dep').hide()
		}else if (profile=="ufr") {
			$('#div_ue_mat').hide();
			$('#div_ufr_dep').show()
		}else {
			$('#div_ue_mat').hide();
			$('#div_ufr_dep').hide()
		}
	}

	UE_UFR()

	$('#profileType').change(UE_UFR)
}

$(document).ready(script)