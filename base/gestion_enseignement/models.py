from django.db import models
from base.models import AnneeUniv, Enseignant, Matiere



class Enseign_An_Matiere(models.Model):
    """
    """
    annee_univ=models.ForeignKey(AnneeUniv, verbose_name="Année Universitaire", on_delete=models.DO_NOTHING)
    enseignant=models.ForeignKey(Enseignant , on_delete=models.DO_NOTHING)
    matiere=models.ForeignKey(Matiere ,on_delete=models.DO_NOTHING)
    is_actif=models.BooleanField(default=False)
    
    def __str__(self):
        return self.enseignant.nom+' '+self.enseignant.prenom+' =>'+self.matiere.intitule+' En '+self.annee_univ.an



class Vol_Statutaire(models.Model):
    """
    """
    prof, m_conference, m_assistant, monsieur, assist = 'Professeur Titulaire','Maître de Conférence','Maître Assistant(e)','Monsieur', 'Assistant'
    GRADE=(
        (prof, 'Professeur Titulaire'),
        (m_conference, 'Maître de Conférence'),
        (m_assistant,'Maître Assistant(e)'),
        (monsieur, 'Monsieur'),
        (assist, 'Assistant')
        )
    grade=models.CharField(choices=GRADE, max_length=20, default='Monsieur', unique=True)
    volume = models.IntegerField()
    
    def __str__(self):
        """
        """
        return self.grade+" correspond à un volume statutaire de "+str(self.volume)+" Heures "