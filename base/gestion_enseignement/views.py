#!/usr/bin/env python
# -*- coding: utf8 -*-

from django.shortcuts import render, redirect
#from base.models import Enseignant
from gestion_enseignement.forms import AffectMatiereForm, Filtre_Enseignant,\
    Filtre_Matiere
from base.models import Matiere, Enseignant, AnneeUniv, Departement,\
    Notifications, AnneeAcad, UE
from django.core.serializers import serialize
from django.http.response import JsonResponse
from gestion_enseignement.models import Enseign_An_Matiere#, NouvelleEnseignants
from django.views.generic.dates import timezone_today
from base.other import envoyer_Mail_Affectation, envoi_Admin_Aff_Jury
from base.views import form_user#, page_enseignant_connecte
from django.conf import settings
from deliberation.models import Formation_Jury
from base.forms import EnseignantForm, Create_userForm, ScolariteForm,\
    Recherche_Matiere
from gestion_enseignement.models import Vol_Statutaire
# Create your views here.

def Affectation(request):
    """
    """
    message=''
    filtre_ens=Filtre_Enseignant()
    filtre_mat=Filtre_Matiere()
    affectation_form=AffectMatiereForm()
    logged_user=form_user(request)
    if logged_user:
        if request.POST and ('matiere' in request.POST) and ('enseignant' in request.POST):
            affectation_form=AffectMatiereForm(request.POST)
            if affectation_form.is_valid():
                
                an=AnneeUniv.objects.last()
                enseignant=affectation_form.cleaned_data['enseignant']
                matiere=affectation_form.cleaned_data['matiere']
               
                if len(Enseign_An_Matiere.objects.filter(is_actif=True, matiere=matiere)) == 0:
                    
                    #liste_enseigne= Enseign_An_Matiere.objects.filter(enseignant=enseignant, is_actif = True)
                    #vol_statutaire=Vol_Statutaire.objects.get(grade=enseignant.grade)
                    
                    #volume=vol_statutaire.volume
                    
                    #vol_stat=0
                    #for item in liste_enseigne:
                    #    vol_stat=vol_stat+ item.matiere.volHoraire
                    
                    #if vol_stat >= volume: 
                    #    message='Affectation échouée; Cet enseignant a atteint son volume horaire statutaire ! '
                    
                    #elif vol_stat + matiere.volHoraire > volume :
                        
                    #    message='Affectation échouée; Cet enseignant dépassera son volume horaire statutaire ! '
                    
                    #else :
                    ###### creation d'un Enseignant qui dispense cour dans une matière ######
                    matiere.is_affecte=True
                    matiere.save()
                    
                    affect= Enseign_An_Matiere(enseignant=enseignant, matiere=matiere, annee_univ=an, is_actif=True)
                    
                    affect.save()
                    
                    # Message dans la Table des notifications
                    
                    new=Notifications(enseignant=enseignant, information="Une matière vous a été affectée.",url="http://localhost:8000/enseignement/affiche_matiere/<matiere.id>/", date=timezone_today())
                    
                    new.save()
                    
                    # envois de mail
                    
                    #envoyer_Mail_Affectation(type_mail="Affectation", destinataire=dest)
                    mail_envoye=envoyer_Mail_Affectation(type_mail="Affectation", destinataire=enseignant.compte.email)
                    
                    #print(mail_envoye)
                    detail="L'affectation de la matière "+matiere.intitule+" à l'enseignant "+enseignant.prenom+" "+enseignant.nom+" pour l'année universitaire : "+str(an)+" à été effectuée avec succès"
                    envoi_Admin_Aff_Jury(type_mail = 'Jury', detail =detail, mail_envoye = mail_envoye)
                    
                    message='Affectation effectuée avec succès'
                        
                    
                else :
                    message='Affectation échouée; un autre enseignant est chargé de dispenser ce cours ! '
                
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
                
        context={'user':logged_user,'filtre_ens':filtre_ens, 'filtre_mat':filtre_mat ,'affectation_form':affectation_form, 'message':message,'liste_jury':liste_jury,'nbre_notif':nbre_notif, 'liste_notif':liste_notif}
                
        return render(request, 'affectationMatiere.html', context)
        
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
            
def charger_Matiere(request):
    """
    """
    if request.is_ajax() and request.GET:
        
        if ('ufr' in request.GET) and ('departement' in request.GET) and ('niveau' in request.GET):
            ufr=request.GET['ufr']
            dep=request.GET['departement']
            niv=request.GET['niveau']
            
            
            if ufr=="" and dep=="" and niv=="" :
                matiere=serialize (format = 'json', queryset= Matiere.objects.filter(is_actif=True, is_affecte = False))
            elif ufr!="" and dep=="" and niv=="":
                matiere=serialize(format='json', queryset=Matiere.objects.filter(uniteEnseign__departement__ufr=ufr,is_actif=True, is_affecte = False))
            elif ufr=="" and dep!="" and niv=="":
                matiere=serialize(format='json', queryset=Matiere.objects.filter(uniteEnseign__departement=dep,is_actif=True, is_affecte = False))
            elif ufr=="" and dep=="" and niv!="":
                matiere=serialize(format='json', queryset=Matiere.objects.filter(uniteEnseign__niveau=niv,is_actif=True, is_affecte = False))
            
            elif ufr!="" and dep!="" and niv=="":
                matiere=serialize(format='json', queryset=Matiere.objects.filter(uniteEnseign__departement__ufr=ufr,uniteEnseign__departement=dep,is_actif=True, is_affecte = False))
            elif ufr=="" and dep!="" and niv!="":
                matiere=serialize(format='json', queryset=Matiere.objects.filter(uniteEnseign__departement=dep,uniteEnseign__niveau=niv,is_actif=True, is_affecte = False))
            elif ufr!="" and dep=="" and niv!="":
                matiere=serialize(format='json', queryset=Matiere.objects.filter(uniteEnseign__departement__ufr=ufr,uniteEnseign__niveau=niv,is_actif=True, is_affecte = False))
            
            elif ufr!="" and dep!="" and niv!="":
                matiere=serialize(format='json', queryset=Matiere.objects.filter(uniteEnseign__departement__ufr=ufr,uniteEnseign__departement=dep,uniteEnseign__niveau=niv,is_actif=True, is_affecte = False))
                
            donnee={'matiere':matiere}
            
            return JsonResponse(donnee)
        
    return JsonResponse({'erreur': 'Problème de connexion au serveur'})

def Filtre_UE(request):
    """
    """
    if request.is_ajax() and request.GET:
        
        if ('departement' in request.GET) and ('niveau' in request.GET):
            
            dep=request.GET['departement']
            niv=request.GET['niveau']
            
            
            if dep=="" and niv=="" :
                ue=serialize (format = 'json', queryset= UE.objects.filter())
            
            elif dep!="" and niv=="":
                ue=serialize(format='json', queryset=UE.objects.filter(departement=dep))
            elif dep=="" and niv!="":
                ue=serialize(format='json', queryset=UE.objects.filter(niveau=niv))
            
            
            elif dep!="" and niv!="":
                ue=serialize(format='json', queryset=UE.objects.filter(departement=dep,niveau=niv))
                
            donnee={'liste_ue':ue}
            
            return JsonResponse(donnee)
        
    return JsonResponse({'erreur': 'Problème de connexion au serveur'})

def charger_Enseignant(request):
    """
    """
    if request.is_ajax() and request.GET:
        
        if ('ufr' in request.GET) and ('departement' in request.GET):
            ufr=request.GET['ufr']
            dep=request.GET['departement']
            
            #enseignant=serialize(format='json', queryset=Enseignant.objects.filter(departement=dep))
            
            
            
            if ufr=="" and dep=="":
                enseignant=serialize(format='json', queryset=Enseignant.objects.filter(compte__is_active=True))
                
            elif ufr!="" and dep=="":
                enseignant=serialize(format='json', queryset=Enseignant.objects.filter(departement__ufr=ufr, compte__is_active=True))
            
            elif ufr=="" and dep!="":
                enseignant=serialize(format='json', queryset=Enseignant.objects.filter(departement=dep, compte__is_active=True))
            
            elif ufr!="" and dep!="":
                enseignant=serialize(format='json', queryset=Enseignant.objects.filter(departement__ufr=ufr, departement=dep, compte__is_active=True))
            
            donnee={'enseignant': enseignant}
            
            return JsonResponse(donnee)
    
    return JsonResponse({'erreur': 'Probleme de connexion au serveur'})
            
def charger_Enseignant_Jury(request):
    """
    """
    if request.is_ajax() and request.GET:
        
        if 'departement' in request.GET:
            
            dep=request.GET['departement']
            
            #enseignant=serialize(format='json', queryset=Enseignant.objects.filter(departement=dep))
            
            #print(str(dep.ufr))
            
            
            if dep=="":
                enseignant=serialize(format='json', queryset=Enseignant.objects.filter( compte__is_active=True))
                
            elif dep!="":
                
                depart=Departement.objects.get(id=dep)
                
                ufr=depart.ufr
                
                # recuperation des enseignants appartenant à l'ufr
                enseignant=serialize(format='json', queryset=Enseignant.objects.filter(departement__ufr=ufr, compte__is_active=True))
                
                #enseignant=serialize(format='json', queryset=Enseignant.objects.all())
            
            donnee={'enseignant': enseignant}
            
            return JsonResponse(donnee)
    
    return JsonResponse({'erreur': 'Probleme de connexion au serveur'})
                        
def charger_annee(request):
    """
    """
    if request.is_ajax():
        
        annee=serialize(format='json', queryset=AnneeAcad.objects.filter(id=1))

        donnee={'annee': annee}
        print(annee)
        return JsonResponse(donnee)
    
    return JsonResponse({'erreur': 'Probleme de connexion au serveur 54545'})
        
def modifierProfil(request):
    """
    """
    logged_user=form_user(request)
    if logged_user:
        profil=''
        if logged_user.type_user == 'Enseignant':
            compte=logged_user.compte
            print(compte)
            profil=EnseignantForm(data = request.POST or None, instance = logged_user)
            create=Create_userForm(data = request.POST or None)
        elif logged_user.type_user == 'Scolarite':
            profil=ScolariteForm(data = request.POST or None, instance = logged_user)
            create=Create_userForm(data = request.POST or None, instance = logged_user)
            
        if request.POST:
            if profil.is_valid() and create.is_valid():
                if create.has_changed():
                    create.save()
                
                if profil.has_changed():
                    profil.save()
                    
            return redirect('/page_enseignant_connecte')
        return render(request, 'modifier_profil.html', {'profil':profil, 'create':create})
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def Matiere_dispense(request):
    """
    Cette vue permet de voir la liste des matières enseignées et les enseignants qui les dispenses
    """
        
    logged_user=form_user(request)
    
    if logged_user:
        statutaire = Vol_Statutaire.objects.all()
        if logged_user.titre == 'Directeur':
            liste_enseignant = Enseignant.objects.filter(departement__ufr=logged_user.departement.ufr, compte__is_active=True).order_by('nom', 'prenom','departement')
            liste_mat_ens = Enseign_An_Matiere.objects.filter(is_actif=True, enseignant__departement__ufr = logged_user.departement.ufr)
            
        elif logged_user.titre == 'Chef de département' :
            liste_enseignant = Enseignant.objects.filter(departement=logged_user.departement, compte__is_active=True).order_by('nom', 'prenom')
            liste_mat_ens = Enseign_An_Matiere.objects.filter(is_actif=True, enseignant__departement= logged_user.departement)
            
        #### Pour calculer le nombre d'heure dispensé par chaque enseignant ####
        dict_vol_stat={}
        
        for ens in liste_enseignant:
            
            vol_dispense=0 
            
            for elt in liste_mat_ens:
                if elt.enseignant == ens :
                    vol_dispense= vol_dispense + elt.matiere.volHoraire
            
            dict_vol_stat[ens]=vol_dispense
            
        
        
        liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
        nbre_notif=len(liste_notif)
        
        liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
        
        context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif,
                 'liste_mat_ens':liste_mat_ens,'liste_enseignant':liste_enseignant, 'dict':dict_vol_stat,'statutaire':statutaire}
        
        return render(request, 'retirer_matiere.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def Retirer_matiere(request, ide):
    """
    Cette vue permet de retirer l'enseignement d'une matière à un enseignant
    """
    
    logged_user=form_user(request)
    
    if logged_user:
        
        retrait= Enseign_An_Matiere.objects.get(id=ide)
        retrait.is_actif=False
        
        
        email=retrait.enseignant.compte.email
        matiere=retrait.matiere
        
        matiere.is_affecte =False
        
        matiere.save()
        
        enseignant=retrait.enseignant
        retrait.save()
        # envois de mail
        
        #envoyer_Mail_Affectation(type_mail="Affectation", destinataire=dest)
        mail_envoye=envoyer_Mail_Affectation(type_mail="Retrait", destinataire=email)
        
        #print(mail_envoye)
        detail="Le retrait de la matière "+matiere.intitule+" à l'enseignant "+enseignant.prenom+" "+enseignant.nom+" à été effectuée avec succès"
        envoi_Admin_Aff_Jury(type_mail = 'Jury', detail =detail, mail_envoye = mail_envoye)
        
    
        return redirect('/enseignement/liste-matiere-dispense')
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

def RemplirTb(request):
    """
    Cette requête AJAX permet de remplir le tableau des matières d'une classe
    """
    if request.is_ajax() and request.GET:
        
        if ('departement' in request.GET) and ('niveau' in request.GET):
            
            dep=request.GET['departement']
            niv=request.GET['niveau']
            
            
            if dep!="" and niv!="" :
                liste_ue=serialize(format='json', queryset=UE.objects.filter(departement=dep,niveau=niv))
                liste_ec=serialize(format='json', queryset=Matiere.objects.filter(uniteEnseign__departement=dep,uniteEnseign__niveau=niv))
                liste_ens=serialize(format='json', queryset=Enseign_An_Matiere.objects.filter(is_actif=True))
            
            
            donnee={'liste_ue':liste_ue, 'liste_ec':liste_ec, 'liste_ens':liste_ens}
            
            return JsonResponse(donnee)
        
    return JsonResponse({'erreur': 'Problème de connexion au serveur'})

def Afficher_matiere_par_Classe(request):
    """
    """
    logged_user=form_user(request)
    
    if logged_user:
        dict_nbre_ec={}
        liste_ue=list()
        liste_ec=list()
        liste_ens=list()
        rech=Recherche_Matiere(data=request.POST or None)
        titre =''
        
        if request.POST :
            if rech.is_valid():
                dep = rech.cleaned_data['departement']
                niv= rech.cleaned_data['niveau']
                titre=str(niv)+'-Parcours '+str(dep)
                liste_ens=Enseign_An_Matiere.objects.filter(is_actif=True)
                
                if niv != None and dep!=None:
                    liste_ue=UE.objects.filter(niveau=niv, departement=dep).order_by('niveau', 'departement')
                    liste_ec=Matiere.objects.filter(uniteEnseign__niveau=niv, uniteEnseign__departement=dep)
                    
                elif niv == None and dep!=None:
                    
                    liste_ue=UE.objects.filter(departement=dep).order_by('niveau', 'departement')
                    liste_ec=Matiere.objects.filter(uniteEnseign__departement=dep)
                elif niv!=None and dep == None:
                    liste_ue=UE.objects.filter(niveau=niv).order_by('niveau', 'departement')
                    liste_ec=Matiere.objects.filter(uniteEnseign__niveau=niv)
                    
                
                
                for ue in liste_ue :
                    nb=0
                    for ec in liste_ec:
                        if ec.uniteEnseign == ue :
                            nb=nb+1
                
                    
                    dict_nbre_ec[ue]=nb
                
        
        
        if logged_user.type_user == 'Enseignant':
            
            liste_jury=Formation_Jury.objects.filter(enseignant=logged_user)
            
            liste_notif=Notifications.objects.filter(enseignant=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            context={'liste_jury':liste_jury,'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif, 'rech':rech,
                     'dict_nbre_ec':dict_nbre_ec, 'liste_ue':liste_ue, 'liste_ec':liste_ec, 'liste_ens':liste_ens, 'titre':titre}
        
        elif logged_user.type_user == 'Scolarite':
            liste_notif=Notifications.objects.filter(scolarite=logged_user, is_actif=True) # pour afficher les notifications sous la cloche
            nbre_notif=len(liste_notif)
            
            context={'user':logged_user,'nbre_notif':nbre_notif, 'liste_notif':liste_notif, 'rech':rech, 'titre':titre,
                     'dict_nbre_ec' : dict_nbre_ec, 'liste_ue':liste_ue, 'liste_ec':liste_ec, 'liste_ens':liste_ens}
        
        return render(request, 'afficher_matière_par_classe.html', context)
    
    return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        
        
            
            
            
            
            
            
            
            
            