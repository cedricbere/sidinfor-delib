from django.urls import path#,include
from gestion_enseignement.views import Affectation#, charger_Matiere,\
from gestion_enseignement.views import modifierProfil, Matiere_dispense,\
    Retirer_matiere, Afficher_matiere_par_Classe
# charger_Enseignant

urlpatterns=[
    path('affectation/', Affectation, name='affectation'),
    path('modifier/profil/',modifierProfil, name="modifierProfil"),
    path('liste-matiere-dispense/', Matiere_dispense, name='Matiere_dispense'),
    path('retirer-matiere/<ide>/',Retirer_matiere, name="Retirer_matiere"),
    path('Afficher-matiere/', Afficher_matiere_par_Classe, name="Afficher_matiere_par_Classe"),
    #path('ajax/charger/enseignants/', charger_Enseignant, name='charger_enseignants'),
    ]