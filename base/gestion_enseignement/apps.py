from django.apps import AppConfig


class GestionEnseignementConfig(AppConfig):
    name = 'gestion_enseignement'
