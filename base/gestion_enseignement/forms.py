# -*- coding: utf8 -*-

from django import forms
from base.models import Ufr, Departement, Niveau, Matiere, Enseignant

class AffectMatiereForm(forms.Form):
    """
    """
    
    matiere=forms.ModelChoiceField(queryset=Matiere.objects.filter(is_actif=True, is_affecte=False), label="Matière",  widget=forms.Select(attrs={'class':'form-control'}))
    enseignant=forms.ModelChoiceField(queryset=Enseignant.objects.filter(compte__is_active=True, is_ancien=False), label="Enseignant",  widget=forms.Select(attrs={'class':'form-control'}))
    
    
    def clean (self):
        """
        """
        cleaned_data=super(AffectMatiereForm,self).clean()
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Vous n'avez pas entré de bonne donnée !")
        
        
class Filtre_Enseignant(forms.Form):
    """
    """
    ufr1=forms.ModelChoiceField(queryset=Ufr.objects.all(), label="UFR ", widget=forms.Select(attrs={'class':'form-control'}),required=False)
    departement1=forms.ModelChoiceField(queryset=Departement.objects.all(), label="Departement", widget=forms.Select(attrs={'class':'form-control'}),required=False)
    
    def clean(self):
        """
        """
        cleaned_data=super(Filtre_Enseignant,self).clean()
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Vous n'avez pas entré de bonne donnée !")
        
class Filtre_Matiere(forms.Form):
    """
    """
    ufr=forms.ModelChoiceField(queryset=Ufr.objects.all(), label="UFR ", widget=forms.Select(attrs={'class':'form-control'}),required=False)
    departement=forms.ModelChoiceField(queryset=Departement.objects.all(), label="Departement", widget=forms.Select(attrs={'class':'form-control'}),required=False)
    niveau=forms.ModelChoiceField(queryset=Niveau.objects.all(), label="Niveau", widget=forms.Select(attrs={'class':'form-control'}),required=False)
   
    def clean(self):
        """
        """
        cleaned_data=super(Filtre_Matiere,self).clean()
        if cleaned_data:
            return cleaned_data
        else:
            raise forms.ValidationError("Vous n'avez pas entré de bonne donnée !")
        
        