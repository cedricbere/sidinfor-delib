from django.contrib import admin
from gestion_enseignement.models import Enseign_An_Matiere, Vol_Statutaire
# Register your models here.

admin.site.register(Enseign_An_Matiere)
admin.site.register(Vol_Statutaire)