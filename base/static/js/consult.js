function script_consult(){

    function displayCorrectlyForm(valeur){
        if ($('#profileType').val()=='etudiant') {
            $('#ens_matricule').hide();
            $('#etd_matricule').show();
            $('#ens_tout').hide();
            $('#ens_dep').hide();
            $('#etd_tout').hide();
            $('#etd_dep').hide();
            $('#etd_ufr').hide();


        }else if($('#profileType').val()=='enseignant'){
            $('#ens_matricule').show();
            $('#etd_matricule').hide();
            $('#ens_tout').hide();
            $('#ens_dep').hide();
            $('#etd_tout').hide();
            $('#etd_dep').hide();
            $('#etd_ufr').hide();

        }else if($('#profileType').val()=='enseignant_tout'){
            $('#ens_matricule').hide();
            $('#etd_matricule').hide();
            $('#ens_tout').show();
            $('#ens_dep').hide();
            $('#etd_tout').hide();
            $('#etd_dep').hide();
            $('#etd_ufr').hide();

        }else if($('#profileType').val()=='enseignant_dep'){
            $('#ens_matricule').hide();
            $('#etd_matricule').hide();
            $('#ens_tout').hide();
            $('#ens_dep').show();
            $('#etd_tout').hide();
            $('#etd_dep').hide();
            $('#etd_ufr').hide();
            
        }else if($('#profileType').val()=='etudiant_tout'){
            $('#ens_matricule').hide();
            $('#etd_matricule').hide();
            $('#ens_tout').hide();
            $('#ens_dep').hide();
            $('#etd_tout').show();
            $('#etd_dep').hide();
            $('#etd_ufr').hide();
            
        }else if($('#profileType').val()=='etudiant_dep'){
            $('#ens_matricule').hide();
            $('#etd_matricule').hide();
            $('#ens_tout').hide();
            $('#ens_dep').hide();
            $('#etd_tout').hide();
            $('#etd_dep').show();
            $('#etd_ufr').hide();
            
        }else if($('#profileType').val()=='etudiant_ufr'){
            $('#ens_matricule').hide();
            $('#etd_matricule').hide();
            $('#ens_tout').hide();
            $('#ens_dep').hide();
            $('#etd_tout').hide();
            $('#etd_dep').hide();
            $('#etd_ufr').show();
            
        }
        else {
            $('#ens_matricule').hide();
            $('#etd_matricule').hide();
            $('#ens_tout').hide();
            $('#ens_dep').hide();
            $('#etd_tout').hide();
            $('#etd_dep').hide();
            $('#etd_ufr').hide();
        }
    }

    function afficherTableau(type, valeur){

        
    }

    displayCorrectlyForm()

    $('#profileType').change(displayCorrectlyForm);
}

$(document).ready(script_consult);
